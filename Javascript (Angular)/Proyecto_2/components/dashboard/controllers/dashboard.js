var dashboardCtrl = function()
{
	console.log("En dashboard");

	var loginHTMLReq = $.get("components/dashboard/views/dashboard.html");
	
	var funcionDeCuandoCargue = function(htmlRecibido){
		var htmlSelector = $(htmlRecibido);

		var divContenedor = $('#dashboard--container');
		divContenedor.html(htmlSelector);
	}

	loginHTMLReq.then(funcionDeCuandoCargue);
}