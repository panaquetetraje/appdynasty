var app = angular.module('app', ['ui.router', 'naif.base64']);

app.controller('MainCtrl', ['$scope', '$rootScope', function($scope,$rootScope)
{
	/**
	* INICIALIZACIÓN DE VARIABLES GLOBALES ($rootScope)
	*/
	$rootScope.state = "";
	$rootScope.usuarios = // simula la bd de usuarios del sistema
		[
			{
				"id":123,
				"nombre":"pepe",
				"usuario":"pepe",
				"clave":"",
				"fechaCreado":"2019-03-01",
				"fechaEditado":"2019-03-01",
				"activo":1
			}
		]
	;







	$rootScope.listaDeTracks = [];


	/**
	* Simulación de creación de 100 tracks.
	*/
	for (var i = 0; i < 100; i++) {
		if(Math.floor(Math.random()* 2 + 1) == 1)
			$rootScope.listaDeTracks.push(angular.copy(
				{
					"name":"IMM" + i,
					"duration":"4:12",
					"mp3":"http://www.ivoox.com/fabiana-goyeneche-sobre-controles-intendencia_mf_30170994_feed_1.mp3",
					"cantPlay": i
				}
			));
		else
			$rootScope.listaDeTracks.push(angular.copy(
				{
					"name":"Football" + i,
					"duration":"5:13",
					"mp3":"http://www.ivoox.com/futbol-uruguayo-es-verdadero-caos_mf_35368844_feed_1.mp3",
					"cantPlay": i
				}
			));
			/*$rootScope.listaDeTracks.push(angular.copy({
				nombre:"Fútbol",
				url:"http://www.ivoox.com/futbol-uruguayo-es-verdadero-caos_mf_35368844_feed_1.mp3",
				type:"audio/mpeg",
				cantPlay: 3
			}));*/
	}


	var tracks = [];

	for (var i = 0; i < 10; i++) {
		tracks[i] = [];
		for (var j = i*10; j < i*10+10; j++)
			tracks[i].push($rootScope.listaDeTracks[j]);
	};

	$rootScope.albumesIniciales = [
		{
			"id": 0,
			"user": window.localStorage.getItem("user"),
			"nombre": "Otra navidad en las trincheras",
			"artist":"pop",
			"type":"music",
			"img":"http://cdn-d15c.kxcdn.com/fotos/discos/000/012/520/original/foto.jpg",
			"tracks":tracks[0]
		},
		{
			"id": 1,
			"user": window.localStorage.getItem("user"),
			"nombre": "De las contradicciones",
			"artist":"disco",
			"type":"music",
			"img":"http://cdn-d15c.kxcdn.com/fotos/discos/000/010/662/original/foto.jpg",
			"tracks":tracks[1]
		},
		{
			"id": 2,
			"user": "Roque",
			"nombre": "Discovery",
			"artist":"disco",
			"type":"music",
			"img":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjzsMS2ITgxyNiez2rvU21Vj7mAimwoZVQc1qKFHXw9k820RYg",
			"tracks":tracks[2]
		},
		{
			"id": 3,
			"user": "Roque",
			"nombre": "Por lo Menos Hoy",
			"artist":"rock",
			"type":"music",
			"img":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFgm2ozREz3_RWTBwflG-7pVNaR6HaFwv3Ps62a38cTyDpAqU3nw",
			"tracks":tracks[3]
		},
		{
			"id": 4,
			"user": "Roque",
			"nombre": "Solo de Noche",
			"artist":"pop",
			"type":"music",
			"img":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQtR_1QPEId8E2PFaLLBK5hhnHLkKFT53_cBjvSkJRDqu6brpqg",
			"tracks":tracks[4]
		}
	];



	//playUnTrack(rootScope.listaDeTracks[Math.floor(Math.random()*rootScope.listaDeTracks.length)]);




















	if(!window.localStorage.getItem("user"))
		$rootScope.state = 'login';
	//else
		//$rootScope.state = 'mainPage';
	//$rootScope.state = 'mainPage';
	//$rootScope.state = 'register';
	console.log("MainCtrl::");
}]);


console.log("appi: " + app);
