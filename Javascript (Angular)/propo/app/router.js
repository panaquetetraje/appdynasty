app.config(
	function($stateProvider, $urlRouterProvider)
	{
		var loginState = {
			name: 'login',
			url: '/',
			templateUrl: 'app/components/login/views/login.html',
			controller: 'LoginCtrl'
		};

		var mainPageState = {
			name: 'mainPage',
			url: '/mainPage/:page/:albumsPerPage/:styleName/',
			templateUrl: 'app/components/mainPage/views/mainPage.html',
			controller: 'MainPageCtrl'
		};


		var albumState = {
			name: 'album',
			url: '/album/:albumId/:command/:page/:tracksPerPage/:playMode',
			templateUrl: 'app/components/album/views/album.html',
			controller: 'AlbumCtrl'
		};

		
		$stateProvider.state(loginState);
		$stateProvider.state(mainPageState);
		$stateProvider.state(albumState);

		$urlRouterProvider.otherwise('/');
	}
);