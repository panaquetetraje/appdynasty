
app.controller('LoginCtrl', ['$scope', '$rootScope', '$timeout', '$state', function($scope,$rootScope,$timeout,$state)
{
	$scope.error = "";
	$scope.userLogin = {};
	$scope.userLogin.user = "";
	$scope.userLogin.psw = "";
	$scope.userLogin.img = "https://lh3.googleusercontent.com/-owdtEVX-n_Y/AAAAAAAAAAI/AAAAAAAAAKs/SJIjIZsMn5U/photo.jpg?sz=328";
	$scope.resultado = "";
	$scope.enLogin = "true";
	$scope.usuarios;

	$scope.tryLogin = function()
	{
		/**
		* TODO: REEMPLAZAR COMPARACIÓN CON CONSULTA DE USUARIO A LA API CORRESPONDIENTE.
		*/
		if(usuarioValido($scope.userLogin.user, $scope.userLogin.psw))
		{
			window.localStorage.setItem("user", $scope.userLogin.user);
			$state.go('mainPage', {"page":1, "albumsPerPage":12, "styleName":"All"} );
		}
		else
		{
			$scope.error = "No se pudo loguear.";
			$timeout(function(){$scope.error="";}, 1000);
		}
	}

	var usuarioValido = function(usuario, psw)
	{
		var usuarios = $rootScope.usuarios;
		
		for(i=0; i < usuarios.length; i++)
		{	
			if(usuario == usuarios[i].usuario && psw == usuarios[i].clave)
			{
				return true;
			}
		}
		return false;
	}

	$scope.register = function()
	{
		// INTEGRAR: DEBERÍA LLAMAR A LA API ENVIANDO INFORMACIÓN DEL USUARIO REGISTRADO.
		$timeout(
			function() // Primer parámetro de timeout, el callback o función que va a realizar al pasar el tiempo pedido.
			{
				$rootScope.usuarios.push({user:$scope.userLogin.user, psw:$scope.userLogin.psw});
				$scope.resultado = "";
				$scope.enLogin = "true";
				$scope.userLogin.user = "";
				$scope.userLogin.psw = "";
			}
			, 1000); // Segundo parámetro: tiempo de espera para ejecutar el callback (en milisegundos).
	};


}]);