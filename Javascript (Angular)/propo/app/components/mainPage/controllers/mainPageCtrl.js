
app.controller('MainPageCtrl', ['$scope', '$rootScope','$state', '$stateParams','$timeout', function($scope,$rootScope,$state,$stateParams,$timeout)
{
	$scope.ARTIST_TODOS = {nombre:"All", id:0};
	$scope.ARTIST_MY_ALBUMS = {nombre:"My Albums", id:1};
	if(!window.localStorage.getItem("user"))
	{ 	
		$timeout(function() 
			{
				$state.go('login');
			}, 100);
		
	}

	$scope.Math = window.Math;

	

	$scope.albumesFiltrados = $rootScope.albumesIniciales;
	$scope.albumesPorFila = $scope.albumesPerPage;
  	$scope.filaActual = 0;

  	$scope.pageNumber = $stateParams.page;
	$scope.pageSize = $stateParams.albumsPerPage;
  	
 	$scope.logout = function()
	{
		window.localStorage.setItem("user", "");
		$state.go('/');
	}
	
	$scope.eliminarAlbum = function(albumId)
	{
		var albumABorrar = $rootScope.albumesIniciales.find(function(element)
		{
			return element.id == albumId;
		});
		var indiceDelAlbumABorrar = $rootScope.albumesIniciales.indexOf(albumABorrar);
		//console.log("indiceDelAlbumABorrar: " + indiceDelAlbumABorrar);
		$rootScope.albumesIniciales.splice(indiceDelAlbumABorrar, 1);

		// INTEGRAR: LOCALMENTE FUE ELIMINADO EL ÁLBUM, AHORA FALTARÍA ELIMINARLO DE LA API.
	}
	
	$scope.onEstiloSeleccionado = function()
	{
		console.log("$scope.artistSeleccionado.nombre: " + $scope.artistSeleccionado.nombre);
		$scope.albumesFiltrados = [];
		for(album of $rootScope.albumesIniciales)
			if($scope.artistSeleccionado.nombre == $scope.ARTIST_TODOS.nombre 
				|| ($scope.ARTIST_MY_ALBUMS.nombre == $scope.artistSeleccionado.nombre
					 && album.user == window.localStorage.getItem("user"))
				|| album.artist == $scope.artistSeleccionado.nombre)
				$scope.albumesFiltrados.push(album);
		acomodarPageNumber();
	}
	
	$scope.obtenerDistintosEstilos = function()
	{
		var artists = [$scope.ARTIST_TODOS, $scope.ARTIST_MY_ALBUMS];
		for(album of $scope.albumesFiltrados)
		{
			var encontrado = false;
			for(artist of artists)
				if(artist.nombre == album.artist)
					encontrado = true;
			
			if(!encontrado)
				artists.push({nombre:album.artist, id:artists.length});
		}
		
		return artists;
	}

	var acomodarPageNumber = function()
	{
		/*console.log("Acomodando page number: " + $scope.pageNumber);
		console.log("$scope.tracks.length: " + $scope.tracks.length);
		console.log("$scope.pageSize: " + $scope.pageSize);
		*/
		var maxPagina = Math.ceil($scope.albumesFiltrados.length / $scope.pageSize);
		//console.log("maxPagina: " + maxPagina);
		var paginaAcotadaSuperiormente = Math.min($scope.pageNumber, maxPagina);
		//console.log("paginaAcotadaSuperiormente: " + paginaAcotadaSuperiormente);
		$scope.pageNumber = Math.max(1, paginaAcotadaSuperiormente);
		//console.log("Nuevo page number: " + $scope.pageNumber);
		//$scope.$apply();
	}

	$scope.artistsDiferentes = $scope.obtenerDistintosEstilos();
	$scope.artistSeleccionado = $scope.artistsDiferentes.find(function(style)
	{
		return style.nombre == $stateParams.styleName;
	});

 	acomodarPageNumber();
}]);