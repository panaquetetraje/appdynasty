
app.controller('AlbumCtrl', ['$scope', '$rootScope','$state','$timeout', '$stateParams', function($scope,$rootScope,$state,$timeout,$stateParams)
{
	if(!window.localStorage.getItem("user") )
	{ 	
		$timeout(function() 
			{
				$state.go('login');
			}, 10);
	}
	//console.log("En el album : " + $stateParams.albumId + " haciendo: " + $stateParams.command);


	$scope.Math = window.Math;
	$scope.albumActual= {};
	$scope.albumActual.id = $stateParams.albumId;
	$scope.albumActual.imagen= "https://lh3.googleusercontent.com/-owdtEVX-n_Y/AAAAAAAAAAI/AAAAAAAAAKs/SJIjIZsMn5U/photo.jpg?sz=328";
	$scope.albumActual.nombre= "";
	$scope.albumActual.artist= "";
	$scope.trackSiendoRep= null;
	

	$scope.state = $stateParams.command;
	$scope.tiposDeReproduccion = [
		{nombre:"Un track solamente", id:0},
		{nombre:"Continua", id:1},
		{nombre:"Aleatoria", id:2}
	];
	$scope.tipoReproduccion = $scope.tiposDeReproduccion.find(function(tipoDeReprEnArray)
	{
		return tipoDeReprEnArray.id == $stateParams.playMode;
	});
	

	$scope.tracks = []; // guarda los tracks que se están mostrando en pantalla.
	$scope.tracksDelAlbum = []; // guarda los tracks que le pertenecen al álbum.
	$scope.todosLosTracks = []; // guarda todos los tracks que existen.

	$scope.pageNumber = angular.copy($stateParams.page);
	$scope.pageSize = angular.copy($stateParams.tracksPerPage);

	$scope.trackCargado = {};

	var reproductor = $("#player")[0];
	var reproductorTrackSubido = $("#reproductorTrackSubido")[0];


	var obtenerAlbumPorId = function(albumId, callback/*(object album)*/)
	{
		// TODO: Debería conectarse con la API correspondiente y pedir el álbum que corresponda al ID.
		setTimeout(function()
		{
			var albumADevolver = $rootScope.albumesIniciales.find(function(element)
			{
				return element.id == albumId;
			});

			callback(albumADevolver);
		}, 150);
	};

	var onObtenerAlbumPorId = function(album)
	{
		$scope.albumActual = album;
		if(!$scope.$$phase)$scope.$apply();
	};

	if($scope.state != "create")
		obtenerAlbumPorId($stateParams.albumId, onObtenerAlbumPorId);
	else
		$scope.albumActual.id = $stateParams.albumId;

	var obtenerTracksPorAlbumId = function(albumId, callback/*(array listaDeTracks)*/)
	{
		// TODO: Debería conectarse con la API correspondiente y pedir los tracks que correspondan al ID del album.
		albumCargado = angular.copy($rootScope.albumesIniciales.find(function(element)
			{
				return element.id == $scope.albumActual.id;
			}));

		var tracksDelAlbum = albumCargado.tracks;
		
		
		//console.log("tracksDelAlbum.length " + tracksDelAlbum.length);


		setTimeout(function()
		{
			//console.log("listaDeTracks cargada con largo: " + $rootScope.listaDeTracks.length)
			callback(tracksDelAlbum);
		}, 150);
	}

	
	var onObtenerTracksPorAlbumId = function(listaDeTracks)
	{
		//console.log("// LLEGARON LOS TRACKS DEL ALBUM.");
		$scope.tracksDelAlbum = listaDeTracks;
		$scope.trackCargado = $scope.tracksDelAlbum[($scope.pageNumber-1) * $scope.pageSize];
		$scope.reproduccionIncrementada = false;
		if(!$scope.$$phase)$scope.$apply();

		getAllTracks(onGetAllTracks);
	}

	var getAllTracks = function(callback/*(array tracks)*/)
	{
		// INTEGRAR API: DEBERÍA LEVANTAR LOS TRACKS.
		setTimeout(function()
		{
			//console.log("listaDeTracks cargada con largo:: " + listaDeTracks.length)
			callback($rootScope.listaDeTracks)
		}, 150);
	};


	var onGetAllTracks = function(tracks) // Pensado para usar como callback de getAllTracks.
	{
		// LLEGARON LOS TRACKS CARGADOS DE LA API.
		//console.log("// LLEGARON TODOS LOS TRACKS CARGADOS DE LA API.");
		$scope.todosLosTracks = tracks;
		if(!$scope.$$phase)$scope.$apply();



		/**
		* A esta altura ya están cargados todos los tracks y además los tracks del álbum, entonces
		* podemos mostrar los que corresponda de acuerdo al comando que se esté usando.
		*/

		actualizarCampoEstaEnElAlbumDeTracks();

		mostrarTracksConformeAComando();
	}


	if($scope.state != "create")
		obtenerTracksPorAlbumId($stateParams.albumId, onObtenerTracksPorAlbumId);
	else
		getAllTracks(onGetAllTracks);

	
	
	var acomodarPageNumber = function()
	{
		/*console.log("Acomodando page number: " + $scope.pageNumber);
		console.log("$scope.tracks.length: " + $scope.tracks.length);
		console.log("$scope.pageSize: " + $scope.pageSize);
		*/
		var maxPagina = Math.ceil($scope.tracks.length / $scope.pageSize);
		//console.log("maxPagina: " + maxPagina);
		var paginaAcotadaSuperiormente = Math.min($scope.pageNumber, maxPagina);
		//console.log("paginaAcotadaSuperiormente: " + paginaAcotadaSuperiormente);
		$scope.pageNumber = Math.max(0, paginaAcotadaSuperiormente);
		//console.log("Nuevo page number: " + $scope.pageNumber);
		//$scope.$apply();
	}

	var actualizarCampoEstaEnElAlbumDeTracks = function()
	{
		for  (var i=0; i < $scope.todosLosTracks.length; i++ ) 
		{
			var track = $scope.todosLosTracks[i];
			var trackEncontrado = $scope.tracksDelAlbum.find(function(trackEnElAlbum)
				{
					return trackEnElAlbum.name == track.name;
				});
			track.estaEnElAlbum = (trackEncontrado != null);
		}
	}

	


	var mostrarTracksConformeAComando = function()
	{
		if($scope.state == "view")
			$scope.tracks = $scope.tracksDelAlbum;
		else // create o edit
			$scope.tracks = $scope.todosLosTracks;
		//console.log($scope.tracks.length);
		acomodarPageNumber();
		if(!$scope.$$phase)$scope.$apply();
		
	}

	var reproducirTrack = function(trackAReproducir)
	{
		//console.log("trackAReproducir: " + trackAReproducir);
		$scope.trackCargado = $scope.tracksDelAlbum[trackAReproducir];
		$scope.reproduccionIncrementada = false;
		console.log("$scope.$$phase: " + $scope.$$phase);
		if(!$scope.$$phase)$scope.$apply();
		reproductor.play();
	}

	$scope.ordenarTracksPorCantRepr = function()
	{
		console.log("ordenando");
	}

	$scope.reproducirAnterior = function()
	{
		var anteriorIndice = $scope.tracksDelAlbum.indexOf($scope.trackCargado)-1;
		if(anteriorIndice < ($scope.pageNumber - 1) * $scope.pageSize)
			anteriorIndice = Math.min($scope.tracks.length-1, $scope.pageNumber * $scope.pageSize - 1);
		reproducirTrack(anteriorIndice);
		$scope.trackSiendoRep=null;
	}
	
	$scope.reproducirSiguiente = function()
	{
		$scope.trackSiendoRep=null;
		//console.log("$scope.trackCargado.nombre: " + $scope.trackCargado.nombre);
		//console.log("reproducirSiguiente: " + $scope.tracksDelAlbum.indexOf($scope.trackCargado));
		switch($scope.tipoReproduccion.id)
		{
			case 0: // reproducir sólo un track.
				// no se reproduce nada.
			break;
			case 1: // reproduccion continua.
				var siguienteIndice = ($scope.tracksDelAlbum.indexOf($scope.trackCargado)+1);
				var maximoMostrado = Math.min($scope.tracks.length, $scope.pageNumber * $scope.pageSize);
				if(siguienteIndice >= maximoMostrado)
					siguienteIndice = ($scope.pageNumber-1) * $scope.pageSize;
				console.log("($scope.pageNumber-1): " + ($scope.pageNumber-1));
				console.log("$scope.pageSize: " + $scope.pageSize);
				console.log("siguienteIndice: " + siguienteIndice);
				reproducirTrack(siguienteIndice);
			break;
			case 2: // reproduccioim aleatoria.
				var cantTracksMostrados = Math.min($scope.pageSize, $scope.tracksDelAlbum.length-$scope.pageSize*($scope.pageNumber-1));
				var aleatorioEnRango = Math.floor(Math.random()*cantTracksMostrados);

				console.log("aleatorioEnRango: " + aleatorioEnRango);
				var indiceAleatorio = aleatorioEnRango + ($scope.pageNumber - 1) * $scope.pageSize;
				reproducirTrack(indiceAleatorio);
			break;
		}
	}
	
	$scope.cargarTrack = function(trackNumber)
	{
		$scope.trackCargado = $scope.tracks[trackNumber];
		$scope.reproduccionIncrementada = false;
		if(!$scope.$$phase)$scope.$apply();
	}


	$scope.quitarTrack = function(track)
	{
		track.estaEnElAlbum = false;
		if(!$scope.$$phase)$scope.$apply();

		quitarTrackDelAlbumEnApi(track, $scope.albumActual.id, onTrackQuitadoDeAlbumEnApi);
	}

	$scope.agregarTrack = function(track)
	{
		//$scope.tracksDelAlbum.push(track);
		track.estaEnElAlbum = true;
		if(!$scope.$$phase)$scope.$apply();

		agregarTrackAAlbumEnApi(track, $scope.albumActual.id, onTrackAgregadoAAlbumEnApi);
	}

	
	$scope.actualizarTracksMostrados = function()
	{
		mostrarTracksConformeAComando();
	}


	$scope.confirmarCreacionAlbum = function()
	{
		/*
		console.log("$scope.albumActual.id: " + $scope.albumActual.id);
		console.log("$scope.albumActual.imagen: " + $scope.albumActual.imagen);
		console.log("$scope.albumActual.nombre: " + $scope.albumActual.nombre);
		console.log("$scope.albumActual.artist: " + $scope.albumActual.artist);
		console.log("$scope.tracksDelAlbum.length: " + $scope.tracksDelAlbum.length);
		*/

		var nuevoAlbum = {
			"id": $scope.albumActual.id,
			"user": window.localStorage.getItem("user"),
			"nombre": $scope.albumActual.nombre,
			"artist":$scope.albumActual.artist,
			"type":"music",
			"img":$scope.albumActual.imagen,
			"tracks":$scope.tracksDelAlbum
		};


		console.log("$scope.tracksDelAlbum.length: " + $scope.tracksDelAlbum.length);

		/**
		* INTEGRAR: DEBERÍA ENVIAR A LA API EL NUEVO OBJETO ALBUM Y QUE ELLA LE GENERE UNA ID.
		*/
		$rootScope.albumesIniciales.push(nuevoAlbum);
		$state.go('mainPage', {"page":1, "albumsPerPage":12, "styleName":"All"} );


	}
	
	reproductor.addEventListener("ended", $scope.reproducirSiguiente);
	reproductor.addEventListener("play", function()
	{
		//console.log("---PLAY---");
		$scope.trackSiendoRep = $scope.trackCargado;

		var tiempoParaReprVal = 5000;
		if(reproductor.duration)
		{
			tiempoParaReprVal = Math.min(Math.round(reproductor.duration / 50), 20); // en segundos.
			//console.log("tiempoParaReprVal: " + tiempoParaReprVal);
		}
			
		setTimeout(function()
		{
			//console.log("Tiempo transcurrido: " + tiempoParaReprVal + " segundos.");
			if($scope.trackSiendoRep == $scope.trackCargado && !$scope.reproduccionIncrementada){
				//console.log("$scope.trackCargado.cantPlay antes: " + $scope.trackCargado.cantPlay);
				$scope.trackCargado.cantPlay++;

				/**
				* INTEGRAR: ENVIAR A LA API LA INFORMACIÓN DE QUE EL TRACK FUE REPRODUCIDO.
				*/


				$scope.reproduccionIncrementada= true;
				//console.log("$scope.trackCargado.cantPlay después: " + $scope.trackCargado.cantPlay);
				if(!$scope.$$phase)$scope.$apply();
			}

		}, tiempoParaReprVal*1000); // pasando a milisegundos.
	});


	reproductor.addEventListener("pause", function()
	{	
		$scope.trackSiendoRep=null;
	});








	/**
	* TESTING
	*/
	//var FileReader = FileReader();
	$scope.audioObj = {};
	$scope.audioObj.textoReproducirOPausar = "";
	$scope.audioObj.nombreTrackASubir = "";
	//$scope.urlTrackASubir = "No cargado.";
	$scope.audioSubidoState = false;



	$scope.cancelarSubidaTrack = function(){
		$scope.audioSubidoState = false;
		$scope.audioObj.nombreTrackASubir = "";
	};


	$scope.reproducirTrackSubido = function(){
		if($scope.audioObj.audioSubido.audio)
		{
			if($scope.audioObj.textoReproducirOPausar == "Pausar"){
				$scope.audioObj.audioSubido.audio.pause();
				$scope.audioObj.textoReproducirOPausar = "Reproducir";
			}
			else{
				$scope.audioObj.audioSubido.audio.play();
				$scope.audioObj.textoReproducirOPausar = "Pausar";
			}
		}
	};

			
	$scope.agregarAudioAlAlbum = function()
	{
		enviarAudioCargadoAApi(angular.copy($scope.audioObj), onTrackSubidoAApi);
		$scope.audioSubidoState = false;
		$scope.audioObj.nombreTrackASubir = "";
	};

	$scope.onAudioSubido = function(event)
	{
		$timeout( function  () {
			$scope.audioObj.textoReproducirOPausar = "Reproducir";
			$scope.audioObj.audioSubido.audio = new Audio("data:audio/mpeg;base64,"+$scope.audioObj.audioSubido.base64);

			$scope.audioObj.nombreTrackASubir = $scope.audioObj.audioSubido.filename;
			$scope.audioSubidoState = true;
			if(!$scope.$$phase)$scope.$apply();
		})
	};

	
	var onTrackQuitadoDeAlbumEnApi = function(track, albumId)
	{
		obtenerTracksPorAlbumId(albumId, onObtenerTracksPorAlbumId);
	}
	var onTrackAgregadoAAlbumEnApi = function(track, albumId)
	{
		obtenerTracksPorAlbumId(albumId, onObtenerTracksPorAlbumId);
	}

	var quitarTrackDelAlbumEnApi = function(track, albumId, callback/*(track, albumId)*/)
	{
		// INTEGRAR: ENVIAR EL OBJETO TRACK ASOCIADO AL ALBUM ID A LA API PARA SER QUITADO.
		setTimeout(function() // SIMULA DEMORA DE LA API.
		{
			var albumParaQuitarTrack = $rootScope.albumesIniciales.find(function(element)
			{
				return element.id == $scope.albumActual.id;
			});
			var índiceTrack = albumParaQuitarTrack.tracks.indexOf(track);
			albumParaQuitarTrack.tracks.splice(índiceTrack, 1);
		
			if(!$scope.$$phase)$scope.$apply();
			callback(track, albumId);
		}, 150);
	};

	var agregarTrackAAlbumEnApi = function(track, albumId, callback/*(track, albumId)*/)
	{
		// INTEGRAR: ENVIAR EL OBJETO TRACK ASOCIADO AL ALBUM ID A LA API.
		setTimeout(function() // SIMULA DEMORA DE LA API.
		{
			var albumParaInsertarTrack = $rootScope.albumesIniciales.find(function(element)
			{
				return element.id == $scope.albumActual.id;
			});
			albumParaInsertarTrack.tracks.push(track);
		
			if(!$scope.$$phase)$scope.$apply();
			callback(track, albumId);
		}, 150);
	};

	var onTrackSubidoAApi = function(trackEnLaApi)
	{
		agregarTrackAAlbumEnApi(trackEnLaApi, $scope.albumActual.id, onTrackAgregadoAAlbumEnApi);
	};



	var enviarAudioCargadoAApi = function(audioObj, callback)
	{
		// INTEGRAR: ENVIAR TRACK CARGADO A API Y MANEJAR URL QUE VUELVA.
		//console.log("enviarAudioCargadoAApi");

		setTimeout(function()
		{

			console.log(audioObj);
			var minutos = Math.floor((audioObj.audioSubido.audio.duration)/60);
			var segundos = Math.round(audioObj.audioSubido.audio.duration%60);
			var duración =  minutos + ":" + segundos;
			var nombreTrack = audioObj.nombreTrackASubir;
			var track = {
					"name": nombreTrack,
					"duration": duración,
					"mp3": "http://tracks/" + nombreTrack,
					"cantPlay": 0
				};
			$rootScope.listaDeTracks.push(track);
			callback(track); // simula el callback de la api.
		}, 150);
	}
}]);