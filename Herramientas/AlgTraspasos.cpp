#include <stdio.h>
#include <math.h>
int main()
{
/*
Caso de prueba 1, todos TOP:

Tenía 866 en presupuesto TOP.
Tenía 866 en jugadores.
Tenía resto 0.
1er intercambio:
- Vendo uno que decreció 3 (79 A 76).
- Compro uno a 76.

2do intercambio:
- Vendo uno que creció 2 (76 a 78).
- Compro uno a 78.

3er intercambio:
- Vendo uno que creció 3 (76 a 79)
- Compro uno a 79.

Resultado esperado:
1 intercambio: Puede hacerse, queda 863 TOP.
2 intercambio: No puede hacerse, no dan los puntos (resto 1, precisa 2)
3 intercambio: No puede hacerse, no dan los puntos (resto 0, precisa 2)
*/
	int presupuestoTop = 866;
	int sumaJugadoresTop = 866;
	int resto = presupuestoTop - sumaJugadoresTop;

	int cantIntercambios = 3;

	int *valorJugadorAComprar 	= new int[cantIntercambios]; // lo que sale el nuevo a comprar
	int *pagadoJugadorAVender 	= new int[cantIntercambios]; // lo que salió
	int *overallJugadorAVender 	= new int[cantIntercambios]; // hoy en día
	
	int *valorVentaJugadorTop 	= new int[cantIntercambios];
	int *gananciaJT 			= new int[cantIntercambios];
	int *costoIntercambioTop 	= new int[cantIntercambios];
	bool *falloIntercambio	 	= new bool[cantIntercambios];

	for(int i = 0; i < cantIntercambios; i++)
		falloIntercambio[i] = false;

    
    valorJugadorAComprar[0] = 76;
    pagadoJugadorAVender[0] = 79;
    overallJugadorAVender[0] = 76;
    
    valorJugadorAComprar[1] = 78;
    pagadoJugadorAVender[1] = 76;
    overallJugadorAVender[1] = 78;
    
    valorJugadorAComprar[2] = 79;
    pagadoJugadorAVender[2] = 76;
    overallJugadorAVender[2] = 79;


    int sumaCompraTop = valorJugadorAComprar[1] + valorJugadorAComprar[2] + valorJugadorAComprar[3];

    for (int i = 0; i < cantIntercambios; i++)
    {
    	resto = presupuestoTop - sumaJugadoresTop; // cuánto me queda para gastar arriba del presupuesto.
    	int crecimiento = overallJugadorAVender[i] - pagadoJugadorAVender[i]; // crecimiento o decrecimiento
    	int faltanteParaTopear = fmax(0, 864 - (presupuestoTop) );
    	if (crecimiento  > faltanteParaTopear) // Me paso del tope.
			gananciaJT[i] = faltanteParaTopear + 1;
		else
			gananciaJT[i] = crecimiento;
		
		valorVentaJugadorTop[i] = pagadoJugadorAVender[i] + gananciaJT[i]; // el valor al que realmente voy a vender al jugador

		costoIntercambioTop[i] = valorJugadorAComprar[i] - valorVentaJugadorTop[i]; // cuánto me sale la compra

		if(resto - costoIntercambioTop[i] >= 0)
		{
		   // Es válido el bloque de traspasos.
		   presupuestoTop += gananciaJT[i];
		}
		else
		{
		  falloIntercambio[i] = true;
		}

    }



    // TERMINADO ALGORITMO, REPORTE:
    bool falloAlguno = false;
    for (int i = 0; i < cantIntercambios; ++i)
    {
    	if(falloIntercambio[i])
    	{

    		printf("fallo el intercambio %d.\n", i+1);
    		falloAlguno = true;
    	}
    	else
    		printf("salio bien el %d.\n", i+1);
    }
    if(!falloAlguno)
    	printf("Se hicieron los intercambios. Presupuesto final: %d.\n", presupuestoTop);
    else
    	printf("No se hicieron los intercambios.\n");

    return 0;
}
