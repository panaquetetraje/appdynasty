package scraper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import datatypes.JugadorDT;
//import model.EscritorDeArchivos;
import model.ModeloJugador;

public class CargadorDeJugadores implements Runnable{

	private String _página = "Error";
	private ModeloJugador _modeloJugador;
	private String _nombreArchivo;
	public CargadorDeJugadores(String página, ModeloJugador modeloJugador, String nombreArchivo) {
		_página = página;
		_modeloJugador = modeloJugador;
		_nombreArchivo = nombreArchivo;
	}

	public void cargarPágina() {
		int jugadoresActualizados = 0;
		try 
		{
    		//turn off htmlunit warnings
		    java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.ALL);
		    java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.ALL);
	
		    
		    final WebClient webClient = new WebClient(BrowserVersion.CHROME);
		    webClient.getOptions().setCssEnabled(false);//if you don't need css
		    webClient.getOptions().setJavaScriptEnabled(false);//if you don't need js
//		    HtmlPage page = webClient.getPage("http://XXX.xxx.xx");
		    
		    
		    HtmlPage page;
		    ArrayList<JugadorDT> jugadores = new ArrayList<JugadorDT>();
		    
		    page = webClient.getPage(_página);
			
			String[] lines = page.getBody().asXml().split(System.getProperty("line.separator"));
			
			
			
			//EscritorDeArchivos.crearNuevoArchivo(_nombreArchivo);
	    	
//			for (j = 0; j < lines.length; j++) 
//			{
				//EscritorDeArchivos.escribirTextoEnArchivo(_nombreArchivo, bodyAsXML);
//			}
			
			
			
			
			
			
//			System.out.println("Largo lÃ­neas: " + lines.length);

			for (int j = 0; j < lines.length; j++) 
//			for (int j = 0; j < 9600; j++) 
			{
				boolean jugadorEncontrado = false;
				String overallRating = "";
				String name="";
				String id = "";
				
				
				
//				System.out.println(j + ": " + lines[j]);
				/**
				 * Busco ID
				 */
				if(lines[j].contains("<a class=\"tooltip\" href=\"/player"))
				{
//					System.out.println("Id en: " + j + ": " + lines[j]);
//					System.out.println("Ã³ id en: " + (j+1) + ": " + lines[j+1]);
					
//					System.out.println("----BUSCANDO NOMBRE");
//					for (int i = 0; i < 10; i++) {
//						System.out.println((j-43+i) + ": " + lines[j-43+i+1]);
//						
//					}
//					System.out.println("----TERMINÃ“ BUSCANDO NOMBRE");
//					if(j>0)
//					{
//						System.out.println("Â¿Nombre en " + (j-38) + "?");
//						System.out.println(lines[j-38]);
////						throw new Error("terminando a prepo");
//					}
						
					try
					{
					
						jugadorEncontrado = true;
						
						
						
						/**
						 * Agarro la id.
						 */
//						id = getIdFromLine(lines[j]);
//						if(id.equals(""))
							id = getIdFromLine(lines[j].trim());
						
//						System.out.println("ID agarrada: " + id);
						int idAsInt = Integer.parseInt(id);
						int overallAsInt = 0;
						
						
						/**
						 * Agarro el nombre
						 */
//						int nameLineIndex = j+1;
						int nameLineIndex = j+3;
						name = getNombreFromLine(lines[nameLineIndex]);
						
						
						int overallLineIndex = 0;
						for (int i = 10; i < 35; i++) {
							if(lines[j+i].contains("<td class=\"col col-oa col-sort\" data-col=\"oa\">"))
							{
								overallLineIndex = j+i+2;
//								System.out.println("lÃ­nea: " + i + " con " + overallLineIndex + " deberÃ­a tener el overall  :" + lines[overallLineIndex]);
//								System.out.println("OVERALL CON Ã�NDICE DIN: " + getOverallFromLine(lines[overallLineIndex]));
								/**
								 * Agarro el overall.
								 */
								overallRating = getOverallFromLine(lines[overallLineIndex]);
								overallAsInt = Integer.parseInt(overallRating);
								break;
							}
							//System.out.println((j+i) + ": " + lines[j+i+1]);
							
						}
						
						
//						j+=60;
						
						
						
						
						
						
						
						JugadorDT jugador = new JugadorDT(idAsInt, name, Math.max(66,overallAsInt));
						jugadores.add(jugador);
						System.out.println("Jugador: " + id + ": " + name + " (" + overallRating + ")");
						if(_modeloJugador.persistirJugador(jugador))
							jugadoresActualizados++;
						if(jugadoresActualizados == 60)
							break;
					}
					catch(NumberFormatException e)
					{
						System.out.println("Problemas en lÃ­nea " + j + ":" + lines[j]);
						e.printStackTrace();
						
						for (int i = 0; i < 35; i++) {
							System.out.println((j+i) + ": " + lines[j+i+1]);
							
						}
					}
				}
				
			}

//				
			System.out.println("Terminado, se cargaron: " + jugadores.size() + " y se actualizaron: " + jugadoresActualizados + " jugadores.");

			if(jugadores.size()<60)
				throw new Error("SE CARGARON MENOS DE 60 EN PÃ�GINA: " + _página);
			webClient.close();
		} catch (FailingHttpStatusCodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private static String getOverallFromLine(String linea)
	{
		String overallRating = "";
		Pattern p = Pattern.compile("\\b");  // insert your pattern here
		Matcher m = p.matcher(linea);
		if (m.find()) 
		{
		   int position = m.start();
		   //System.out.println("REGEX getOverallFromLine: " + position);
		   //System.out.println("linea: " + linea);
		   overallRating = linea.substring(position, linea.length());
//		   overallRating = linea.substring(position, linea.length()-1);
		}
		//System.out.println("overallRating: " + overallRating + ".");
		return overallRating;
	}
	
	private static String getNombreFromLine(String linea)
	{
		String name = "ERROR";
		Pattern p = Pattern.compile("\\b");  // insert your pattern here
		Matcher m = p.matcher(linea);
		if (m.find()) 
		{
		   int position = m.start();
//		   name = linea.substring(position, linea.length()-1);
		   name = linea.substring(position, linea.length());
		   //System.out.println("name: " + name + ".");
		}
		return name;
	} 
	
	private static String getIdFromLine(String linea)
	{
		
		String id = "";
		Pattern p = Pattern.compile("[0-9]/");
//		Pattern p = Pattern.compile("[0-9]*");
		Pattern p2 = Pattern.compile("player/[0-9]*");
		Matcher m = p.matcher(linea.trim());
		Matcher m2 = p2.matcher(linea.trim());
		if (m.find() && m2.find())
		{
			int position = m.start()+1;
			int position2 = m2.start()+7;
//			System.out.println("REGEX getIdFromLine: " + position /*+","+ position2*/);
			id = linea.substring(position2, position);
//			System.out.println("id: " + id + ".");
//			System.out.println("linea: " + linea + ".");
		}
		else
		{
			System.out.println("No encontrÃ© id en lÃ­nea: " + linea + "\n" + linea.trim());
			throw new Error("No encontrÃ© id en lÃ­nea: " + linea + "\n" + linea.trim());
		}
		return id;
	}
	@Override
	public void run() {
		cargarPágina();
		
	}

}
