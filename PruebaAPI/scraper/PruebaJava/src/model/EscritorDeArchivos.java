package model;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class EscritorDeArchivos {

	public static void escribirTextoEnArchivo(String uri, String contenido) {
		try {

			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(uri));

			bufferedWriter.append(contenido);
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void crearNuevoArchivo(String uri) {
		try {
			FileWriter fw = new FileWriter(uri);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean existeArchivo(String uri) {
		File file = new File(uri);
		boolean existe = file.isFile();
		return existe;
	}

	public static boolean existeDirectorio(String uri) {
		File file = new File(uri);
		boolean existe = file.isDirectory();
		return existe;
	}

	public static void escribirArchivoBinario(String uri, byte[] buffer)
	{
		try {
			FileOutputStream outputStream = new FileOutputStream(uri);
			outputStream.write(buffer);
			outputStream.close();
			
			System.out.println("Wrote " + buffer.length + " bytes");
		} catch (IOException ex) {
			System.out.println("Error writing file '" + uri + "'");
		}
	}
}