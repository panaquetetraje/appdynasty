package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import org.json.JSONObject;

import datatypes.JugadorDT;

public class ModeloJugador {
	private Connection _connection;
	public static  String SERVER_URL = "https://dynastyfobal.000webhostapp.com";
	public static  String SERVER_PORT = "";
	
	
	
	public ModeloJugador()
	{
		/**
		 * Para probar localmente:
		 */
		
		 SERVER_URL = "http://localhost";
		 SERVER_PORT = ":8889";
			//*/
		 try {
//			 _connection = DriverManager.getConnection("jdbc:mysql://localhost/PruebaScraper?user='root'&characterEncoding=UTF-8");
//			 _connection = DriverManager.getConnection("jdbc:mysql://localhost/dynasty?user='root'&characterEncoding=UTF-8");
			 _connection = DriverManager.getConnection("jdbc:mysql://localhost/id10281982_dynasty?user='root'&characterEncoding=UTF-8&serverTimezone=America/Montevideo");
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean persistirJugador(JugadorDT jugador) {
		return persistirJugadorLocalmente(jugador) /*&& persistirJugadorRemotamente(jugador)*/;
	}

	private boolean persistirJugadorLocalmente(JugadorDT jugador) {
		boolean resultado = false;
		PreparedStatement stmtSelect, stmtInsert, stmtUpdate;
		// System.out.println("Persistiendo jugador");
		try {
			_connection.setAutoCommit(false);
			stmtSelect = _connection.prepareStatement("SELECT overall_rating FROM jugadores WHERE id = ?");
			stmtSelect.setInt(1, jugador.get_id());
			ResultSet rs = stmtSelect.executeQuery();
			
			
			
			if(!rs.next())
			{
				System.out.println("Creando nuevo jugador: " + jugador.toString() + " con " + jugador.get_overallRating());
				stmtInsert = _connection.prepareStatement("INSERT INTO jugadores VALUES(?, ?, ?)");
				stmtInsert.setInt(1, jugador.get_id());
				stmtInsert.setString(2, jugador.get_name());
				stmtInsert.setInt(3, jugador.get_overallRating());
				stmtInsert.execute();
				resultado = true;
			}
			else if(rs.getInt("overall_rating") != jugador.get_overallRating())
			{
				System.out.println("Actualizando jugador: " + jugador.toString() + " de " + rs.getInt("overall_rating") + " a " + jugador.get_overallRating());
				stmtUpdate = _connection.prepareStatement("UPDATE jugadores SET name = ?, overall_rating = ? WHERE ID = ? ;");
				stmtUpdate.setString(1, jugador.get_name());
				stmtUpdate.setInt(2, jugador.get_overallRating());
				stmtUpdate.setInt(3, jugador.get_id());
				stmtUpdate.execute();
				resultado = true;
			}
			else
			{
//				System.out.println("No se modificÃ³ el overall de: " + jugador.toString() + ", porque tenía: " + rs.getInt("overall_rating"));
				resultado = false;
			}
				
			_connection.commit();
			_connection.setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Problemas persistiendo: " + jugador.get_name() + ": " + jugador.get_id() + ", con overall " + jugador.get_overallRating());
			e.printStackTrace();
			try {
				_connection.rollback();
				_connection.setAutoCommit(true);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		return resultado;
		
	}

	private boolean persistirJugadorRemotamente(JugadorDT jugador) {
		
		JugadorDT jugadorLeído = leerJugadorRemotamente(jugador.get_id());
//		System.out.println("Leído: " + jugadorLeído.toString());
//		System.out.println("A escribir: " + jugador.toString());
		if(jugadorLeído == null)
		{
			System.out.println("Creando remotamente. " + jugador.toString());
			crearJugadorRemotamente(jugador);
			return true;
		}
		else if(!jugadorLeído.equals(jugador))
		{
			System.out.println("Actualizando remotamente. " + jugador.toString());
			actualizarJugadorRemotamente(jugador);
			return true;
		}
		else
		{
//			System.out.println("No persistiendo remotamente, eran iguales. " + jugador.toString());
			return false;
		}
		
	}

	private void actualizarJugadorRemotamente(JugadorDT jugador) {
			
		try {
			
			String urlString = SERVER_URL + SERVER_PORT+"/api/jugador/update.php";
			
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); // PUT is another valid option
			http.setDoOutput(true);
			
			
			Map<String,String> arguments = new HashMap<>();
			StringJoiner sj = new StringJoiner("&");
			for(Map.Entry<String,String> entry : arguments.entrySet())
			    sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
			         + URLEncoder.encode(entry.getValue(), "UTF-8"));
			
			String jsonString = 
					"{"
						+ "\"id\":\"" + String.valueOf(jugador.get_id()) + "\""
						+ ",\"name\":\"" + jugador.get_name() +"\""
						+ ",\"overall_rating\":\""+String.valueOf(jugador.get_overallRating())+"\""
					+ "}";
			byte[] out = jsonString.getBytes(StandardCharsets.UTF_8);
			int length = out.length;

			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    		http.getInputStream()));
			String decodedString;
			while ((decodedString = in.readLine()) != null) {
//				System.out.println(decodedString);
			}
			in.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	private void crearJugadorRemotamente(JugadorDT jugador) {
		try {
			
			
			
			String urlString = SERVER_URL + SERVER_PORT+"/api/jugador/create.php";
			
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST"); // PUT is another valid option
			http.setDoOutput(true);
			
			
			Map<String,String> arguments = new HashMap<>();
			StringJoiner sj = new StringJoiner("&");
			for(Map.Entry<String,String> entry : arguments.entrySet())
			    sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
			         + URLEncoder.encode(entry.getValue(), "UTF-8"));
			
			String jsonString = 
					"{"
						+ "\"id\":\"" + String.valueOf(jugador.get_id()) + "\""
						+ ",\"name\":\"" + jugador.get_name() +"\""
						+ ",\"overall_rating\":\""+String.valueOf(jugador.get_overallRating())+"\""
					+ "}";
			byte[] out = jsonString.getBytes(StandardCharsets.UTF_8);
			int length = out.length;

			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    		http.getInputStream()));
			
			while ((in.readLine()) != null) {
//				System.out.println(decodedString);
			}
			in.close();
			 
			
			
			/*
			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(urlString);

			// Request parameters and other properties.
			
			List<NameValuePair> params = new ArrayList<NameValuePair>(3);
			params.add(new BasicNameValuePair("id", "12345"));
			params.add(new BasicNameValuePair("name", "Hello!"));
			params.add(new BasicNameValuePair("overall_rating", "55"));
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

			//Execute and get the response.
			org.apache.http.HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = ((org.apache.http.HttpResponse) response).getEntity();

			if (entity != null) {
			    try (InputStream instream = entity.getContent()) {
			        System.out.println("LlegÃ³: " + instream.toString());
			    }
			}
			*/
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private JugadorDT leerJugadorRemotamente(int idRemota) {
		JugadorDT jugador = null;
		try {
						
			String urlString = SERVER_URL + SERVER_PORT+"/api/jugador/read_one.php?id=" + idRemota;
			
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("GET");
			http.setDoOutput(true);
			
			
			Map<String,String> arguments = new HashMap<>();
			StringJoiner sj = new StringJoiner("&");
			for(Map.Entry<String,String> entry : arguments.entrySet())
			    sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
			         + URLEncoder.encode(entry.getValue(), "UTF-8"));
			
			
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			
			BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                    		http.getInputStream()));
			String decodedString;
			
			
			while ((decodedString = in.readLine()) != null)
			{
//				System.out.println(decodedString);
				JSONObject obj = new JSONObject(decodedString);
				int id = obj.getInt("id");
				String name = obj.getString("name");
				int overall_rating = obj.getInt("overall_rating");
				jugador = new JugadorDT(id, name, overall_rating);
//				System.out.println("Leyendo de : " + urlString + " / "+ jugador.toString());
			}
			in.close();
			 
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//cre.printStackTrace();
		}
    	return jugador;
  	}
}
