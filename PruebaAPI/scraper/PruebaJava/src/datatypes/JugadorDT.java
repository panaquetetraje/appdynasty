package datatypes;

public class JugadorDT {

	private int _id;
	private String _name;
	private int _overallRating;

	public JugadorDT(int id, String name, int overallRating) {
		_id = id;
		_name = name;
		_overallRating = overallRating;
	}

	public int get_id() {
		return _id;
	}

	public String get_name() {
		return _name;
	}

	public int get_overallRating() {
		return _overallRating;
	}

	@Override
	public String toString() {
		return "Jugador: " + _name + ", overall: " + _overallRating + ", id: " + _id;
	}

	@Override
	public boolean equals(Object jugador) {
		JugadorDT jugadorCasteado = (JugadorDT) jugador;
		return jugadorCasteado.get_id() == get_id()
				&& jugadorCasteado.get_overallRating() == get_overallRating()
				&& jugadorCasteado.get_name().equals(get_name());
	}
	
	

}
