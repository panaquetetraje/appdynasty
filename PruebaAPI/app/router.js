app.config(
	function($stateProvider, $urlRouterProvider)
	{
		var mainPageState = {
			name: 'mainPage',
			url: '/mainPage',
			templateUrl: 'app/components/mainPage/views/mainPage.html',
			controller: 'MainPageCtrl'
		};
		var loginState = {
			name: 'login',
			url: '/',
			templateUrl: 'app/components/login/views/login.html',
			controller: 'LoginCtrl'
		};
		var userZoneState = {
			name: 'userZone',
			url: '/userZone',
			templateUrl: 'app/components/userZone/views/userZone.html',
			controller: 'UserZoneCtrl'
		};
		var verEquipoState = {
			name: 'verEquipo',
			url: '/verEquipo/:id',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/verEquipo.html',
					controller: 'VerEquipoCtrl'
				}
			},
			parent: userZoneState
		};
		var traspasosState = {
			name: 'traspasos',
			url: '/traspasos',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/traspasos.html',
					controller: 'TraspasosCtrl'
				}
			},
			parent: userZoneState
		};
		var verJugadorState = {
			name: 'verJugador',
			url: '/verJugador/:id',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/verJugador.html',
					controller: 'VerJugadorCtrl'
				}
			},
			parent: userZoneState
		};
		var proximosPartidosState = {
			name: 'proximosPartidos',
			url: '/proximosPartidos/:id',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/proximosPartidos.html',
					controller: 'ToornamentCtrl'
				}
			},
			parent: userZoneState
		};
		var reglamentoState = {
			name: 'reglamento',
			url: '/reglamento',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/reglamento.html',
					controller: 'ReglamentoCtrl'
				}
			},
			parent: userZoneState
		};

		var verSeccionReglamentoState = {
			name: 'verSeccionReglamento',
			url: '/verSeccionReglamento/:indice',
			views: 
			{
				'CenterPanel':
				{
					templateUrl: 'app/components/userZone/views/verSeccionReglamento.html',
					controller: 'ReglamentoCtrl'
				}
			},
			parent: userZoneState
		};
		$stateProvider.state(loginState);
		$stateProvider.state(userZoneState);
		$stateProvider.state(verEquipoState);
		$stateProvider.state(verJugadorState);
		$stateProvider.state(proximosPartidosState);
		$stateProvider.state(mainPageState);
		$stateProvider.state(traspasosState);
		$stateProvider.state(reglamentoState);
		$stateProvider.state(verSeccionReglamentoState);

		$urlRouterProvider.otherwise('/');
	}
);