var app = angular.module('app', ['ui.router', 'naif.base64']);

app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});

var SERVER_URL = "https://dynastyfobal.000webhostapp.com";
var SERVER_PORT = "";
var ID_MERCADO_ASIATICO = 1;
var NOMBRE_MERCADO_ASIATICO = "Mercado Asiático";



/**
* PARA PROBAR LOCALMENTE
*/


  
//SERVER_URL = "http://localhost";
//SERVER_PORT = ":8889";
//SERVER_URL = "http://localhost/Dynasty/appdynasty/PruebaAPI";

//Agustin
SERVER_URL = "http://localhost";
SERVER_PORT = "";

app.controller('MainController', ['$scope', '$rootScope', function($scope, $rootScope)
{
	//console.log("MainController::", $scope.principal);
  /**
  * Cambio la imagen de fondo para el login.
  */
  $rootScope.urlBackgroundImage = "";
  	
}]);


console.log("app: " + app);