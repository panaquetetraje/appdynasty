app.controller('LoginCtrl', ['$scope', '$rootScope', '$timeout', '$state', function($scope,$rootScope,$timeout,$state)
{


	/**
	* Declaración e inicialización de variables del scope.
	*/
	$scope.error = "";
	$scope.userLogin = {};
	$scope.userLogin.user = "";
	$scope.userLogin.psw = "";
	$scope.userLogin.img = "https://lh3.googleusercontent.com/-owdtEVX-n_Y/AAAAAAAAAAI/AAAAAAAAAKs/SJIjIZsMn5U/photo.jpg?sz=328";
	$scope.resultado = "";
	$scope.enLogin = "true";
	$scope.usuarios;
	
	/**
	* Cambio la imagen de fondo para el login.
	*/
	$rootScope.urlBackgroundImage = "app/Imagenes/Fondos/Fondo2.jpeg";
	
	
	
	/**
	* Definición de operaciones del scope.
	*/
	$scope.tryLogin = function()
	{
		usuarioValido($scope.userLogin.user, $scope.userLogin.psw, function(res)
			{
				if(res.result)
				{
					window.localStorage.setItem("user", $scope.userLogin.user);
					window.localStorage.setItem("userIsAdmin", res.is_admin);
					console.log("res.is_admin", res.is_admin);
					if(window.localStorage.getItem("userIsAdmin") == "true")
						$state.transitionTo('mainPage');
					else
						$state.transitionTo('verEquipo', {id:0});
					
				}
				else
				{
					$scope.error = "No se pudo loguear.";
					$timeout(function(){$scope.error="";}, 2000);
					if(!$scope.$$phase)$scope.$apply();
				}
			}
		);
	}

	/**
	* Definición de operaciones locales.
	*/
	var usuarioValido = function(usuario, psw, callback/*(boolean)*/)
	{
		$( document ).ready(function() 
		{
			console.log("Logueando: " + usuario + "/" + psw);
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/usuario/login.php",
		        data: JSON.stringify({"name":usuario, "pass":psw})
		    }).done(
		    	function( data )
			    {
			    	callback(data);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		    		console.log(jqXHR);
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}


}]);