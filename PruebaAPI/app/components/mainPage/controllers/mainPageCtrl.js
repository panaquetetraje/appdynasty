app.controller('MainPageCtrl', ['$scope','$rootScope','$state', '$stateParams','$timeout', '$http', function($scope,$rootScope,$state,$stateParams,$timeout,$http)
{
	/**
	* Autenticación de usuario
	*/
	if(!window.localStorage.getItem("user") || !window.localStorage.getItem("userIsAdmin") == "true")
	{ 	
		$timeout(function() 
			{
				console.log("NO ES USUARIO");
				$state.transitionTo('login');
			}, 100);
		
	}

	console.log("MainPageCtrl :: iniciando");
	
	/**
	* Declaración e inicialización de variables del scope.
	*/
	$scope.Math = window.Math;

	$scope.infoAdmin = {};
	$scope.infoAdmin.usuario = window.localStorage.getItem("user");
	$scope.infoAdmin.clave = "";

	$scope.filtroJugador = {};
	$scope.filtroJugador.filtroId = "";
	$scope.filtroJugador.filtroNombre = "";
	$scope.filtroJugador.filtroValorActualMin = 0;
	$scope.filtroJugador.filtroValorActualMax = 100;
	$scope.filtroJugador.filtroValorCompraMin = 0;
	$scope.filtroJugador.filtroValorCompraMax = 100;
	$scope.filtroJugador.filtroEquipoActual = "";

	$scope.pagInicial = 1;
	$scope.pagFinal = 1;
	$scope.jugadoresMostrados = [];
	$scope.equipoSeleccionado = {};
	$scope.usuarioSeleccionado = {};
	$scope.funcionalidadSeleccionada = "Simular Intercambio";
	$scope.equipos = [];
	$scope.equipos_asignados = [];
	$scope.usuarios = [];

	$scope.campeonatos = [];
	$scope.campeonato_seleccionado = null;
	$scope.etapas_campeonato = [];
	$scope.etapa_seleccionada = {};
	$scope.num_fecha_a_habilitar = "";
	$scope.dia_habilitacion = "";

	$scope.infoCrearUsuario = {};
	$scope.infoCrearUsuario.usuario = "";
	$scope.infoCrearUsuario.clave = "";
	$scope.infoCrearUsuario.repetirClave = "";
	$scope.infoCrearEquipo = {};
	$scope.infoCrearEquipo.name = "";
	$scope.infoCrearEquipo.top_budget = 850;
	$scope.infoCrearEquipo.perrunflas_budget = 650;
	$scope.infoCrearEquipo.owner = "";


	$scope.infoSimularIntercambio = {};
	$scope.infoSimularIntercambio.simulacionHecha = false;

	$scope.infoSimularIntercambio.top = {};
	$scope.infoSimularIntercambio.top.presupuesto = "866";
	$scope.infoSimularIntercambio.top.sumaJugadores = "866";
	$scope.infoSimularIntercambio.top.cantidadIntercambios = "3";
	$scope.infoSimularIntercambio.top.valorJugadorAComprar = [76,78,78];
	$scope.infoSimularIntercambio.top.overallJugadorAVender = [76,78,78];
	$scope.infoSimularIntercambio.top.pagadoJugadorAVender = [79,76,76];
	$scope.infoSimularIntercambio.top.resultadoIntercambio = [];
	$scope.infoSimularIntercambio.top.falloElIntercambio = false;
	$scope.infoSimularIntercambio.top.tope = 864;

	$scope.infoSimularIntercambio.perrunfla = {};
	$scope.infoSimularIntercambio.perrunfla.presupuesto = "650";
	$scope.infoSimularIntercambio.perrunfla.sumaJugadores = "650";
	$scope.infoSimularIntercambio.perrunfla.cantidadIntercambios = "1";
	$scope.infoSimularIntercambio.perrunfla.valorJugadorAComprar = [72];
	$scope.infoSimularIntercambio.perrunfla.overallJugadorAVender = [72];
	$scope.infoSimularIntercambio.perrunfla.pagadoJugadorAVender = [74];
	$scope.infoSimularIntercambio.perrunfla.resultadoIntercambio = [];
	$scope.infoSimularIntercambio.perrunfla.falloElIntercambio = false;
	$scope.infoSimularIntercambio.perrunfla.tope = 664;

	$scope.periodoDeTraspasos = {};
	$scope.periodos = [];
	
	$scope.fecha_para_listar_partidos_pendientes = '';
	$scope.infoListarPartidosPendientes = {};
	$scope.infoListarPartidosPendientes.partidos_pendientes = [];

	$scope.seccion_reglamento_indice = "";
	$scope.seccion_reglamento_titulo = "";
	$scope.seccion_reglamento_contenido = "";
	$scope.secciones_reglamento = [];

	$rootScope.urlBackgroundImage = "";
	/**
	* Declaración e inicialización de variables locales.
	*/
	var cmbSeleccionarFuncionalidad = angular.element( document.querySelector( '#seleccionarFuncionalidad' ) )[0];
	var cmbSeleccionarEquipos = angular.element( document.querySelector( '#cmbSeleccionarEquipo' ) )[0];
	var SOFIFA_API = "https://sofifa-api.herokuapp.com/api/v1/players/";
	var alertResultados = $('#alertResultados');

	/**
	* Definición de operaciones del scope.
	*/
	$scope.crearNuevoPeriodo = function()
	{
		$( document ).ready(function()
		{
			var datosUsuario =
				{
					"user": $scope.infoAdmin.usuario, 
					"pass": $scope.infoAdmin.clave
				};
			var periodo = {
				diaInicio: $scope.periodoDeTraspasos.diaInicio,
				mesInicio: $scope.periodoDeTraspasos.mesInicio,
				anioInicio: $scope.periodoDeTraspasos.anioInicio,
				diaFin: $scope.periodoDeTraspasos.diaFin,
				mesFin: $scope.periodoDeTraspasos.mesFin,
				anioFin: $scope.periodoDeTraspasos.anioFin
			};
			var solicitud = {periodo:periodo, userData:datosUsuario};
			console.log("Enviando para la bd:", solicitud);
			
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/admin/create_periodo.php",
		        data: JSON.stringify(solicitud)
		    }).done(
		    	function( data )
			    {
			    	console.log("Período creado", data.message);
			    	alert("Período creado");
			    	cargarPeriodos();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Período creado: " + textStatus );
			    	alert("ERROR: Período no creado: " + textStatus);
		    	}
		    );
		});
	}

	$scope.modificarPresupuesto = function()
	{
		
		$( document ).ready(function()
		{
			var id = $scope.equipo_seleccionado.id;
			var presupuesto_top=$scope.modificarPresupuesto_top_nuevo;
			var presupuesto_perrunfla=$scope.modificarPresupuesto_perrunfla_nuevo;

			console.log( "ID " + id );

			var datosUsuario =
				{
					"id": id, 
					"presupuesto_top": presupuesto_top,
					"presupuesto_perrunfla": presupuesto_perrunfla

				};
			console.log("Enviando para la bd:", datosUsuario);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/equipo/update_budget.php",
		        data: JSON.stringify(datosUsuario)
		    }).done(
		    	function( data )
			    {
			    	console.log("Se actualizo el presupuesto", data.message);
			    	alert("Se actualizo el presupuesto");
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
			    	alert("ERROR: No se pudo actualizo el presupuesto: " + textStatus);
		    	}
		    );
		});
	}

	$scope.cargarAnual = function()
	{
		console.log($scope.id_apertura);
		console.log($scope.id_clausura);
		console.log($scope.id_anual);
		
	}		

	$scope.seCambioEquipo = function()
	{
		$scope.modificarPresupuesto_top_actual= parseFloat($scope.equipo_seleccionado.top_budget);
		$scope.modificarPresupuesto_top_ganado= parseFloat(0);
		$scope.modificarPresupuesto_top_nuevo= parseFloat($scope.equipo_seleccionado.top_budget);

		$scope.modificarPresupuesto_perrunfla_actual= parseFloat($scope.equipo_seleccionado.perrunflas_budget);
		$scope.modificarPresupuesto_perrunfla_ganado= parseFloat(0);
		$scope.modificarPresupuesto_perrunfla_nuevo= parseFloat($scope.equipo_seleccionado.perrunflas_budget);
		console.log($scope.equipo_seleccionado);
	}

	$scope.seCambioCampeonato = function()
	{
		console.log("campeonato_seleccionado: ", $scope.campeonato_seleccionado);

		$( document ).ready(function() 
		{
			swal({
				title: "Cargando etapas del campeonato, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
  				buttons: false
			});
			console.log("Cargando etapas del campeonato: " + SERVER_URL + SERVER_PORT+"/api/partidos/read_stages_for_championship.php?championship_id=" + $scope.campeonato_seleccionado.id);
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/partidos/read_stages_for_championship.php?championship_id=" + $scope.campeonato_seleccionado.id
		    }).done(
		    	function(data)
			    {
			    	swal.close();
			    	console.log("Etapas cargadas.");
			    	console.log(data);
			    	
			    	$scope.etapas_campeonato = angular.copy(data);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		    		swal({
						title: "No se pudieron cargar las etapas.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
		      		console.log( "Etapas no cargadas: " + textStatus );
		    	}
		    );
		});
	}

	$scope.seCambioGananciaTop = function()
	{
		
		$scope.modificarPresupuesto_top_nuevo= parseFloat($scope.modificarPresupuesto_top_actual)+parseFloat($scope.modificarPresupuesto_top_ganado);
		console.log($scope.equipo_seleccionado);
	}

	$scope.seCambioGananciaPerrunfla = function()
	{
		
		$scope.modificarPresupuesto_perrunfla_nuevo= parseFloat($scope.modificarPresupuesto_perrunfla_actual)+parseFloat($scope.modificarPresupuesto_perrunfla_ganado);
		console.log($scope.equipo_seleccionado);
	}

	$scope.restaurarBD = function()
	{
		$( document ).ready(function()
		{
			var datosUsuario =
				{
					"user": $scope.infoAdmin.usuario, 
					"pass": $scope.infoAdmin.clave
				};
			console.log("Enviando para la bd:", datosUsuario);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/admin/restore_database.php",
		        data: JSON.stringify(datosUsuario)
		    }).done(
		    	function( data )
			    {
			    	console.log("Base de datos restaurada", data.message);
			    	alert("Base de datos restaurada");
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
			    	alert("ERROR: Base de datos no restaurada: " + textStatus);
		    	}
		    );
		});
	}

	$scope.listarPendientes = function()
	{
		$( document ).ready(function() 
		{
			swal({
				title: "Listando partidos pendientes, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
					buttons: false
			});
		    
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/partidos/read_pending_and_enabled_matches_at_date.php?date=" + $scope.fecha_para_listar_partidos_pendientes
		    }).done(
		    	function( data )
			    {
			    	console.log("Partidos pendientes cargados: " , data);
			    	swal.close();
			    	//todosLosJugadores = angular.copy(data.records);
			    	$scope.infoListarPartidosPendientes.partidos_pendientes = angular.copy(data);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		swal.close();
		    	}
		    );
		});	
	}

	$scope.reiniciarSimulador = function()
	{
		$scope.infoSimularIntercambio.simulacionHecha = false;

		$scope.infoSimularIntercambio.top.presupuesto = "";
		$scope.infoSimularIntercambio.top.sumaJugadores = "";
		$scope.infoSimularIntercambio.top.cantidadIntercambios = "";
		$scope.infoSimularIntercambio.top.valorJugadorAComprar = [];
		$scope.infoSimularIntercambio.top.overallJugadorAVender = [];
		$scope.infoSimularIntercambio.top.pagadoJugadorAVender = [];
		$scope.infoSimularIntercambio.top.resultadoIntercambio = [];
		$scope.infoSimularIntercambio.top.falloElIntercambio = false;

		$scope.infoSimularIntercambio.perrunfla.presupuesto = "";
		$scope.infoSimularIntercambio.perrunfla.sumaJugadores = "";
		$scope.infoSimularIntercambio.perrunfla.cantidadIntercambios = "";
		$scope.infoSimularIntercambio.perrunfla.valorJugadorAComprar = [];
		$scope.infoSimularIntercambio.perrunfla.overallJugadorAVender = [];
		$scope.infoSimularIntercambio.perrunfla.pagadoJugadorAVender = [];
		$scope.infoSimularIntercambio.perrunfla.resultadoIntercambio = [];
		$scope.infoSimularIntercambio.perrunfla.falloElIntercambio = false;
	}


	$scope.simularIntercambio = function()
	{
		$scope.infoSimularIntercambio.simulacionHecha = true;
		realizarSimulacionIntercambioPara($scope.infoSimularIntercambio.top);
		realizarSimulacionIntercambioPara($scope.infoSimularIntercambio.perrunfla);
		$('#resultadoSimulacion').append(alertResultados);
		if(!$scope.$$phase)$scope.$apply();
		alertResultados.addClass('show');
	}

	$scope.betweenMinAndMax = function(min, max, prop)
	{
		return function(item)
		{
			if (parseFloat(item[prop])>=min && parseFloat(item[prop])<=max) return true;
		}
	};

	$scope.actualizarJugadoresEnBD = function()
	{
		console.log("Actualizando en bd de página " + $scope.pagInicial + " hasta " + $scope.pagFinal);
		cargarJugadoresDeSofifa($scope.pagInicial, 0);
	};
	
	$scope.desasociarDeEqipo = function(jugador)
	{
		console.log("Desasociando", jugador.name , " del equipo de " , jugador.team_id);
		$( document ).ready(function() 
		{
			var jugadorEnEquipo = 
				{
					"player_id": jugador.id, 
					"team_id": jugador.team_id
				};
			console.log("Enviando para la bd:", jugadorEnEquipo);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/remove_from_team.php",
		        data: JSON.stringify(jugadorEnEquipo)
		    }).done(
		    	function( data )
			    {
			    	console.log(data.message);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};

	$scope.asociarAEqipo = function(jugador)
	{
		console.log("Asociando", jugador.name , "al equipo de" , $scope.equipoSeleccionado);
		console.log("Valor de compra: " + jugador.price_paid);
		$( document ).ready(function() 
		{
			var jugadorEnEquipo = 
				{
					"player_id": jugador.id, 
					"team_id": $scope.equipoSeleccionado.id, 
					"price_paid": (jugador.price_paid!=0)?jugador.price_paid:65,
					"is_top": (jugador.is_top!=0)?1:0
				};
			console.log("Enviando para la bd:", jugadorEnEquipo);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/add_to_team.php",
		        data: JSON.stringify(jugadorEnEquipo)
		    }).done(
		    	function( data )
			    {
			    	console.log(data.message);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};
  	
 	$scope.logout = function()
	{
		window.localStorage.setItem("user", "");
		$state.go('/');
	}

 	$scope.crearNuevoUsuario = function()
 	{
 		console.log("Creando nuevo usuario: ", $scope.infoCrearUsuario.usuario, $scope.infoCrearUsuario.clave, $scope.infoCrearUsuario.repetirClave);

 		if($scope.infoCrearUsuario.clave != $scope.infoCrearUsuario.repetirClave)
 			alert("Las claves no coinciden.");
 		else if($scope.infoCrearUsuario.usuario == "")
 			alert("El nombre no puede ser vacio.");
 		else
 		{
			$( document ).ready(function() 
			{
				var usuarioACrear = 
					{
						"name": $scope.infoCrearUsuario.usuario, 
						"pass": $scope.infoCrearUsuario.clave, 
						"is_admin": 0
					};
				console.log("Enviando para la bd:", usuarioACrear);
			    $.ajax(
			    {
			        method: "POST",
			        url: SERVER_URL + SERVER_PORT+"/api/usuario/create.php",
			        data: JSON.stringify(usuarioACrear)
			    }).done(
			    	function( data )
				    {
				    	console.log(data.message);
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});	 	
		}	
 	}
	
	$scope.crearNuevoEquipo = function()
 	{
 		console.log("Creando nuevo equipo: ", $scope.infoCrearEquipo.name, $scope.infoCrearEquipo.top_budget, $scope.infoCrearEquipo.perrunflas_budget, $scope.infoCrearEquipo.owner.name);

 		if($scope.infoCrearEquipo.name == "" || $scope.infoCrearEquipo.top_budget == "" || $scope.infoCrearEquipo.perrunflas_budget == "" || $scope.infoCrearEquipo.owner.name == "")
 			alert("No puede haber datos vacios.");
 		else
 		{
			$( document ).ready(function() 
			{
				var equipoACrear = 
					{
						"name": $scope.infoCrearEquipo.name,
						"top_budget": $scope.infoCrearEquipo.top_budget,
						"perrunflas_budget": $scope.infoCrearEquipo.perrunflas_budget,
						"owner": $scope.infoCrearEquipo.owner.name
					};
				console.log("Enviando para la bd:", equipoACrear);
			    $.ajax(
			    {
			        method: "POST",
			        url: SERVER_URL + SERVER_PORT+"/api/equipo/create.php",
			        data: JSON.stringify(equipoACrear)
			    }).done(
			    	function( data )
				    {
				    	console.log(data.message);
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});	 	
		}	
 	}
			    	
	/**
	* Definición de operaciones locales.
	*/

	var realizarSimulacionIntercambioPara = function(infoIntercambio)
	{
		var presupuesto = infoIntercambio.presupuesto;
		var sumaJugadores = infoIntercambio.sumaJugadores;
		var resto = infoIntercambio.presupuesto - infoIntercambio.sumaJugadores;

		var cantIntercambios = infoIntercambio.cantidadIntercambios;
		var valorJugadorAComprar 	=  infoIntercambio.valorJugadorAComprar;
		var overallJugadorAVender 	=  infoIntercambio.overallJugadorAVender;
		var pagadoJugadorAVender 	=  infoIntercambio.pagadoJugadorAVender;		
		var valorVentaJugador 	=  [];
		var gananciaJT 			=  [];
		var costoIntercambio 	=  [];
		infoIntercambio.resultadoIntercambio	 	=  [];

		for(var i = 0; i < cantIntercambios; i++)
			infoIntercambio.resultadoIntercambio[i] = {fallo: false, motivo:"N / A"};


	    var sumaCompra = parseFloat(valorJugadorAComprar[0]) + parseFloat(valorJugadorAComprar[1]) + parseFloat(valorJugadorAComprar[2]);

	    for (var i = 0; i < cantIntercambios; i++)
	    {
	    	resto = (presupuesto - sumaJugadores); // cuánto me queda para gastar arriba del presupuesto.
	    	var crecimiento = overallJugadorAVender[i] - pagadoJugadorAVender[i]; // crecimiento o decrecimiento
	    	var faltanteParaTopear = Math.max(0, parseFloat(infoIntercambio.tope) - (presupuesto) );
	    	if (crecimiento  > faltanteParaTopear) // Me paso del tope.
				gananciaJT[i] = faltanteParaTopear + 1;
			else
				gananciaJT[i] = parseFloat(crecimiento);
			
			valorVentaJugador[i] = parseFloat(pagadoJugadorAVender[i]) + parseFloat(gananciaJT[i]); // el valor al que realmente voy a vender al jugador

			costoIntercambio[i] = valorJugadorAComprar[i] - valorVentaJugador[i]; // cuánto me sale la compra

			if(resto - costoIntercambio[i] >= 0)
			{
			   // Es válido el bloque de traspasos.
			   var presupuestoViejo = presupuesto;
			   presupuesto = parseFloat(presupuestoViejo) + parseFloat(gananciaJT[i]);
			   sumaJugadores = parseFloat(sumaJugadores) - parseFloat(pagadoJugadorAVender[i]) + parseFloat(valorJugadorAComprar[i]);
			   infoIntercambio.resultadoIntercambio[i].motivo = "Compró uno a " + valorJugadorAComprar[i] + " y vendió a "  + valorVentaJugador[i] + " uno que le valió " + pagadoJugadorAVender[i] + ", con un resto de " + resto + " y un presupuesto de " + presupuestoViejo + ". Nuevo presupuesto: " + presupuesto + ", nuevo resto: " + (presupuesto - sumaJugadores);
			}
			else
			{
			  infoIntercambio.resultadoIntercambio[i].fallo = true;
			  infoIntercambio.resultadoIntercambio[i].motivo = "Quiso comprar a " + valorJugadorAComprar[i] + " y vender a "  + valorVentaJugador[i] + " uno que le valió " + pagadoJugadorAVender[i] + ", con un resto de " + resto + " y un presupuesto de " + presupuestoViejo + ". Por esto la cuenta daba: " + (resto - costoIntercambio[i]) + ".";
			}

	    }



	    // TERMINADO ALGORITMO, REPORTE:
	    infoIntercambio.falloElIntercambio = false;
	    for (var i = 0; i < cantIntercambios; ++i)
	    {
	    	if(infoIntercambio.resultadoIntercambio[i].fallo)
	    	{
	    		console.log("fallo el intercambio ", (i+1));
	    		infoIntercambio.falloElIntercambio = true;
	    	}
	    	else
	    		console.log("salio bien el", (i+1));
	    }
	    if(!infoIntercambio.falloElIntercambio)
	    	console.log("Se hicieron los intercambios. Presupuesto final: ", presupuesto);
	    else
	    	console.log("No se hicieron los intercambios.");
	}

	var obtenerJugadorLimpio = function(jugador)
	{
		return {
			id: jugador.id,
			name: jugador.name,
			overall_rating:jugador.overall_rating
		}
	}

	var crearJugadorEnBd = function(jugador)
	{
		$( document ).ready(function() 
		{
			//console.log("Enviando para la bd:", jugador);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/create.php",
		        data: JSON.stringify(jugador)
		    }).done(
		    	function( data )
			    {
			    	console.log("Se creó el jugador: ", jugador);
			    	$scope.jugadoresMostrados.push(jugador);
			    	
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		//console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	}

	var actualizarJugadorEnBd = function(jugador)
	{
		$( document ).ready(function() 
		{
			//console.log("Enviando para la bd:", jugador);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/update.php",
		        data: JSON.stringify(jugador)
		    }).done(
		    	function( data )
			    {
			    	//console.log(data);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		//console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	}

	var cargarJugadoresDeSofifa = function(pageNumber, intentos)
	{
		var urlACargar = SOFIFA_API + "?page=" + pageNumber;
		console.log("CARGANDO DE SOFIFA: ", urlACargar);
		$( document ).ready(function() 
		{
		    $.ajax(
		    {
		        method: "GET",
		        url: urlACargar
		    }).done(
		    	function( data )
			    {
			    	var algunoFaltabaAgregar = false;
			    	for(jugador of data.results)
			    	{
			    		//$scope.jugadores.push(jugador);
			    		jugador.id = jugador.photo_url.substring(jugador.photo_url.lastIndexOf("/")+1, jugador.photo_url.lastIndexOf("."));

			    		for(skill in jugador.skills)
			    		{
			    			jugador[skill] = jugador.skills[skill];
			    		}
			    		delete (jugador.skills);
			    		delete (jugador.social);
			    		switch(jugador.preferred_foot)
			    		{
			    			case "Left":
			    				jugador.preferred_foot = 'L';
			    			break;
			    			case "Right":
			    				jugador.preferred_foot = 'R';
			    			break;
			    			default:
			    				jugador.preferred_foot = 'B';
			    			break;
			    		}
			    		jugador.real_face = (jugador.real_face == 'Yes');
			    		//console.log("Por crear o actualizar jugador", jugador);

			    		jugador = obtenerJugadorLimpio(jugador);

						var jugadorExistente = $scope.jugadoresMostrados.find(
							function(jugadorEnArray)
							{
								return jugadorEnArray.id == jugador.id;
							}
						);

			    		if(jugadorExistente != null)
			    		{
			    			//console.log("jugadorExistente.name != jugador.name", jugadorExistente.name != jugador.name);
			    			//console.log("jugadorExistente.overall_rating != jugador.overall_rating", jugadorExistente.overall_rating != jugador.overall_rating);
			    			var huboCambios = 
		    					jugadorExistente.name != jugador.name ||											
								jugadorExistente.overall_rating != jugador.overall_rating											
								;
			    			//if(!huboCambios)
				    			//console.log("Jugador ya existe y quedó igual.");
				    		//else
				    		if(huboCambios)
				    		{
				    			console.log("---- Jugador " + jugador.name + " ya existe y fue cambiado---.");
				    			actualizarJugadorEnBd(jugador);
				    		}
			    		}
			    		else
			    		{
			    			console.log("Jugador no existe.", jugador);
			    			crearJugadorEnBd(jugador);
			    		}
			    		algunoFaltabaAgregar = 
			    			algunoFaltabaAgregar 
			    			|| (jugadorExistente == null)
			    			|| huboCambios
			    		;
			    	}
			    	if(!$scope.$$phase)$scope.$apply();

			    	if(algunoFaltabaAgregar)
			    		if(intentos < 10)
			    			$timeout(function() 
								{
									cargarJugadoresDeSofifa(parseInt(pageNumber), intentos+1)
								}, 2000 + 1000*intentos);
			    		else
			    		{
			    			console.log("HAY PROBLEMAS CARGANDO LA PÁGINA");
			    			console.log(pageNumber);
			    			console.log("DETENIENDO EL CARGADO.");
			    		}	
			    	else if(parseInt(pageNumber) < parseInt($scope.pagFinal))
			    		cargarJugadoresDeSofifa(parseInt(pageNumber)+1, 0);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );

		});
	};

	var cargarJugadoresDeBD = function()
	{
		$( document ).ready(function() 
		{
			console.log("Cargando jugadores de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/read.php"
		    }).done(
		    	function( data )
			    {
			    	console.log("Jugadores cargados: " , data.records);
			    	//todosLosJugadores = angular.copy(data.records);
			    	$scope.jugadoresMostrados = angular.copy(data.records);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};

	var cargarPeriodos = function()
	{

		$( document ).ready(function() 
		{
			console.log("Cargando periodos de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/periodo/read.php"
		    }).done(
		    	function( data)
			    {
			    	console.log("Periodos cargados: " , data.records);
			    	$scope.periodos = angular.copy(data.records);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	};

	var cargarEquipos = function()
	{

		$( document ).ready(function() 
		{
			console.log("Cargando equipos de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/equipo/read.php"
		    }).done(
		    	function( data)
			    {
			    	

			    	for (var i = 0; i < data.records.length; i++) {
			    		$scope.equipos.push(data.records[i]);
			    		if(data.records[i].id>0){
			    			$scope.equipos_asignados.push(data.records[i]);
			    		}

			    	}	

			    	console.log("Equipos cargados: " , $scope.equipos);
			    	console.log("Equipos asignados: " , $scope.equipos_asignados);
			    	

			    	//$scope.equipos = angular.copy(data.records);
			    	cmbSeleccionarEquipos.selectedIndex = -1;
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	};


	var cargarCampeonatos = function()
	{
		$( document ).ready(function() 
		{
			console.log("Cargando campeonatos de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/partidos/read_championships.php"
		    }).done(
		    	function(data)
			    {
			    	console.log("Campeonatos cargados.");
			    	console.log(data);
			    	
			    	$scope.campeonatos = angular.copy(data);
			    	// cmbSeleccionarEquipos.selectedIndex = -1;
			    	// if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Campeonatos no cargados: " + textStatus );
		    	}
		    );
		});
	};

	var cargarUsuarios = function()
	{

		$( document ).ready(function() 
		{
			console.log("Cargando usuarios de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/usuario/read.php"
		    }).done(
		    	function( data)
			    {
			    	console.log("Usuarios cargados: " , data.records);
			    	$scope.usuarios = angular.copy(data.records);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};

	var cargarSeccionesDelReglamento = function()
	{
		$( document ).ready(function() 
		{
			console.log("Cargando secciones del reglamento de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/reglamento/listar_todas_las_secciones.php"
		    }).done(
		    	function( data)
			    {
			    	console.log("Secciones cargadas: " , data);
			    	$scope.secciones_reglamento = angular.copy(data);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Error: Secciones no cargadas de la BD: " + textStatus );
		    	}
		    );
		});	
	};

	$scope.habilitarFecha = function()
	{
		$( document ).ready(function()
		{
			var solicitud = {
				id_campeonato: $scope.campeonato_seleccionado.id, 
				id_etapa: $scope.etapa_seleccionada.id,
				num_fecha_a_habilitar: $scope.num_fecha_a_habilitar,
				dia_habilitacion: $scope.dia_habilitacion
			};
			console.log("Enviando para la bd:", solicitud);
			swal({
				title: "Habilitando fecha, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
					buttons: false
			});
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/partidos/ingresar_nueva_habilitacion.php",
		        data: JSON.stringify(solicitud)
		    }).done(
		    	function( data )
			    {
			    	swal({
						title: "Se habilitó la fecha correctamente.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		    		swal({
						title: "No se pudo habilitar la fecha.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
		    	}
		    );
		});	

	}

	$scope.editarSeccionReglamento = function(indiceSeccionAEditar)
	{
		console.log("Editando: " + indiceSeccionAEditar);
		for (var i = $scope.secciones_reglamento.length - 1; i >= 0; i--) 
		{
			if($scope.secciones_reglamento[i].indice == indiceSeccionAEditar)
			{
				$scope.seccion_reglamento_indice = $scope.secciones_reglamento[i].indice;
				$scope.seccion_reglamento_titulo = $scope.secciones_reglamento[i].titulo;
				$scope.seccion_reglamento_contenido = $scope.secciones_reglamento[i].contenido;
			}
		}
		$scope.funcionalidadSeleccionada = "modificar-seccion-reglamento-editar";
	}


	$scope.ingresarSeccionReglamento = function()
	{
		$( document ).ready(function()
		{
			var solicitud = {
				indice: $scope.seccion_reglamento_indice, 
				titulo: $scope.seccion_reglamento_titulo,
				contenido: $scope.seccion_reglamento_contenido
			};
			console.log("Enviando para la bd:", solicitud);
			swal({
				title: "Creando sección en el reglamento, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
				buttons: false
			});
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/reglamento/ingresar_nueva_seccion.php",
		        data: JSON.stringify(solicitud)
		    }).done(
		    	function( data )
			    {
			    	swal({
						title: "Se creó la sección correctamente.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		    		swal({
						title: "No se pudo crear la sección.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
		    	}
		    );
		});	
	}

	$scope.actualizarSeccionReglamento = function()
	{
		$( document ).ready(function()
		{
			var solicitud = {
				indice: $scope.seccion_reglamento_indice, 
				titulo: $scope.seccion_reglamento_titulo,
				contenido: $scope.seccion_reglamento_contenido
			};
			console.log("Enviando para la bd:", solicitud);
			swal({
				title: "Actualizando sección en el reglamento, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
				buttons: false
			});
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/reglamento/actualizar_seccion.php",
		        data: JSON.stringify(solicitud)
		    }).done(
		    	function( data )
			    {
			    	swal({
						title: "Se actualizó la sección correctamente.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
					$scope.funcionalidadSeleccionada = "modificar-seccion-reglamento";
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		    		swal({
						title: "No se pudo actualizar la sección.",
						closeOnClickOutside: true,
						icon: "info",
		  				showCancelButton: false
					});
		    	}
		    );
		});	
	}

	/**
	* Carga inicial de datos.
	*/
	cargarCampeonatos();
	cargarUsuarios();
	cargarEquipos();
	cargarJugadoresDeBD();
	cargarPeriodos();
	cargarSeccionesDelReglamento();
}]);