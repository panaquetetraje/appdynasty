app.controller('ReglamentoCtrl', ['$scope','$state', '$stateParams','$timeout', function($scope,$state,$stateParams,$timeout)
{
	/**
	* Declaración e inicialización de variables del scope.
	*/
	if(!$scope.secciones_reglamento)
		$scope.secciones_reglamento = [];
	$scope.ver_seccion_reglamento = {"indice":"Índice", "titulo":"Título", "contenido": "Contenido"};
	
	/**
	* Definición de variables locales
	*/
	var indiceSeccionAMostrar = $stateParams.indice;
	

	/**
	* Definición de operaciones del scope.
	*/
	$scope.verSeccionReglamento = function(indice)
	{
		cargarSeccionMostrada(indice);
		$state.transitionTo('verSeccionReglamento', {indice:indice});
	}

	/**
	* Definición de operaciones locales.
	*/
	var cargarSeccionesDelReglamento = function()
	{
		$( document ).ready(function() 
		{
			console.log("Cargando secciones del reglamento de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/reglamento/listar_todas_las_secciones.php"
		    }).done(
		    	function( data)
			    {
			    	console.log("Secciones cargadas: " , data);
			    	$scope.secciones_reglamento = angular.copy(data);


			    	if(indiceSeccionAMostrar)
					{
						cargarSeccionMostrada(indiceSeccionAMostrar);
					}
					else
					{
						console.log("Hay algún null", indiceSeccionAMostrar, $scope.secciones_reglamento);
					}
					
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Error: Secciones no cargadas de la BD: " + textStatus );
		    	}
		    );
		});	
	};

	var cargarSeccionMostrada = function(indice)
	{
		for (var i = $scope.secciones_reglamento.length - 1; i >= 0; i--) {
			console.log($scope.secciones_reglamento[i]);
			if($scope.secciones_reglamento[i].indice == indice)
			{
				console.log("Copiando");
				$scope.ver_seccion_reglamento = angular.copy($scope.secciones_reglamento[i]);
			}
		}
	}


	cargarSeccionesDelReglamento();

	// if(idEquipoAMostrar != 0)
	// 	cargarInfoEquipo(idEquipoAMostrar);
	console.log("ReglamentoCtrl :: Cargada");
}]);