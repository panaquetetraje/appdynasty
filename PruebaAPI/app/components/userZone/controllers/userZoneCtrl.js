app.controller('UserZoneCtrl', ['$scope','$rootScope','$state', '$stateParams','$timeout', '$http', function($scope,$rootScope,$state,$stateParams,$timeout,$http)
{
	/**
	* Autenticación de usuario
	*/
	if(!window.localStorage.getItem("user"))
	{ 	
		$timeout(function()
			{
				$state.go('login');
			}, 100);
		
	}

	console.log("UserZoneCtrl :: iniciando");
	
	/**
	* Declaración e inicialización de variables del scope.
	*/
	$scope.Math = window.Math;
	$scope.usuarioLogueado = {};
	$scope.usuarioLogueado.name = window.localStorage.getItem("user");
   	$scope.usuarioLogueado.equipo = {};
	$scope.usuarioLogueado.equipo.id = "";
	$scope.usuarioLogueado.equipo.name = "";
	$scope.usuarioLogueado.equipo.top_budget = 0;
	$scope.usuarioLogueado.equipo.perrunflas_budget = 0;
	$scope.filtroJugador = {};
	$scope.filtroJugador.filtroId = "";
	$scope.filtroJugador.filtroNombre = "";
	$scope.filtroJugador.filtroValorActualMin = 0;
	$scope.filtroJugador.filtroValorActualMax = 100;
	$scope.filtroJugador.filtroValorCompraMin = 0;
	$scope.filtroJugador.filtroValorCompraMax = 100;
	$scope.filtroJugador.filtroEquipoActual = "";
	$scope.jugadoresMostrados = [];
	$scope.jugadoresDelUsuario = {};
	$scope.jugadoresDelUsuario.tops = [];
	$scope.jugadoresDelUsuario.perrunflas = [];
	$scope.todosLosJugadores = [];
	$scope.equipos = [];
	$scope.equipos_asignados = [];
	$scope.textoJugadorABuscar = "";
	$scope.textoEquipoABuscar = "";
	$scope.jugadoresEncontrados = [];
	$scope.jugadoresLibresEncontrados = [];
	$scope.sideBarSpaceSource = "";
	$scope.onUserLoadedCallbacks = [];
	$scope.userLoaded = false;
	$scope.equipoMostrado = {};
	$scope.periodosInfo = {};
	$scope.periodosInfo.enPeriodoDeTraspasos = false;
	$scope.periodosInfo.proximoCierre = "22/09/2019";
	$scope.periodosInfo.proximaApertura = "21/09/2019";
	
	$scope.infoTraspasosUsuarioLogueado = {};
	$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas = [];
	$scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas = [];
	$scope.infoTraspasosUsuarioLogueado.traspasos_actualizaciones = [];
	$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun = [];
	$scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo = [];
	$scope.infoTraspasosUsuarioLogueado.cantidadTraspasosPendientes = 0;
	/**
	* Guarda objetos 
		{
			onCancel:[función a llamar si cancela], 
			onConfirm:[función a llamar si confirma], 
			borrar:[true/false]
		}
	* que tienen el callback y true si deben ser borrados después de llamados.
	*/
	$scope.onCancelarModalIntercambioCallbacks = [];
	
	/**
	* Cambio la imagen de fondo para la zona de usuario.
	*/
	// $rootScope.urlBackgroundImage = "app/Imagenes/Fondos/Fondo1.jpeg";
	$rootScope.urlBackgroundImage = "";

	if(!$rootScope.$$phase)$rootScope.$apply();

	


	/**
	* Declaración e inicialización de variables locales.
	*/
	var modalIntercambio = $('#modalIntercambio');
	var miTimeout = null;
	var SOFIFA_API = "https://sofifa-api.herokuapp.com/api/v1/players/";
	


	/**
	* Definición de operaciones del scope.
	*/
	$scope.seleccionarJugadorParaComprar = function(jugador)
	{
		console.log("seleccionarJugadorParaComprar", jugador);
		$scope.infoModalIntercambios.jugadorSeleccionado = jugador;
		$scope.infoModalIntercambios.jugadorSeleccionado.idMitad1 = jugador.id.substring(0,3);
		$scope.infoModalIntercambios.jugadorSeleccionado.idMitad2 = jugador.id.substring(3,6);
		$scope.infoModalIntercambios.jugadoresEncontrados = null;
		$scope.infoModalIntercambios.textoJugadorABuscar = "";



		$scope.infoModalIntercambios.jugadorParaComprar = jugador;
		console.log("seleccionarJugadorParaComprar por ", jugador);
		if(jugador.team_id == null)
		{
			jugador.team_id = ID_MERCADO_ASIATICO;
			jugador.team_name = NOMBRE_MERCADO_ASIATICO;
		}

		$scope.infoModalIntercambios.equipoAComprarle = 
			{
				"id":jugador.team_id, 
				"name":jugador.team_name
			};
	}

	$scope.actualizarBusquedaJugador = function()
	{
		console.log($scope.infoModalIntercambios.textoJugadorABuscar.length);
		if($scope.infoModalIntercambios.textoJugadorABuscar.length>1)
		{
			window.clearTimeout(miTimeout);
			miTimeout = window.setTimeout(
				function()
				{
					$( document ).ready(function() 
					{
						console.log("Buscando jugador en la bd", $scope.infoModalIntercambios.textoJugadorABuscar);
						$.ajax(
					    {
					        method: "GET",
					        url: SERVER_URL + SERVER_PORT+"/api/jugador/search.php?s="+$scope.infoModalIntercambios.textoJugadorABuscar
					    }).done(
					    	function( data )
						    {
						    	console.log("Jugadores cargados: " , data.records);
						    	//todosLosJugadores = angular.copy(data.records);
						    	$scope.infoModalIntercambios.jugadoresEncontrados = angular.copy(data.records);
						    	if(!$scope.$$phase)$scope.$apply();
						    }
						).fail(
					    	function( jqXHR, textStatus ) 
					    	{
					      		console.log( "Request failed: " + textStatus );
					    	}
					    );
					});
				}, 2000/$scope.infoModalIntercambios.textoJugadorABuscar.length);
		}
	}


	/**
	* modalData trae la información para rellenar el modal:
	{
		title:[Título del modal],
		confirmText: [Texto del botón de confirmar]
		showBuySection: [true si debe mostrarse la sección de comprar un jugador],
		showSellSection: [análogo a showBuySection],
		showSelectPlayerToSell: [Permite elegir el jugador a vender],
		jugadorParaVender: [El jugador para vender en el intercambio],
		jugadorParaComprar: [Análogo a jugadorParaVender].
	}
	*/
	$scope.filterPeloPelo = function (valor) {
		return function (item) {
	    	//mi jugador +3 <= compra o mi jugador -3 
	    	max=parseInt($scope.infoModalIntercambios.jugadorParaComprar.overall_rating)+3;
	    	min=$scope.infoModalIntercambios.jugadorParaComprar.overall_rating-3;
	    	//console.log("val",$scope.infoModalIntercambios.jugadorParaComprar.overall_rating);
	    	//console.log("max",max);
	    	//console.log("min",min);
	    	if (item.overall_rating<=max && item.overall_rating >=min)
	        {
	            return true;
	        }
	        return false;
	    };
	};

	$scope.addModalIntercambioCallback = function(onCancel/*:function*/, onConfirm/*:function*/, modalData/*JSon*/)
	{
		modalIntercambio.modal("show");
		console.log("Si");
		$scope.infoModalIntercambios.title = modalData.title;
		$scope.infoModalIntercambios.showSellSection = modalData.showSellSection;
		$scope.infoModalIntercambios.showBuySection = modalData.showBuySection;
		$scope.infoModalIntercambios.showSelectPlayerToSell = modalData.showSelectPlayerToSell;
		$scope.infoModalIntercambios.showSelectPlayerToBuy = modalData.showSelectPlayerToBuy;
		$scope.infoModalIntercambios.showPeloPeloPlayer = modalData.showPeloPeloPlayer;
		$scope.infoModalIntercambios.showOfrecerJugador = modalData.showOfrecerJugador;


		$scope.infoModalIntercambios.jugadorParaVender = modalData.jugadorParaVender;
		$scope.infoModalIntercambios.jugadorParaComprar = modalData.jugadorParaComprar;
		$scope.infoModalIntercambios.esCompraSuperPerrunfla = modalData.esCompraSuperPerrunfla;
		$scope.infoModalIntercambios.esVentaSuperPerrunfla = modalData.esVentaSuperPerrunfla;
		$scope.infoModalIntercambios.esVentaPeloPelo = modalData.esVentaPeloPelo;

		
		console.log("$scope.infoModalIntercambios", $scope.infoModalIntercambios);

		if($scope.infoModalIntercambios.jugadorParaComprar != null && $scope.infoModalIntercambios.jugadorParaComprar.team_id == null)
		{
			$scope.infoModalIntercambios.jugadorParaComprar.team_id = ID_MERCADO_ASIATICO;
			$scope.infoModalIntercambios.jugadorParaComprar.team_name = NOMBRE_MERCADO_ASIATICO;
		}

		if(!$scope.$$phase)$scope.$apply();

		console.log("addModalIntercambioCallback", $scope.infoModalIntercambios);
		$scope.onCancelarModalIntercambioCallbacks.push(
			{
				"onCancel": onCancel,
				"onConfirm": onConfirm,
				"borrar": true
			});
	}

	$scope.confirmarModalIntercambio = function()
	{
		console.log("confirmarModalIntercambio", $scope.infoModalIntercambios, $scope.infoModalIntercambios.jugadorParaComprar);
		if($scope.infoModalIntercambios.showSelectPlayerToBuy && !$scope.infoModalIntercambios.jugadorParaComprar)
		{
			alert("Debés elegir un jugador para comprar.");
		}
		else
		{
			var resultado = angular.copy($scope.infoModalIntercambios);
			limpiarInformacionIntercambio();
			console.log("resultado2", resultado);
			modalIntercambio.on('hidden.bs.modal', function () {
				console.log("resultado3", resultado);
				llamarCallbacksYLimpiar("onConfirm", resultado);
			});
			modalIntercambio.modal("hide");
		}
		
	}

	$scope.cancelarModalIntercambio = function()
	{
		limpiarInformacionIntercambio();
		modalIntercambio.on('hidden.bs.modal', function () {
   			llamarCallbacksYLimpiar("onCancel");
		});
		modalIntercambio.modal("hide");
	}

	$scope.addUserLoadedCallback = function(callback/*()*/)
	{
		if(!$scope.userLoaded)
		{
			$scope.onUserLoadedCallbacks.push(callback);
		}
		else
			callback();
	}

	$scope.buscar = function()
	{
		console.log("Buscando " + $scope.textoJugadorABuscar);
		if($scope.textoJugadorABuscar != "")
		{
			$( document ).ready(function() 
			{
				console.log("Buscando jugador en la bd", $scope.textoJugadorABuscar, SERVER_URL + SERVER_PORT+"/api/jugador/search.php?s="+$scope.textoJugadorABuscar);
				$.ajax(
			    {
			        method: "GET",
			        url: SERVER_URL + SERVER_PORT+"/api/jugador/search.php?s="+$scope.textoJugadorABuscar
			    }).done(
			    	function( data )
				    {
				    	//console.log("Jugadores cargados: " , data.records);
				    	//todosLosJugadores = angular.copy(data.records);
				    	$scope.jugadoresEncontrados = angular.copy(data.records);
				    	if(!$scope.$$phase)$scope.$apply();
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});
		}	
	}
	
	$scope.listarJugadoresLibres = function()
	{
		console.log("Listando libres: ", $scope.minimoDeJugadorLibre, $scope.maximoDeJugadorLibre);
		if($scope.minimoDeJugadorLibre != "" && $scope.maximoDeJugadorLibre != "")
		{
			$( document ).ready(function() 
			{
				console.log("Listando libres de la BD", $scope.minimoDeJugadorLibre + "-" + $scope.maximoDeJugadorLibre, SERVER_URL + SERVER_PORT+"/api/jugador/listar_jugadores_libres.php?min="+$scope.minimoDeJugadorLibre+"&max=" + $scope.maximoDeJugadorLibre);
				$.ajax(
			    {
			        method: "GET",
			        url: SERVER_URL + SERVER_PORT+"/api/jugador/listar_jugadores_libres.php?min="+$scope.minimoDeJugadorLibre+"&max=" + $scope.maximoDeJugadorLibre
			    }).done(
			    	function( data )
				    {
				    	console.log("Jugadores libres cargados: " , data.records);
				    	//todosLosJugadores = angular.copy(data.records);
				    	$scope.jugadoresLibresEncontrados = angular.copy(data.records);
				    	if(!$scope.$$phase)$scope.$apply();
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});
		}	
	}
	

	$scope.irAVerEquipo = function(id)
	{
		$state.transitionTo('verEquipo', {id:id});
	}
	
	$scope.habilitarTraspasos = function()
	{
		$state.transitionTo('traspasos');
	}
	
	$scope.habilitarBuscarJugador = function()
	{
		$scope.sideBarSpaceSource = "/app/components/userZone/views/buscarJugador.html";
	}

	$scope.habilitarVerOtroEquipo = function()
	{
		console.log($scope.equipos);
		$scope.sideBarSpaceSource = "/app/components/userZone/views/verOtroEquipo.html";
	}
	
	$scope.habilitarProximosPartidos = function()
	{
		$state.transitionTo('proximosPartidos', {id:$scope.usuarioLogueado.equipo.id});
	}

	$scope.habilitarVerReglamento = function()
	{
		$state.transitionTo('reglamento', {id:$scope.usuarioLogueado.equipo.id});
	}
	
	$scope.habilitarVerJugadoresLibres = function()
	{
		$scope.sideBarSpaceSource = "/app/components/userZone/views/verJugadoresLibres.html";
	}

	$scope.betweenMinAndMax = function(min, max, prop)
	{
		return function(item)
		{
			if (parseFloat(item[prop])>=min && parseFloat(item[prop])<=max) return true;
		}
	};
  	
 	$scope.logout = function()
	{
		window.localStorage.setItem("user", "");
		$state.go('/');
	}
	
	$scope.colapseSidebar = function($event)
	{
		console.log("colapseSidebar", $("#wrapper"));
		
		$("#wrapper").toggleClass("toggled");
	  		
	}

	$scope.cargarJugadoresDelEquipo = function(idEquipo, callback/*(jugadores)*/)
	{
		$( document ).ready(function() 
		{
			//console.log("Cargando jugadores del equipo: ",idEquipo);
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/equipo/read_team_players.php",
		        data: JSON.stringify({"id":idEquipo})
		    }).done(
		    	function( data)
			    {
			    	//console.log("Jugadores del equipo cargados: " , data.records);
			    	callback(data.records);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	};
	/**
	* Definición de operaciones locales.
	*/

	var limpiarInformacionIntercambio = function()
	{
		$scope.infoModalIntercambios = {};
		$scope.infoModalIntercambios.title = "Título del modal";
		$scope.infoModalIntercambios.confirmText = "Confirmar";
		$scope.infoModalIntercambios.showBuySection = true;
		$scope.infoModalIntercambios.showSellSection = true;
		$scope.infoModalIntercambios.showPeloPeloPlayer = true;
		$scope.infoModalIntercambios.showOfrecerJugador = true;

		$scope.infoModalIntercambios.jugadorParaComprar = {};
		$scope.infoModalIntercambios.jugadorParaComprar.isTop = false;
		$scope.infoModalIntercambios.jugadorParaComprar.overall_rating = 0;
		$scope.infoModalIntercambios.jugadorParaComprar.team_id = ID_MERCADO_ASIATICO;
		
		$scope.infoModalIntercambios.jugadorParaVender = {};
		$scope.infoModalIntercambios.jugadorParaVender.isTop = false;
		$scope.infoModalIntercambios.jugadorParaVender.overall_rating = 0;
		$scope.infoModalIntercambios.jugadorParaVender.team_id = ID_MERCADO_ASIATICO;
		
		$scope.infoModalIntercambios.diferenciaEnPresupuesto = 0;
		$scope.infoModalIntercambios.puntosTopArribaDeCompra = "";
		$scope.infoModalIntercambios.puntosTopArribaDeVenta = "";
		$scope.infoModalIntercambios.puntosPerrunflasArribaDeCompra = "";
		$scope.infoModalIntercambios.puntosPerrunflasArribaDeVenta = "";
		$scope.infoModalIntercambios.equipoAVenderle = {id:ID_MERCADO_ASIATICO};	
		$scope.infoModalIntercambios.equipoAComprarle = {id:ID_MERCADO_ASIATICO};

		$scope.infoModalIntercambios.jugadorSeleccionado = null;
		modalIntercambio.unbind();
	}

	var llamarCallbacksYLimpiar = function(callbackType/*:String*/, resultado/*:JSon*/)
	{
		for(index in $scope.onCancelarModalIntercambioCallbacks)
		{
			var callbackObject = $scope.onCancelarModalIntercambioCallbacks[index];
			callbackObject[callbackType](resultado);
			if(callbackObject.borrar == true)
			{
				$scope.onCancelarModalIntercambioCallbacks[index] = null;
				delete $scope.onCancelarModalIntercambioCallbacks[index]
				delete callbackObject;
			}	

		}
	}

	var obtenerJugadorLimpio = function(jugador)
	{
		return {
			id: jugador.id,
			name: jugador.name,
			overall_rating:jugador.overall_rating
		}
	}

	var cargarJugadoresDeBD = function()
	{
		$( document ).ready(function() 
		{
			//console.log("Cargando jugadores de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/read.php"
		    }).done(
		    	function( data )
			    {
			    	//console.log("Jugadores cargados: " , data.records);
			    	//todosLosJugadores = angular.copy(data.records);
			    	$scope.todosLosJugadores = angular.copy(data.records);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};


	var cargarCantidadTraspasos  = function(teamId)
	{
		$( document ).ready(function() 
		{
			console.log("Cargando cantidad de traspasos de BD.");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/get_amount.php?id=" + teamId
		    }).done(
		    	function( data)
			    {
			    	console.log("Cantidad de traspasos cargada: " , data);
					$scope.infoTraspasosUsuarioLogueado.cantidadTraspasosPendientes=data;
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}


	var cargarInfoPeriodo = function()
	{
		$( document ).ready(function() 
		{
			//console.log("Cargando equipos de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/periodo/get_info_periodo.php"
		    }).done(
		    	function( data)
			    {
			    	console.log("Info del período cargada: " , data.records);
					$scope.periodosInfo = {};
					$scope.periodosInfo.enPeriodoDeTraspasos = data.enPeriodoDeTraspasos;
					$scope.periodosInfo.proximoCierre = data.proximoCierre;
					$scope.periodosInfo.proximoInicio = data.proximoInicio;
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}

	var cargarEquipos = function()
	{
		$( document ).ready(function() 
		{
			//console.log("Cargando equipos de la bd");
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/equipo/read.php"
		    }).done(
		    	function( data)
			    {
			    	
			    	//$scope.equipos = angular.copy(data.records);
			    	$scope.equipos_asignados.length = 0; // Vacío el array para que sólo estén los cargados.
			    	for (var i = 0; i < data.records.length; i++) {
			    		$scope.equipos.push(data.records[i]);
			    		if(data.records[i].id>0){
			    			$scope.equipos_asignados.push(data.records[i]);
			    		}

			    	}	
			    	console.log("Equipos cargados: " , $scope.equipos);
			    	console.log("Equipos asignados: " , $scope.equipos_asignados);

			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
	

		});
	};
	var onJugadoresDelUsuarioCargados = function(jugadores)
	{
		$scope.jugadoresDelUsuario.tops = angular.copy(jugadores.filter(
				function(jugador, index, array)
				{
					return jugador.is_top != "0";
				}


			));

		$scope.jugadoresDelUsuario.perrunflas = angular.copy(jugadores.filter(
				function(jugador, index, array)
				{
					return jugador.is_top == 0;
				}

			));
		$scope.onJugadoresCargados(jugadores);
	}
	
	

	var cargarDatosDeUsuario = function(callback/*()*/)
	{

		$( document ).ready(function() 
		{
			var usuarioLogueado = 
				{
					"name": $scope.usuarioLogueado.name
				};
			//console.log("Enviando para la bd:", usuarioLogueado);
		    $.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/usuario/read_logged_user_data.php",
		        data: JSON.stringify(usuarioLogueado)
		    }).done(
		    	function( data )
			    {
			    	console.log("Cargados los datos del usuario logueado: ", data);
			    	$scope.usuarioLogueado.equipo.id = data.team_id;
			    	$scope.usuarioLogueado.equipo.name = data.team_name;
			    	$scope.usuarioLogueado.equipo.top_budget = data.top_budget;
			    	$scope.usuarioLogueado.equipo.perrunflas_budget = data.perrunflas_budget;
			    	$scope.usuarioLogueado.equipo.suma_tops = data.suma_tops;
			    	$scope.usuarioLogueado.equipo.resto_tops = (data.top_budget - data.suma_tops);
			    	$scope.usuarioLogueado.equipo.suma_perrunflas = data.suma_perrunflas;
			    	$scope.usuarioLogueado.equipo.resto_perrunflas =  (data.perrunflas_budget - data.suma_perrunflas);
			    	$scope.usuarioLogueado.equipo.valor_equipo_tops =  (parseFloat(data.suma_real_tops) + parseFloat($scope.usuarioLogueado.equipo.resto_tops));
			    	$scope.usuarioLogueado.equipo.valor_equipo_perrunflas = (parseFloat(data.suma_real_perrunflas) + parseFloat($scope.usuarioLogueado.equipo.resto_perrunflas));
			    	$scope.usuarioLogueado.equipo.logo = data.logo;
			    	$scope.usuarioLogueado.equipo.sofifa_team_uri = data.sofifa_team_uri;
			    	$scope.usuarioLogueado.equipo.canal_youtube = data.canal_youtube;
			    	$scope.usuarioLogueado.equipo.usuario_psn = data.usuario_psn;
			    	$scope.usuarioLogueado.equipo.fylyal_name = data.fylyal_name;
			    	$scope.usuarioLogueado.equipo.fylyal_id = data.fylyal_id;

			    	console.log("Datos de equipo mostrado: ", $scope.usuarioLogueado.equipo);
			    	$scope.equipoMostrado = angular.copy($scope.usuarioLogueado.equipo);
			    	/**
			    	* Llamo a todos los callbacks que se registraron para ser llamados cuando los datos del usuario logueado estén cargados.
			    	*/
			    	console.log("Llamando callbacks", $scope.onUserLoadedCallbacks);
			    	$scope.userLoaded = true;
			    	for(func of $scope.onUserLoadedCallbacks)
			    	{
			    		func();
			    	}
			    	$scope.onUserLoadedCallbacks = [];
			    	callback();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		//console.log( "Request failed: " + textStatus );
		    	}
		    );
		});	
	};

	$scope.onJugadoresCargados = function(jugadores)
	{
		console.log("mostrando jugadores del usuario: ", jugadores);
		
		$scope.jugadoresMostrados = angular.copy(jugadores.filter(
				function(jugador, index, array)
				{
					return jugador.is_top != "0";
				}


			));

		$scope.jugadoresMostrados = $scope.jugadoresMostrados.concat(angular.copy(jugadores.filter(
				function(jugador, index, array)
				{
					return jugador.is_top == 0;
				}

			)));

		

		if(!$scope.$$phase)$scope.$apply();
	};

	/**
	* Carga inicial de datos.
	*/
	$scope.cargaInicialDeDatos = function()
	{
		cargarInfoPeriodo();
		cargarEquipos();
		cargarJugadoresDeBD();
		cargarDatosDeUsuario(
			function()
			{
				cargarCantidadTraspasos($scope.usuarioLogueado.equipo.id);
				$scope.cargarJugadoresDelEquipo($scope.usuarioLogueado.equipo.id, onJugadoresDelUsuarioCargados);
			}
		);
	}

	$scope.cargaInicialDeDatos();
	limpiarInformacionIntercambio();
}]);