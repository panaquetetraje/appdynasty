app.controller('TraspasosCtrl', ['$scope','$state', '$stateParams','$timeout', '$transitions', function($scope,$state,$stateParams,$timeout,$transitions)
{
	/**
	* Declaración e inicialización de variables del scope.
	*/
	/*

	// Los siguientes se definieron en userZoneCtrl.
	$scope.infoTraspasosUsuarioLogueado = {};
	$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas = [];
	$scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas = [];
	$scope.infoTraspasosUsuarioLogueado.traspasos_actualizaciones = [];
	$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun = [];
	$scope.infoTraspasosUsuarioLogueado.cantidadTraspasosPendientes = 0;
	*/
	
	$scope.infoTraspasosUsuarioLogueado.simulacionHecha = false;
	$scope.infoTraspasosUsuarioLogueado.simulacionCadenaHecha = false;
	$scope.infoTraspasosUsuarioLogueado.simulacionSuperPerrunHecha = false;
	$scope.infoTraspasosUsuarioLogueado.simulacionPeloPelo = false;
	$scope.infoTraspasosUsuarioLogueado.simulacionActualizacionHecha = false;


	/**
	* Definición de variables locales
	*/
	var alertResultados = $('#alertResultados');
	var alertResultadosCadena = $('#alertResultadosCadena');
	var alertResultadosSuperPerrun = $('#alertResultadosSuperPerrun');
	var alertResultadosPeloPelo = $('#alertResultadosPeloPelo');
	var resultadoSimulacion = $('#resultadoSimulacion');
	var resultadoSimulacionCadena = $('#resultadoSimulacionCadena');
	var resultadoSimulacionSuperPerrun = $('#resultadoSimulacionSuperPerrun');
	var resultadoSimulacionPeloPelo = $('#resultadoSimulacionPeloPelo');
	//var modalAceptarOfertaRecibida = $('#modalAceptarOfertaRecibida');

	const tiposTraspaso = {
		CADENA: 'cadena',
		SUPER_PERRUN: 'superPerrun',
		PELO_PELO: 'PeloPelo'
	};

	
	
	/*$scope.confirmarAceptarVenta = function()
	{
		console.log("confirmarAceptarVenta");

		var date = new Date();
		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate();


		var seller_id = (jugadorParaComprar.team_id != null)?jugadorParaComprar.team_id:ID_MERCADO_ASIATICO;
		
		var added_top_points = parseFloat(puntosTopArribaDeVenta);
		if(isNaN(added_top_points))added_top_points = 0;

		var added_perrunfla_points = parseFloat(puntosPerrunflasArribaDeVenta);
		if(isNaN(added_perrunfla_points))added_perrunfla_points = 0;
		
		console.log("added_top_points", added_top_points);
		console.log("added_perrunfla_points", added_perrunfla_points);

		var compraACrear = 
			{
			    "buyer_id": $scope.usuarioLogueado.equipo.id,
			    "player_id": jugadorParaComprar.id,
			    "seller_id": seller_id,
			    "sell_date": fecha,
			    "sell_price": jugadorParaComprar.overall_rating,
			    "is_pending": 1,
			    "added_top_points": added_top_points,
			    "added_perrunflas_points": added_perrunfla_points,
			    "engaged_to": idCadenaTraspaso
			};

		var traspasos = [compraACrear];
		

		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Venta confirmada en la BD.", resultado);
				modalAceptarOfertaRecibida.modal('hide');
				$scope.setCadenaState(idCadenaTraspaso, 1);
			}
		);
	}
	*/
	
	$scope.confirmarTraspasoSuperPerrun = function()
	{
		$scope.confirmarCadenaDeIntercambios(tiposTraspaso.SUPER_PERRUN);
	}
	$scope.confirmarTraspasoPeloPelo = function()
	{
		$scope.confirmarCadenaDeIntercambios(tiposTraspaso.PELO_PELO);
	}

	$scope.confirmarCadenaDeIntercambios = function(tipoTraspaso = tiposTraspaso.CADENA)
	{
		var cadenas = (tipoTraspaso == tiposTraspaso.CADENA)?$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas:$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun;
		if(tipoTraspaso == tiposTraspaso.PELO_PELO){
			cadenas=$scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo;
		}
		console.log($scope.infoTraspasosUsuarioLogueado.traspasos_cadenas, $scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun);
		var traspasosAceptados = cadenas.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		console.log("traspasosAceptados", traspasosAceptados);
		//if(traspasosAceptados.length == 2)
		//{
			$( document ).ready(function() 
			{
				console.log("traspasosAceptados", traspasosAceptados);
				var checkBloqueInfo =
				{
				    "cadena_id": traspasosAceptados[0].engaged_to
				};
				$.ajax(
			    {
			        method: "POST",
			        url: SERVER_URL + SERVER_PORT+"/api/traspaso/apply_transfer_chain_if_valid.php",
			        data: JSON.stringify(checkBloqueInfo)
			    }).done(
			    	function( data)
				    {
				    	console.log("Resultado apply_transfers_if_valid: ", data);
				    	$scope.infoTraspasosUsuarioLogueado.resultadoIntercambio = data;
				    	$scope.$parent.cargaInicialDeDatos();
				    	cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
				    	$scope.reiniciarSimulador();
						if(!$scope.$$phase)$scope.$apply();
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});
		//}
		//else
		//{
		//	alert("Error: Las cadenas deben realizarse de a una (una compra y una venta), actualmente hay " + traspasosAceptados.length + " traspaso/s de cadenas aceptados POR COMPRADOR Y VENDEDOR. Asegurate de que tanto la venta como la compra que querés hacer está aceptada por vos y por el otro dynastyer.");
		//}
	}

	$scope.confirmarActualizacion = function(engaged_to)
	{
		$( document ).ready(function() 
		{
			console.log("confirmarActualizacion", engaged_to);
			console.log($scope.usuarioLogueado.equipo.id);
			var checkBloqueInfo =
				{
				    "team_id": $scope.usuarioLogueado.equipo.id,
				    "update_id": engaged_to
				}
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/apply_update.php",
		        data: JSON.stringify(checkBloqueInfo)
		    }).done(
		    	function(data)
			    {
			    	console.log("Resultado apply_update: ", data);
			    	if(data["success"] == true)
		    		{
		    			alert(data["message"]);
		    			$scope.infoTraspasosUsuarioLogueado.resultadoIntercambio = data;
				    	$scope.$parent.cargaInicialDeDatos();
				    	cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
		    		}
		    		else
		    		{
		    			alert("Error: "  + data.message);
		    		}
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, data ) 
		    	{
		      		console.log("Error.");
		    	}
		    );
		});
	}


	$scope.confirmarIntercambio = function()
	{
		var traspasosAceptados = $scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		console.log("traspasosAceptados ", traspasosAceptados);
		$( document ).ready(function() 
		{
			console.log("Realizando intercambios");
			var cadenasIds = [];
			for(traspaso of traspasosAceptados)
			{
				if(!cadenasIds.includes(parseInt(traspaso.engaged_to)))
					cadenasIds.push(parseInt(traspaso.engaged_to));
			}
			console.log($scope.usuarioLogueado.equipo.id);
			var checkBloqueInfo =
				{
				    "team_id": $scope.usuarioLogueado.equipo.id,
				    "traspasos": cadenasIds
				}
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/apply_transfers_if_valid.php",
		        data: JSON.stringify(checkBloqueInfo)
		    }).done(
		    	function( data)
			    {
			    	console.log("Resultado apply_transfers_if_valid: ", data);
			    	$scope.infoTraspasosUsuarioLogueado.resultadoIntercambio = data;
			    	$scope.$parent.cargaInicialDeDatos();
			    	cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
			    	$scope.reiniciarSimulador();
					if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}

	$scope.simularCadenaDeIntercambios = function()
	{
		var traspasosAceptados = $scope.infoTraspasosUsuarioLogueado.traspasos_cadenas.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		console.log("traspasosAceptados", traspasosAceptados);
		if(traspasosAceptados.length == 2)
		{
			$scope.infoTraspasosUsuarioLogueado.simulacionCadenaHecha = true;
			console.log("traspasosAceptados ", traspasosAceptados);
			verificarCadenaDeTraspasos(traspasosAceptados[0], tiposTraspaso.CADENA);
			resultadoSimulacionCadena.append(alertResultadosCadena);
			alertResultadosCadena.addClass('show');
			if(!$scope.$$phase)$scope.$apply();
		}
		else
		{
			alert("Error: Las cadenas deben realizarse de a una (una compra y una venta), actualmente hay " + traspasosAceptados.length + " traspaso/s de cadenas aceptados POR COMPRADOR Y VENDEDOR. Asegurate de que tanto la venta como la compra que querés hacer está aceptada por vos y por el otro dynastyer.");
		}
	}

	$scope.simularIntercambiosSuperPerrun = function()
	{
		var traspasosAceptados = $scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		console.log("traspasosAceptados", traspasosAceptados);
		if(traspasosAceptados.length == 2)
		{
			$scope.infoTraspasosUsuarioLogueado.simulacionSuperPerrunHecha = true;
			console.log("traspasosAceptados ", traspasosAceptados);
			verificarCadenaDeTraspasos(traspasosAceptados[0], tiposTraspaso.SUPER_PERRUN);
			resultadoSimulacionSuperPerrun.append(alertResultadosSuperPerrun);
			alertResultadosSuperPerrun.addClass('show');
			if(!$scope.$$phase)$scope.$apply();
		}
		else
		{
			alert("Error: Los traspasos súper perrun deben realizarse de a uno (una compra y una venta), actualmente hay " + traspasosAceptados.length + " traspaso/s súper perrun aceptados POR COMPRADOR Y VENDEDOR. Asegurate de que tanto la venta como la compra que querés hacer está aceptada por vos y por el otro dynastyer.");
		}
	}
	$scope.simularIntercambiosPeloPelo = function()
	{
		var traspasosAceptados = $scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		console.log("traspasosAceptados", traspasosAceptados);
		if(traspasosAceptados.length == 2)
		{
			$scope.infoTraspasosUsuarioLogueado.simulacionPeloPeloHecha = true;
			console.log("traspasosAceptados ", traspasosAceptados);
			verificarCadenaDeTraspasos(traspasosAceptados[0], tiposTraspaso.PELO_PELO);
			resultadoSimulacionPeloPelo.append(alertResultadosPeloPelo);
			alertResultadosPeloPelo.addClass('show');
			if(!$scope.$$phase)$scope.$apply();
		}
		else
		{
			alert("Error: Los traspasos Pelo A Pelo deben realizarse de a uno (una compra y una venta), actualmente hay " + traspasosAceptados.length + " traspaso/s pelo a pelo aceptados POR COMPRADOR Y VENDEDOR. Asegurate de que tanto la venta como la compra que querés hacer está aceptada por vos y por el otro dynastyer.");
		}
	}


	$scope.simularIntercambio = function()
	{
		var traspasosAceptados = $scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas.filter(traspaso => traspaso.seller_accepted==1 && traspaso.buyer_accepted==1);
		if(traspasosAceptados.length != 0)
		{
			$scope.infoTraspasosUsuarioLogueado.simulacionHecha = true;
			console.log("traspasosAceptados ", traspasosAceptados);
			verificarBloqueDeTraspasos(traspasosAceptados);
			resultadoSimulacion.append(alertResultados);
			alertResultados.addClass('show');
			if(!$scope.$$phase)$scope.$apply();
		}
		else
		{
			alert("Error: No hay traspasos aceptados. Tenés que aceptar para poder simular los traspasos, simularlos no los confirmará sino que después de que la simulación dé bien te permitirá confirmarlo y recién ahí se efectuarán los traspasos.");
		}
	}

	$scope.eliminarTraspaso = function(idCadenaTraspaso)
	{
		var isConfirmed = confirm("¿Seguro que desea eliminar toda la cadena " + idCadenaTraspaso + " de traspasos?");
		
		if(isConfirmed)
		{
			$( document ).ready(function() 
			{
				console.log("Borrando traspaso", idCadenaTraspaso);
				$.ajax(
			    {
			        method: "POST",
			        url: SERVER_URL + SERVER_PORT+"/api/traspaso/delete.php",
			        data: JSON.stringify({idCadenaTraspaso:idCadenaTraspaso})
			    }).done(
			    	function( data)
				    {
				    	console.log("Resultado delete traspaso: ", data);
				    	$scope.infoTraspasosUsuarioLogueado.resultadoIntercambio = data;
				    	$scope.$parent.cargaInicialDeDatos();
				    	cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
				    	$scope.reiniciarSimulador();
						if(!$scope.$$phase)$scope.$apply();
				    }
				).fail(
			    	function( jqXHR, textStatus ) 
			    	{
			      		console.log( "Request failed: " + textStatus );
			    	}
			    );
			});
		}
	}


	$scope.setSuperPerrunState = function(idCadenaTraspaso, aceptar)
	{
		$scope.setCadenaState(idCadenaTraspaso, aceptar, tiposTraspaso.SUPER_PERRUN);
	}
	$scope.setPeloPeloState = function(idCadenaTraspaso, aceptar)
	{
		$scope.setCadenaState(idCadenaTraspaso, aceptar, tiposTraspaso.PELO_PELO);
	}

	/**
	* Tipos cadena: 0 - cadena, 1 - super perrun
	*/
	$scope.setCadenaState = function(idCadenaTraspaso, aceptar, tipoTraspaso = tiposTraspaso.CADENA)
	{
		console.log("setCadenaState", tipoTraspaso);
		obtenerNecesitaAccionPreviaCambioDeEstadoDeCadena(idCadenaTraspaso, 
			function(requiereAccionPrevia)
			{
				if(requiereAccionPrevia)
				{
					mostrarModalAceptarCompra(idCadenaTraspaso, aceptar, tipoTraspaso);
				}
				else
				{
					$( document ).ready(function() 
					{
						//console.log("Cargando jugadores del equipo: ",idEquipo);
						var cadenaStateInfo =
							{
								"cadena_id": idCadenaTraspaso,
								"team_id": $scope.usuarioLogueado.equipo.id,
								"accept": aceptar
							}
						$.ajax(
					    {
					        method: "POST",
					        url: SERVER_URL + SERVER_PORT+"/api/traspaso/set_cadena_state.php",
					        data: JSON.stringify(cadenaStateInfo)
					    }).done(
					    	function( data)
						    {
						    	console.log("Resultado setCadenaState: ", data);
						    	cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
						    }
						).fail(
					    	function( jqXHR, textStatus ) 
					    	{
					      		console.log( "Request failed: " + textStatus );
					    	}
					    );
					});

				}
			});
	}

	$scope.reiniciarSimulador = function()
	{
		$scope.infoTraspasosUsuarioLogueado.simulacionHecha = false;
	   	if(!$scope.$$phase)$scope.$apply();
	}

	$scope.reiniciarSimuladorCadenas = function()
	{
		console.log("reiniciarSimuladorCadenas");
		$scope.infoTraspasosUsuarioLogueado.simulacionCadenaHecha = false;
		if(!$scope.$$phase)$scope.$apply();
	}

	$scope.reiniciarSimuladorSuperPerrun = function()
	{
		console.log("reiniciarSimuladorSuperPerrun");
		$scope.infoTraspasosUsuarioLogueado.simulacionSuperPerrunHecha = false;
		if(!$scope.$$phase)$scope.$apply();
	}
	$scope.reiniciarSimuladorPeloPelo = function()
	{
		console.log("reiniciarSimuladorPeloPelo");
		$scope.infoTraspasosUsuarioLogueado.simulacionPeloPeloHecha = false;
		if(!$scope.$$phase)$scope.$apply();
	}

	/**
	* Definición de operaciones locales.
	*/

	/*
	* Tipos cadena: 0 - cadena, 1 - super perrun
	*/
	var mostrarModalAceptarCompra = function(idCadenaTraspaso, aceptar, tipoTraspaso)
	{
		console.log("mostrarModalAceptarCompra", idCadenaTraspaso, aceptar, tipoTraspaso);

		var ofertaRecibida = obtenerTraspasoDeCadena(idCadenaTraspaso, tipoTraspaso);
		var meQuierenComprar = (ofertaRecibida.seller_id == $scope.usuarioLogueado.equipo.id);
		var modalData;

		if(meQuierenComprar)
		{
			console.log("meQuierenComprar", ofertaRecibida);
			// Soy el vendedor, entonces me ofrecieron comprarme a un jugador.
			var jugadorParaVender = 
				{
					"id": ofertaRecibida.player_id,
					"name": ofertaRecibida.player_name,
					"team_id": ofertaRecibida.seller_id,
					"team_name": ofertaRecibida.seller_name
				}
			console.log("Vendiendo", jugadorParaVender);
			
			var titulo = (tipoTraspaso == tiposTraspaso.SUPER_PERRUN)?("Te quieren comprar a " + ofertaRecibida.player_name + " como súper perrunfla. Para aceptar la venta tenés que comprar a otro jugador."):("Te quieren comprar a " + ofertaRecibida.player_name + " por "+ofertaRecibida.sell_price + ". Para aceptar la venta tenés que comprar a otro jugador.");
			modalData = 
				{
					title:titulo,
					showBuySection:true,
					showSelectPlayerToBuy:true,
					showSellSection:false,
					showSelectPlayerToSell:false,
					jugadorParaVender: jugadorParaVender,
					puntosTopArribaDeVenta: ofertaRecibida.added_top_points,
					puntosPerrunflasArribaDeVenta: ofertaRecibida.added_perrunflas_points,
					puntosTopArribaDeCompra: 0,
					puntosPerrunflasArribaDeCompra: 0
				};
		}
		else
		{
			console.log("no meQuierenComprar, me quieren vender", ofertaRecibida);
			var jugadorParaComprar = 
				{
					"id": ofertaRecibida.player_id,
					"name": ofertaRecibida.player_name,
					"team_id": ofertaRecibida.buyer_id,
					"team_name": ofertaRecibida.buyer_name
				};
			console.log("Comprando", jugadorParaComprar);
			//if(!$scope.$$phase)$scope.$apply();

			modalData = 
				{
					title:"Te quieren vender a " + ofertaRecibida.player_name + " a " + ofertaRecibida.sell_price + ". Para aceptar la compra tenés que vender a uno de tus jugadores.",
					showBuySection:false,
					showSelectPlayerToBuy:false,
					showSellSection:true,
					showSelectPlayerToSell:true,
					jugadorParaComprar: jugadorParaComprar,
					puntosTopArribaDeVenta: 0,
					puntosPerrunflasArribaDeVenta: 0,
					puntosTopArribaDeCompra: ofertaRecibida.added_top_points,
					puntosPerrunflasArribaDeCompra: ofertaRecibida.added_perrunflas_points
				};
		}
		//console.log("ofertaRecibida", ofertaRecibida);
		//puntosTopArribaDeVenta


		
		
		console.log("modalData", modalData);

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			function(result)
			{
				onConfirmModalIntercambio(result, meQuierenComprar, idCadenaTraspaso, tipoTraspaso)
			}
			, modalData
		);

	}

	var onConfirmModalIntercambio = function(result, meQuierenComprar, idCadenaTraspaso, tipoTraspaso)
	{
		console.log("Confirmado", result, tipoTraspaso);
		var date = new Date();

		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate() + " " + 
			date.getHours() + ":" + 
			date.getMinutes() + ":" + 
			date.getSeconds()
			;
		console.log("date", date);
		console.log("fecha2", fecha);

		var added_top_points_venta = parseFloat(result.puntosTopArribaDeVenta);
		if(isNaN(added_top_points_venta))added_top_points_venta = 0;

		var added_perrunflas_points_venta = parseFloat(result.puntosPerrunflasArribaDeVenta);
		if(isNaN(added_perrunflas_points_venta))added_perrunflas_points_venta = 0;
		
		var added_top_points_compra = parseFloat(result.puntosTopArribaDeCompra);
		if(isNaN(added_top_points_compra))added_top_points_compra = 0;

		var added_perrunflas_points_compra = parseFloat(result.puntosPerrunflasArribaDeCompra);
		if(isNaN(added_perrunflas_points_compra))added_perrunflas_points_compra = 0;

		/*
		console.log("added_top_points_venta", added_top_points_venta);
		console.log("added_perrunflas_points_venta", added_perrunflas_points_venta);
		console.log("added_top_points_compra", added_top_points_compra);
		console.log("added_perrunflas_points_compra", added_perrunflas_points_compra);
		*/
		var traspasos = [];
		if(meQuierenComprar)
		{
			var compraACrear = 
				{
				    "buyer_id": result.jugadorParaVender.team_id,
				    "player_id": result.jugadorParaComprar.id,
				    "seller_id": result.jugadorParaComprar.team_id,
				    "sell_date": fecha,
				    "sell_price": result.jugadorParaComprar.overall_rating,
				    "is_pending": 1,
				    "added_top_points": added_top_points_compra,
				    "added_perrunflas_points": added_perrunflas_points_compra,
			  		"engaged_to": idCadenaTraspaso,
				    "is_super_perrun": (tipoTraspaso == tiposTraspaso.SUPER_PERRUN)?1:0,
				    "is_pelo_a_pelo": (tipoTraspaso == tiposTraspaso.PELO_PELO)?1:0
				};
			traspasos.push(compraACrear);
		}
		else
		{
			var ventaACrear = 
				{
				    "buyer_id": result.equipoAVenderle.id,
				    "player_id": result.jugadorParaVender.id,
				    "seller_id": result.jugadorParaVender.team_id,
				    "sell_date": fecha,
				    "sell_price": result.jugadorParaVender.overall_rating,
				    "is_pending": 1,
				    "added_top_points": added_top_points_venta,
				    "added_perrunflas_points": added_perrunflas_points_venta,
			  		"engaged_to": idCadenaTraspaso,
				    "is_super_perrun": (tipoTraspaso == tiposTraspaso.SUPER_PERRUN)?1:0,
				    "is_pelo_a_pelo": (tipoTraspaso == tiposTraspaso.PELO_PELO)?1:0
				}
			traspasos.push(ventaACrear);
		}
		
		console.log("traspasos", traspasos);

		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Propuesta realizada en la BD", resultado);
				$scope.setCadenaState(idCadenaTraspaso, 1);

				//$state.transitionTo('traspasos');
			}
		);
	}

	var crearTraspasosEnBD = function(traspasos, callback/*(resultado)*/)
	{
		$( document ).ready(function()
		{
			console.log("Creando traspasos: ", traspasos);
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/create.php",
		        data: JSON.stringify(traspasos)
		    }).done(
		    	function( data )
			    {
			    	console.log("Traspasos creado");
			    	callback(data);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});//*/
	}

	var obtenerTraspasoDeCadena = function(idCadena, tipoTraspaso)
	{
		var cadenas = (tipoTraspaso == tiposTraspaso.SUPER_PERRUN)?$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun:$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas;
		if(tipoTraspaso == tiposTraspaso.PELO_PELO){
			cadenas=$scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo;
		}
		return angular.copy(cadenas.find(
			function(traspaso)
			{
				return parseInt(traspaso.engaged_to) == parseInt(idCadena);
			}
		));
	}


	var obtenerNecesitaAccionPreviaCambioDeEstadoDeCadena = function(idCadenaTraspaso, callback/*(necesitaAccionPrevia)*/)
	{
		$( document ).ready(function() 
		{
			//console.log("Cargando jugadores del equipo: ",idEquipo);
			var cadenaInfo =
				{
					"cadena_id": idCadenaTraspaso,
					"team_id": $scope.usuarioLogueado.equipo.id
				}
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/has_pending_action_for_team.php",
		        data: JSON.stringify(cadenaInfo)
		    }).done(
		    	function( data)
			    {
			    	console.log("Resultado obtenerNecesitaAccionPreviaCambioDeEstadoDeCadena: ", data);
			    	callback(data.hasPendingAction);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});

	}

	var cargarTraspasosDeJugador = function(idEquipo)
	{
		$( document ).ready(function() 
		{
			console.log("Cargando traspasos de la bd para el equipo con id:" + idEquipo);
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT + "/api/traspaso/read_from_team.php?team_id=" + idEquipo
		    }).done(
		    	function( data )
			    {
			    	console.log("Traspasos cargados: " , data);
			    	if(data)
			    	{
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas = data.no_chains;
				    	// console.log("Traspasos asiatico: " , data.no_chains);
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas = data.chains;
				    	// console.log("Traspasos cadenas: " , data.chains);
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_actualizaciones = data.updates;
				    	// console.log("Traspasos actualizacion: " , data.updates);
				    	$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun = data.superPerrun;
				    	// console.log("Traspasos super: " , data.superPerrun);
				    	$scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo = data.PeloPelo;
				    	// console.log("Traspasos pelopelo: " , data.PeloPelo);
				    	if($scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo != null)
				    	{
				    		// console.log("Traspasos pelopelo: " , $scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo.length);
				    	}

				    }
				    else
				    {
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas = null;
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas = null;
				    	$scope.infoTraspasosUsuarioLogueado.traspasos_actualizaciones = null;
				    	$scope.infoTraspasosUsuarioLogueado.traspasosSuperPerrun = null;
				    	$scope.infoTraspasosUsuarioLogueado.traspasosPeloPelo = null;
				    }
			    	//$scope.jugadoresMostrados = angular.copy(data.records);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		$scope.infoTraspasosUsuarioLogueado.traspasos_no_cadenas = null;
		      		$scope.infoTraspasosUsuarioLogueado.traspasos_cadenas = null;
		    	}
		    );
		});
	}

	var verificarCadenaDeTraspasos = function(cadena, tipoTraspaso)
	{
		$( document ).ready(function() 
		{
			console.log("verificarCadenaDeTraspasos:", cadena.engaged_to);
			var checkBloqueInfo =
				{
				    "cadena_id": cadena.engaged_to
				};
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/check_valid_chain.php",
		        data: JSON.stringify(checkBloqueInfo)
		    }).done(
		    	function( data)
			    {
			    	console.log("Resultado check_valid_chain: ", data, tipoTraspaso);

			    	switch(tipoTraspaso)
			    	{
			    		case tiposTraspaso.CADENA:
			    			$scope.infoTraspasosUsuarioLogueado.resultadoSimulacionCadena = data;
			    		// break;
			    		case tiposTraspaso.SUPER_PERRUN:
			    			$scope.infoTraspasosUsuarioLogueado.resultadoSimulacionSuperPerrun = data;
			    		// break;
			    		case tiposTraspaso.PELO_PELO:
			    			$scope.infoTraspasosUsuarioLogueado.resultadoSimulacionPeloPelo = data;
			    		// break;
			    	}
			    	/**
			    	* TODO: ACÁ HAY QUE REVISAR LO QUE VUELVE Y EXPLICARLE AL USUARIO QUÉ FUE LO QUE PASÓ, PUEDE HABER SALIDO BIEN SU PARTE DE LA CADENA Y MAL LA DE OTRO, TODO ESTO HABRÍA QUE INFORMÁRSELO PARA QUE ESTÉ ENTERADO DE DÓNDE FALLÓ SI FALLÓ.
			    	*/
					if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}


	var verificarBloqueDeTraspasos = function(traspasosAceptados)
	{
		$( document ).ready(function() 
		{
			var cadenasIds = [];
			for(traspaso of traspasosAceptados)
			{
				if(!cadenasIds.includes(parseInt(traspaso.engaged_to)))
					cadenasIds.push(parseInt(traspaso.engaged_to));
			}
			console.log($scope.usuarioLogueado.equipo.id);
			var checkBloqueInfo =
				{
				    "team_id": $scope.usuarioLogueado.equipo.id,
				    "traspasos": cadenasIds
				};
			console.log("verificarBloqueDeTraspasos: ", checkBloqueInfo);
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/check_valid_block.php",
		        data: JSON.stringify(checkBloqueInfo)
		    }).done(
		    	function( data)
			    {
			    	console.log("Resultado check_valid_block: ", data);
			    	$scope.infoTraspasosUsuarioLogueado.resultadoSimulacion = data;
					if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}

	/**
	* Me registro en la variable del scope e userZone para que me llame cuando los datos que voy a usar estén cargados.
	*/
	$scope.addUserLoadedCallback(
		function()
		{
			console.log("Registrada por controller");
			cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
		}
	);


	console.log("TraspasosCtrl :: Cargada");
/*
	$transitions.onStart({}, function(trans) {
	 //console.log("statechange start");
	});

	$transitions.onSuccess({}, function() {
	  	console.log("statechange success", $state.current.name);
	  	if($state.current.name == "traspasos")
	  	{
	  		$scope.addUserLoadedCallback(
				function()
				{
					cargarTraspasosDeJugador($scope.usuarioLogueado.equipo.id);
				}
			);
	  	}
	});*/
}]);