app.controller('ToornamentCtrl', ['$scope','$state', '$stateParams','$timeout', '$transitions', function($scope,$state,$stateParams,$timeout,$transitions)
{
	$scope.infoToornament = {};
	$scope.infoToornament.proximosPartidos = [];

	var idEquipoAMostrar = $stateParams.id;
	
	var cargarInfoPartidos = function()
	{
		console.log("cargarInfoPartidos");

		$( document ).ready(function()
		{
			swal({
				title: "Cargando info de los partidos pendientes, espere por favor...",
				closeOnClickOutside: false,
				icon: "info",
  				buttons: false
			});

			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
			var yyyy = today.getFullYear();

			today = yyyy + '-' + mm + '-' + dd;
			

			// console.log("Cargando información de próximos partidos", idEquipoAMostrar);
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/partidos/read_matches_for_team_at_date.php?team_id=" + idEquipoAMostrar + "&date=" + today
		    }).done(
		    	function( partidos )
			    {
			    	if(partidos.length == 0)
			    	{
			    		swal({
							title: "No se encontraron partidos pendientes y habilitados."
						});
			    	}
			    	else
			    	{
			    		swal.close();
				    	console.log("Información de los próximos partidos cargada:", partidos);

				    	for (var i = 0; i < partidos.length; i++) 
				    	{
					    	var partido = partidos[i];
					    	$scope.infoToornament.proximosPartidos.push(
				    		{
				    			id: partido.id,
				    			fecha: partido.fecha,
				    			nombreLocal: partido.nombre_local,
				    			nombreVisitante: partido.nombre_visitante,
				    			resLocal: partido.res_local,
				    			resVisitante: partido.res_visitante,
				    			linkPartido: partido.link_partido
				    		});
				    	}
				    	if(!$scope.$$phase)$scope.$apply();
			    	}
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
			    	swal({
						title: "No se pudieron cargar los partidos",
						icon: "error"
					});
		      		console.log( "Carga de partidos falló: " + textStatus );
		    	}
		    );
		});
	};

	$scope.ingresarResultado = function(partido)
	{
		var inputResLocal = document.createElement("input");
		inputResLocal.id = "inputResLocal";
		inputResLocal.type = "text";
		inputResLocal.style = "width:30px; margin-left:10px; margin-right:10px; text-align: center;";

		var inputResVisitante = document.createElement("input");
		inputResVisitante.id = "inputResVisitante";
		inputResVisitante.type = "text";
		inputResVisitante.style = "width:30px; margin-left:10px; margin-right:10px; text-align: center;";

		var inputs = document.createElement("h5");
		inputs.append(partido.nombreLocal);
		inputs.append(inputResLocal);
		inputs.append(inputResVisitante);
		inputs.append(partido.nombreVisitante);

		swal({
			title:"Ingresar resultado de " + partido.fecha,
			content: inputs
		}).then((data) => {
			if(data)
			{
				var resLocal = $("#inputResLocal")[0].value;
				var resVisitante = $("#inputResVisitante")[0].value;
				$(document).ready(function()
				{
					swal({
						title: "Ingresando resultado de partido, espere por favor...",
						closeOnClickOutside: false,
						icon: "info",
		  				buttons: false
					});


					console.log("Asignando resultado de partido" , "idPartido",partido.id,
				        	"idEquipoIngresandoResultado",$scope.usuarioLogueado.equipo.id,
				        	"resLocal",resLocal,
				        	"resVisitante",resVisitante);
					$.ajax(
				    {
				        method: "POST",
				        url: SERVER_URL + SERVER_PORT+"/api/partidos/set_match_result.php",
				        data: JSON.stringify
				        ({
				        	"idEquipoIngresandoResultado":$scope.usuarioLogueado.equipo.id,
				        	"idPartido":"" + partido.id + "",
				        	"resLocal":resLocal,
				        	"resVisitante":resVisitante
				        })
				    }).done(
				    	function(data)
					    {
					    	console.log(data);
					    	swal({
								title: "Resultado ingresado",
								icon: "success",
				  				buttons: {confirm:"Vapa-i"},
				  				timer:3000
							});
					    }
					).fail(
				    	function(jqXHR, textStatus) 
				    	{
					    	swal({
								title: "No se pudo ingresar el resultado",
								icon: "error",
				  				buttons: {confirm:"Vapa-i"},
				  				timer:3000
							});

				      		console.log( "Ingreso de resultados falló: " + textStatus );
				    	}
				    );
				});
			}
		});
	};

	cargarInfoPartidos();

	console.log("Toornament controller cargada", idEquipoAMostrar);
}]);