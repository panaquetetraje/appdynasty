app.controller('VerJugadorCtrl', ['$scope','$state', '$stateParams','$timeout', function($scope,$state,$stateParams,$timeout)
{
	/**
	* Declaración e inicialización de variables del scope.
	*/
	$scope.jugadorMostrado = {};
	$scope.jugadorMostrado.id = $stateParams.id;
	$scope.jugadorMostrado.name = "";
	$scope.jugadorMostrado.overall_rating = 0;
	$scope.jugadorMostrado.team_id = 0;
	$scope.jugadorMostrado.team_name = "";
	$scope.jugadorMostrado.price_paid = 0;
	
	$scope.infoTraspaso = {};
	$scope.infoTraspaso.jugadorMostrado = {};
	$scope.infoTraspaso.jugadorMostrado.isTop = false;
	$scope.infoTraspaso.jugadorMostrado.overall_rating = 0;
	$scope.infoTraspaso.jugadorMostrado.team_id = 0;
	$scope.infoTraspaso.diferenciaEnPresupuesto = 0;
	$scope.infoTraspaso.puntosTopArribaDeCompra = "";
	$scope.infoTraspaso.puntosTopArribaDeVenta = "";
	$scope.infoTraspaso.puntosPerrunflasArribaDeCompra = "";
	$scope.infoTraspaso.puntosPerrunflasArribaDeVenta = "";
	$scope.infoTraspaso.equipoAVenderle = {id:"1"};

	/**
	* Definición de variables locales
	*/
	


	/**
	* Definición de operaciones del scope.
	*/
	/*$scope.actualizarDiferenciaDePresupuestoTrasTraspaso = function()
	{
		var presupuestoAUsar = ($scope.infoTraspaso.jugadorPropio.is_top != 0)?$scope.usuarioLogueado.equipo.top_budget:$scope.usuarioLogueado.equipo.perrunflas_budget;
		$scope.infoTraspaso.diferenciaEnPresupuesto = parseFloat(presupuestoAUsar) + parseFloat($scope.infoTraspaso.jugadorPropio.overall_rating) - parseFloat($scope.jugadorMostrado.overall_rating);
	}
	*/


	$scope.mostrarModalComprarJugadorDeOtroDynastyer = function()
	{
		var modalData = 
		{
			title:"Para comprar a " + $scope.jugadorMostrado.name + " tenés que vender a un jugador.",
			showBuySection:true,
			showSelectPlayerToBuy:false,
			showSellSection:true,
			showSelectPlayerToSell:true,
			jugadorParaComprar: angular.copy($scope.jugadorMostrado),
			jugadorParaVender: null,
			esCompraSuperPerrunfla: false,
			esVentaSuperPerrunfla: false
		};

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			onConfirmModalIntercambio
			, modalData
		);
	}

	$scope.mostrarModalComprarSuperPerrunfla = function()
	{
		var modalData = 
		{
			title:"Para comprar al súper perrunfla " + $scope.jugadorMostrado.name + " tenés que vender a un jugador.",
			showBuySection:true,
			showSelectPlayerToBuy:false,
			showSellSection:true,
			showSelectPlayerToSell:true,
			jugadorParaComprar: angular.copy($scope.jugadorMostrado),
			jugadorParaVender: null,
			esCompraSuperPerrunfla: true,
			esVentaSuperPerrunfla: true
		};

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			onConfirmModalIntercambio
			, modalData
		);
	}

	$scope.mostrarModalPeloPelo = function()
	{
		var modalData = 
		{
			title:"Para comprar pelo a pelo a " + $scope.jugadorMostrado.name + " tenés que seleccionar un jugador.",
			showBuySection:false,
			showSelectPlayerToBuy:false,
			showSellSection:true,
			showSelectPlayerToSell:false,
			showPeloPeloPlayer:true,
			showOfrecerJugador:false,
			jugadorParaComprar: angular.copy($scope.jugadorMostrado),
			jugadorParaVender: null,
			esCompraSuperPerrunfla: false,
			esVentaSuperPerrunfla: false,
			esVentaPeloPelo: true
		};

		console.log($scope.jugadorMostrado);
		console.log($scope.jugadoresDelUsuario);

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			
			onConfirmModalIntercambio
			, modalData
		);
	}

	$scope.mostrarModalComprarJugadorLibre = function()
	{
		console.log("mostrarModalComprarJugadorLibre");
		var modalData = 
		{
			title:"Para comprar a " + $scope.jugadorMostrado.name + " tenés que vender a un jugador.",
			showBuySection:false,
			showSelectPlayerToBuy:false,
			showSellSection:true,
			showSelectPlayerToSell:true,
			jugadorParaComprar: angular.copy($scope.jugadorMostrado),
			jugadorParaVender: null
		};

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			onConfirmModalIntercambio
			, modalData
		);
	}

	$scope.mostrarModalVenderJugador = function()
	{
		console.log("mostrarModalVenderJugador");
		var modalData = 
		{
			title:"Para vender a " + $scope.jugadorMostrado.name + " tenés que comprar a otro jugador.",
			showBuySection:true,
			showSelectPlayerToBuy:true,
			showSellSection:true,
			showSelectPlayerToSell:false,
			jugadorParaVender: angular.copy($scope.jugadorMostrado)
		};

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},
			onConfirmModalIntercambio
			, modalData
		);
	}

	$scope.mostrarModalActualizarJugador = function()
	{
		console.log("mostrarModalActualizarJugador", $scope.jugadorMostrado);
		var modalData = 
		{
			title:"¿Seguro de actualizar a " + $scope.jugadorMostrado.name + "? Lo compraste a " + $scope.jugadorMostrado.price_paid + " y ahora sale " + $scope.jugadorMostrado.overall_rating,
			showBuySection:false,
			showSelectPlayerToBuy:false,
			showSellSection:false,
			showSelectPlayerToSell:false,
			jugadorParaVender: angular.copy($scope.jugadorMostrado),
			jugadorParaComprar: angular.copy($scope.jugadorMostrado)
		};

		$scope.addModalIntercambioCallback(
			function(result)
			{
				console.log("Cancelado");
			},

			onConfirmActualizarJugador
			, modalData
		);
	}
	


	/**
	* Definición de operaciones locales.
	*/
	var onConfirmModalIntercambio = function(result)
	{
		console.log("onConfirmModalIntercambio verJugador", result);
		var date = new Date();

		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate() + " " + 
			date.getHours() + ":" + 
			date.getMinutes() + ":" + 
			date.getSeconds()
			;
		console.log("date", date);
		console.log("fecha", fecha);

		var added_top_points_venta = parseFloat(result.puntosTopArribaDeVenta);
		if(isNaN(added_top_points_venta))added_top_points_venta = 0;

		var added_perrunflas_points_venta = parseFloat(result.puntosPerrunflasArribaDeVenta);
		if(isNaN(added_perrunflas_points_venta))added_perrunflas_points_venta = 0;
		
		var added_top_points_compra = parseFloat(result.puntosTopArribaDeCompra);
		if(isNaN(added_top_points_compra))added_top_points_compra = 0;

		var added_perrunflas_points_compra = parseFloat(result.puntosPerrunflasArribaDeCompra);
		if(isNaN(added_perrunflas_points_compra))added_perrunflas_points_compra = 0;

		/*
		console.log("added_top_points_venta", added_top_points_venta);
		console.log("added_perrunflas_points_venta", added_perrunflas_points_venta);
		console.log("added_top_points_compra", added_top_points_compra);
		console.log("added_perrunflas_points_compra", added_perrunflas_points_compra);
		*/
		console.log("result.equipoAVenderle.id", result.equipoAVenderle.id);
		console.log("result.jugadorParaVender.team_id", result.jugadorParaVender.team_id);
		
		console.log("result.jugadorParaVender.team_id", result.jugadorParaVender.team_id);
		console.log("result.jugadorParaComprar.team_id", result.jugadorParaComprar.team_id);
		if(result.esVentaPeloPelo==1){
			result.equipoAVenderle.id=result.jugadorParaComprar.team_id;
			}
		var ventaACrear = 
			{
			    "buyer_id": result.equipoAVenderle.id,
			    "player_id": result.jugadorParaVender.id,
			    "seller_id": result.jugadorParaVender.team_id,
			    "sell_date": fecha,
			    "sell_price": result.jugadorParaVender.overall_rating,
			    "is_pending": 1,
			    "added_top_points": added_top_points_venta,
			    "added_perrunflas_points": added_perrunflas_points_venta,
			    "is_super_perrun": result.esVentaSuperPerrunfla?1:0,
			    "is_pelo_a_pelo": result.esVentaPeloPelo?1:0

			}
		
		var compraACrear = 
			{
			    "buyer_id": result.jugadorParaVender.team_id,
			    "player_id": result.jugadorParaComprar.id,
			    "seller_id": result.jugadorParaComprar.team_id,
			    "sell_date": fecha,
			    "sell_price": result.jugadorParaComprar.overall_rating,
			    "is_pending": 1,
			    "added_top_points": added_top_points_compra,
			    "added_perrunflas_points": added_perrunflas_points_compra,
			    "is_super_perrun": result.esCompraSuperPerrunfla?1:0,
			    "is_pelo_a_pelo": result.esVentaPeloPelo?1:0
			    
			};

		var traspasos = [ventaACrear, compraACrear];
		
		console.log("onConfirmModalIntercambio traspasos", traspasos);

		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Propuesta realizada en la BD", resultado);
				$state.transitionTo('traspasos');
			}
		);
	}

	/**
	* Definición de operaciones locales.
	*/
	var onConfirmActualizarJugador = function(result)
	{
		console.log("onConfirmActualizarJugador", result);
		var date = new Date();

		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate() + " " + 
			date.getHours() + ":" + 
			date.getMinutes() + ":" + 
			date.getSeconds()
			;
		console.log("date", date);
		console.log("fecha2", fecha);

		/*
		console.log("added_top_points_venta", added_top_points_venta);
		console.log("added_perrunflas_points_venta", added_perrunflas_points_venta);
		console.log("added_top_points_compra", added_top_points_compra);
		console.log("added_perrunflas_points_compra", added_perrunflas_points_compra);
		*/

		var compraACrear = 
			{
			    "buyer_id": result.jugadorParaVender.team_id,
			    "player_id": result.jugadorParaComprar.id,
			    "seller_id": result.jugadorParaComprar.team_id,
			    "sell_date": fecha,
			    "sell_price": result.jugadorParaComprar.overall_rating,
			    "is_pending": 1,
			    "added_top_points": 0,
			    "added_perrunflas_points": 0,
			    "is_super_perrun": 0,
			    "is_pelo_a_pelo": 0
			};

		var traspasos = [compraACrear];
		
		console.log("traspasos", traspasos);

		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Propuesta de actualización en la BD", resultado);
				$state.transitionTo('traspasos');
			}
		);
	}
	

	var cargarInfoJugador = function(id)
	{
		$( document ).ready(function()
		{
			console.log("Cargando información de jugador", id);
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/jugador/read_one.php?id="+id
		    }).done(
		    	function( data )
			    {
			    	console.log("Jugador cargado2:", data);
			    	$scope.jugadorMostrado.name = data.name;
			    	$scope.jugadorMostrado.overall_rating = data.overall_rating;
			    	$scope.jugadorMostrado.team_id = data.team_id;
			    	$scope.jugadorMostrado.team_name = data.team_name;
			    	$scope.jugadorMostrado.price_paid = data.price_paid;
			    	$scope.jugadorMostrado.is_top = data.is_top;
			    	$scope.jugadorMostrado.esSuperPerrunfla = data.esSuperPerrunfla;
			    	$scope.jugadorMostrado.real_sell_price = data.real_sell_price;
			    	$scope.jugadorMostrado.idMitad1 = id.substring(0,3);
			    	$scope.jugadorMostrado.idMitad2 = id.substring(3,6);
			    	console.log($scope.jugadorMostrado);
			    	if(!$scope.$$phase)$scope.$apply();
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}

	var crearTraspasosEnBD = function(traspasos, callback/*(resultado)*/)
	{
		$( document ).ready(function()
		{
			console.log("Creando traspasos: ", traspasos);
			$.ajax(
		    {
		        method: "POST",
		        url: SERVER_URL + SERVER_PORT+"/api/traspaso/create.php",
		        data: JSON.stringify(traspasos)
		    }).done(
		    	function( data )
			    {
			    	console.log("Traspasos creado");
			    	callback(data);
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus, jqXHR );
		    	}
		    );
		});//*/
	}

	cargarInfoJugador($scope.jugadorMostrado.id);
	console.log("VerJugadorCtrl :: Cargada", $scope.jugadorMostrado.id);
}]);



/*	$scope.hacerUnaPropuesta = function()
	{
		console.log("hacerUnaPropuesta");
		var date = new Date();
		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate();
		

		var added_top_points_venta = parseFloat($scope.infoTraspaso.puntosTopArribaDeVenta);
		if(isNaN(added_top_points_venta))added_top_points_venta = 0;

		var added_perrunflas_points_venta = parseFloat($scope.infoTraspaso.puntosPerrunflasArribaDeVenta);
		if(isNaN(added_perrunflas_points_venta))added_perrunflas_points_venta = 0;
		
		var added_top_points_compra = parseFloat($scope.infoTraspaso.puntosTopArribaDeCompra);
		if(isNaN(added_top_points_compra))added_top_points_compra = 0;

		var added_perrunflas_points_compra = parseFloat($scope.infoTraspaso.puntosPerrunflasArribaDeCompra);
		if(isNaN(added_perrunflas_points_compra))added_perrunflas_points_compra = 0;


		console.log("added_top_points_venta", added_top_points_venta);
		console.log("added_perrunflas_points_venta", added_perrunflas_points_venta);
		console.log("added_top_points_compra", added_top_points_compra);
		console.log("added_perrunflas_points_compra", added_perrunflas_points_compra);


		var ventaACrear = 
			{
			    "buyer_id": $scope.infoTraspaso.equipoAVenderle.id,
			    "player_id": $scope.infoTraspaso.jugadorPropio.id,
			    "seller_id": $scope.infoTraspaso.jugadorPropio.team_id,
			    "sell_date": fecha,
			    "sell_price": $scope.infoTraspaso.jugadorPropio.overall_rating,
			    "is_pending": 1,
			    "added_top_points": added_top_points_venta,
			    "added_perrunflas_points": added_perrunflas_points_venta
			}
		
		var compraACrear = 
			{
			    "buyer_id": $scope.infoTraspaso.jugadorPropio.team_id,
			    "player_id": $scope.jugadorMostrado.id,
			    "seller_id": $scope.jugadorMostrado.team_id,
			    "sell_date": fecha,
			    "sell_price": $scope.jugadorMostrado.overall_rating,
			    "is_pending": 1,
			    "added_top_points": added_top_points_compra,
			    "added_perrunflas_points": added_perrunflas_points_compra
			};

		var traspasos = [ventaACrear, compraACrear];
		*/
		/*
		console.log("ventaACrear", ventaACrear);
		console.log("compraACrear", compraACrear);
		console.log("equipoAVenderle", $scope.infoTraspaso.equipoAVenderle);
		console.log("puntosTopArribaDeCompra", $scope.infoTraspaso.puntosTopArribaDeCompra);
		console.log("puntosPerrunflasArribaDeCompra", $scope.infoTraspaso.puntosPerrunflasArribaDeCompra);
		*/
/*
		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Propuesta realizada en la BD", resultado);
				modalHacerOferta.on('hidden.bs.modal', function () {
		   			$state.transitionTo('traspasos');
				});
				terminarTraspaso();
			}
		);
	}
*/
	/*
	var realizarIntercambio = function(jugadorAComprar, jugadorAVender)
	{
		console.log("realizarIntercambio");
		var date = new Date();
		var fecha = 
			date.getFullYear() + "-" + 
			(date.getMonth()+1) + "-" + // Ajustando rango de 0-11 a 1-12.
			date.getDate();

		var ventaACrear = 
			{
			    "buyer_id": ID_MERCADO_ASIATICO,
			    "player_id": $scope.infoTraspaso.jugadorPropio.id,
			    "seller_id": $scope.infoTraspaso.jugadorPropio.team_id,
			    "sell_date": fecha,
			    "sell_price": $scope.infoTraspaso.jugadorPropio.overall_rating,
			    "is_pending": 1,
			    "added_top_points": 0,
			    "added_perrunflas_points": 0
			};
		
		var compraACrear = 
			{
			    "buyer_id": $scope.infoTraspaso.jugadorPropio.team_id,
			    "player_id": $scope.jugadorMostrado.id,
			    "seller_id": ID_MERCADO_ASIATICO,
			    "sell_date": fecha,
			    "sell_price": $scope.jugadorMostrado.overall_rating,
			    "is_pending": 1,
			    "added_top_points": 0,
			    "added_perrunflas_points": 0
			};

		var traspasos = [ventaACrear, compraACrear];
		
		crearTraspasosEnBD(traspasos, 
			function(resultado)
			{
				console.log("Traspasos creados en la BD", resultado);
				modalComprarJugador.on('hidden.bs.modal', function () {
		   			$state.transitionTo('traspasos');
				});
				terminarTraspaso();
			}
		);
	}
	*/
