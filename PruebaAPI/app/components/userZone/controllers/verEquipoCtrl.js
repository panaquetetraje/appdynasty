app.controller('VerEquipoCtrl', ['$scope','$state', '$stateParams','$timeout', function($scope,$state,$stateParams,$timeout)
{
	/**
	* Declaración e inicialización de variables del scope.
	*/
	

	
	/**
	* Definición de variables locales
	*/
	var idEquipoAMostrar = $stateParams.id;


	/**
	* Definición de operaciones del scope.
	*/

	/**
	* Definición de operaciones locales.
	*/
	var cargarInfoEquipo = function(id)
	{
		$( document ).ready(function()
		{
			console.log("Cargando información de equipo", id);
			$.ajax(
		    {
		        method: "GET",
		        url: SERVER_URL + SERVER_PORT+"/api/equipo/read_one.php?id="+id
		    }).done(
		    	function( data )
			    {
			    	console.log("Información del equipo cargada:", data);
			    	$scope.$parent.equipoMostrado = angular.copy(data.team);
			    	$scope.$parent.equipoMostrado.valor_equipo_tops =  (parseFloat(data.team.suma_real_tops) + parseFloat(data.team.resto_tops));
			    	$scope.$parent.equipoMostrado.valor_equipo_perrunflas = (parseFloat(data.team.suma_real_perrunflas) + parseFloat(data.team.resto_perrunflas));
			    	$scope.$parent.equipoMostrado.logo = data.team.logo;
			    	$scope.$parent.equipoMostrado.sofifa_team_uri = data.team.sofifa_team_uri;
			    	$scope.$parent.equipoMostrado.fylyal_name = data.team.fylyal_name;
			    	$scope.$parent.equipoMostrado.fylyal_id = data.team.fylyal_id;
			    	console.log("Equipo mostrado: ", $scope.$parent.equipoMostrado);
			    	if(!$scope.$$phase)$scope.$apply();
			    	$scope.cargarJugadoresDelEquipo(id, $scope.onJugadoresCargados);
			    	
			    }
			).fail(
		    	function( jqXHR, textStatus ) 
		    	{
		      		console.log( "Request failed: " + textStatus );
		    	}
		    );
		});
	}

	if(idEquipoAMostrar != 0)
		cargarInfoEquipo(idEquipoAMostrar);
	console.log("VerEquioiCtrl :: Cargada", idEquipoAMostrar);
}]);