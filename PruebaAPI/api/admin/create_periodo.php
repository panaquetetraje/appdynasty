<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
$database = new Database();
$db = $database->getConnection();
 
// get posted data
/**
* ESPERO:
* {
*     "periodo": 
*     {
*       diaInicio: $scope.periodoDeTraspasos.diaInicio,
*       mesInicio: $scope.periodoDeTraspasos.mesInicio,
*       anioInicio: $scope.periodoDeTraspasos.anioInicio,
*       diaFin: $scope.periodoDeTraspasos.diaFin,
*       mesFin: $scope.periodoDeTraspasos.mesFin,
*       anioFin: $scope.periodoDeTraspasos.anioFin
*     } 
*     "userdata": 
*     {           
*        "user": $scope.infoAdmin.usuario, 
*        "pass": $scope.infoAdmin.clave
*     }
* }
*
*
*/
$data = json_decode(file_get_contents("php://input"));

$query = "Select * from usuarios where name=:user and pass=:pass;";
$stmt = $db->prepare($query);

$data->userData->user=htmlspecialchars(strip_tags($data->userData->user));
$data->userData->pass=htmlspecialchars(strip_tags($data->userData->pass));


$stmt->bindParam(":user", $data->userData->user);
$stmt->bindParam(":pass", $data->userData->pass);
/*
error_log("Antes");
error_log("stmt->rowCount()");
error_log($stmt->rowCount());
error_log("data->periodo");
error_log($data->periodo->anioInicio);
error_log($data->periodo->mesInicio);
error_log($data->periodo->diaInicio);
error_log($data->periodo->anioFin);
error_log($data->periodo->mesFin);
error_log($data->periodo->diaFin);
*/
$anduvo = $stmt->execute();
if($anduvo && $stmt->rowCount() > 0){
    $query = "INSERT INTO periodos_traspasos (fecha_inicio, fecha_fin)
        VALUES (:fechaInicio, :fechaFin);";
    /*$query = "INSERT INTO periodos_traspasos (fechaInicio, fechaFin)
        VALUES ('2019-09-22', '2019-09-26');";
*/
    $stmt = $db->prepare($query);

    
    $fechaInicio = $data->periodo->anioInicio . "-" . $data->periodo->mesInicio . "-" . $data->periodo->diaInicio;
    $fechaFin = $data->periodo->anioFin . "-" . $data->periodo->mesFin . "-" . $data->periodo->diaFin;
    
    $stmt->bindParam(":fechaInicio", $fechaInicio);
    $stmt->bindParam(":fechaFin", $fechaFin);
    
    /*
    error_log($query);
    error_log($fechaInicio);
    error_log($fechaFin);
    */
    if($stmt->execute())
    {
        // set response code - 201 created
        http_response_code(201);

        // tell the user
        echo json_encode(array("message" => "Periodo insertado."));
    }
    else
    {

        // set response code - 400 bad request
        http_response_code(400);
     
        // tell the user
        echo json_encode(array("message" => "Error al insertar período."));
    }
}
else
{ 

    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Permiso denegado"));
}
?>