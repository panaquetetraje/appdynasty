<?php
	class DtSeccion
	{
		private $indice /* string */;
		private $titulo /* string */;
		private $contenido /* string */;

		public function __construct($indice, $titulo, $contenido)
		{
			$this->indice = $indice; 
			$this->titulo = $titulo;
			$this->contenido = $contenido;
		}

		public function getIndice() {return $this->indice;}
		public function getTitulo() {return $this->titulo;}
		public function getContenido() {return $this->contenido;}
	}
?>