<?php
class DtPartido
{
	private $id /* string */;
	private $fecha /* string */; // No es de tipo fecha sino la fecha que representa el partido como instancia del campeonato, ejemplo "1" sería la primera fecha del campeonato.
	private $nombreLocal /* string */;
	private $nombreVisitante /* string */;
	private $resLocal /* int */;
	private $resVisitante /* int */;
	private $idCampeonato /* int */;


	public function __construct($id, $fecha, $nombreLocal, $nombreVisitante, $resLocal, $resVisitante, $idCampeonato)
	{
		$this->id = $id; 
		$this->fecha = $fecha; 
		$this->nombreLocal = $nombreLocal;
		$this->nombreVisitante = $nombreVisitante;
		$this->resLocal = $resLocal;
		$this->resVisitante = $resVisitante;
		$this->idCampeonato = $idCampeonato;
	}

	public function getId() {return $this->id;}
	public function getFecha() {return $this->fecha;}
	public function getNombreLocal() {return $this->nombreLocal;}
	public function getNombreVisitante() {return $this->nombreVisitante;}
	public function getResLocal() {return $this->resLocal;}
	public function getResVisitante() {return $this->resVisitante;}
	public function getIdCampeonato() {return $this->idCampeonato;}

}
?>