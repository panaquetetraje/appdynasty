<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../objects/PartidosController.php';


$data = json_decode(file_get_contents("php://input"));
if(!(isset($data->id_etapa) && isset($data->num_fecha_a_habilitar) && isset($data->id_campeonato) && isset($data->dia_habilitacion)))
{
    http_response_code(412);
    echo json_encode(array("error" => "Falta información."));
    die();
}

$idCampeonato = $data->id_campeonato;
$idEtapa = $data->id_etapa;
$numFechaAHabilitar = $data->num_fecha_a_habilitar;
$diaHabilitacion = $data->dia_habilitacion;


// echo "data->idEquipo: {$data->idEquipo}";
$resultadoIngreso = PartidosController::getInstance()->ingresarNuevaHabilitacion($idCampeonato, $idEtapa, $numFechaAHabilitar, $diaHabilitacion);

 
// check if more than 0 record found
if($resultadoIngreso)
{
    // set response code - 200 OK
    http_response_code(200);
    
    // show equipos data in json format
    echo json_encode(
        array("message" => "Fecha habilitada.")
    );
}
else
{
 
    http_response_code(202);
 
    echo json_encode(
        array("error" => "No se pudo habilitar la fecha.")
    );
}
?>