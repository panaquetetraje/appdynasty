<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/PartidosController.php';
 
// instantiate database and jugador object
$database = new Database();
$db = $database->getConnection();
 
$idCampeonato = isset($_GET['championship_id']) ? $_GET['championship_id'] : die("Error de entrada");

$etapas = PartidosController::getInstance()->listarEtapasDeCampeonato($idCampeonato);
 
if(isset($etapas) && count($etapas) > 0){
    $etapas_respuesta = array();
    for ($i=0; $i < count($etapas); $i++) {
        array_push($etapas_respuesta, [
            "id"        => $etapas[$i]->getId(),
            "nombre"    => $etapas[$i]->getNombre()
        ]);
    }

    // set response code - 200 OK
    http_response_code(200);
    // show jugadores data in json format
    echo json_encode($etapas_respuesta);
}
else{
 
    // set response code - 404 Not found
    http_response_code(200);
 
    // tell the user no jugadores found
    echo json_encode(
        array("message" => "No se encontraron campeonatos.")
    );
}
?>