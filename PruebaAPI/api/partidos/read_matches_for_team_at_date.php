<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../objects/PartidosController.php';

 
if(!(isset($_GET['team_id']) && isset($_GET['date'])))
{
    http_response_code(412);
    echo json_encode(array("error" => "Falta información."));
    die();
}

$idEquipo   = $_GET['team_id'];
$fecha      = $_GET['date'];

$partidosInfo = PartidosController::getInstance()->readMatchesForTeamAtDate($idEquipo, $fecha);
 
if(isset($partidosInfo) && count($partidosInfo) > 0){
    $partidos_respuesta = array();
    for ($i=0; $i < count($partidosInfo); $i++) {
        array_push($partidos_respuesta, [
            "id"                => $partidosInfo[$i]->getId(),
            "fecha"             => $partidosInfo[$i]->getFecha(),
            "nombre_local"      => $partidosInfo[$i]->getNombreLocal(),
            "nombre_visitante"  => $partidosInfo[$i]->getNombreVisitante(),
            "res_local"         => $partidosInfo[$i]->getResLocal(),
            "res_visitante"     => $partidosInfo[$i]->getResVisitante(),
            "id_campeonato"     => $partidosInfo[$i]->getIdCampeonato(),
            "link_partido"       => PartidosController::getInstance()->getLinkDePartido($partidosInfo[$i])            
        ]);
    }

    // set response code - 200 OK
    http_response_code(200);
    // show jugadores data in json format
    echo json_encode($partidos_respuesta);
}
else{
 
    // set response code - 404 Not found
    http_response_code(200);
 
    // tell the user no jugadores found
    echo json_encode(
        array()
    );
}
?>