<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/PartidosController.php';
 
// instantiate database and jugador object
$database = new Database();
$db = $database->getConnection();
 
$campeonatos = PartidosController::getInstance()->listarCampeonatosVigentes();
 
// check if more than 0 record found
if(isset($campeonatos) && count($campeonatos) > 0){
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show jugadores data in json format
    echo json_encode($campeonatos);
}
else{
 
    // set response code - 404 Not found
    http_response_code(200);
 
    // tell the user no jugadores found
    echo json_encode(
        array("message" => "No se encontraron campeonatos.")
    );
}
?>