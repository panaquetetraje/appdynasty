<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../objects/PartidosController.php';


$data = json_decode(file_get_contents("php://input"));
$idEquipoIngresandoResultado = $data->idEquipoIngresandoResultado;
$idPartido = $data->idPartido;
$resLocal = $data->resLocal;
$resVisitante = $data->resVisitante;

// echo "data->idEquipo: {$data->idEquipo}";
$resultadoAsignacion = PartidosController::getInstance()->setMatchResult($idEquipoIngresandoResultado, $idPartido, $resLocal, $resVisitante);

 
// check if more than 0 record found
if($resultadoAsignacion)
{
    // set response code - 200 OK
    http_response_code(200);
    
    // show equipos data in json format
    echo json_encode(
        array("message" => "Resultado asignado.", "result" => "ok")
    );
}
else
{
    http_response_code(200);
 
    echo json_encode(
        array("message" => "No se pudo asignar el resultado.", "result" => "error")
    );
}
?>