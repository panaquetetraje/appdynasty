<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../objects/PartidosController.php';


if(!(isset($_GET['id_equipo']) && isset($_GET['id_equipo'])))
{
    http_response_code(412);
    echo json_encode(array("error" => "Falta información."));
    die();
}

$idEquipo = $_GET['id_equipo'];

$partidosInfo = PartidosController::getInstance()->readNextMatchesForTeam($idEquipo);

 
// check if more than 0 record found
if(count($partidosInfo) > 0)
{
    // set response code - 200 OK
    http_response_code(200);
    $partidosArray = array();
    
    for ($i=0; $i < count($partidosInfo); $i++) 
    { 
        array_push($partidosArray, [
            "id"                => $partidosInfo[$i]->getId(),
            "fecha"             => $partidosInfo[$i]->getFecha(),
            "nombre_local"      => $partidosInfo[$i]->getNombreLocal(),
            "nombre_visitante"  => $partidosInfo[$i]->getNombreVisitante(),
            "res_local"         => $partidosInfo[$i]->getResLocal(),
            "res_visitante"     => $partidosInfo[$i]->getResVisitante(),
            "id_campeonato"     => $partidosInfo[$i]->getIdCampeonato(),
            "link_partido"       => PartidosController::getInstance()->getLinkDePartido($partidosInfo[$i])            
        ]);
    }
    // show equipos data in json format
    echo json_encode($partidosArray);
}
else
{
 
    http_response_code(200);
 
    echo json_encode(
        array("message" => "No equipos found.", "partidos" => "")
    );
}
?>