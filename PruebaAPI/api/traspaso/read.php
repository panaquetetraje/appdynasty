<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/traspaso.php';
 
// instantiate database and traspaso object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$traspaso = new Traspaso($db);
 
// query traspasos
$stmt = $traspaso->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // traspasos array
    $traspasos_arr=array();
    $traspasos_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        


        $traspaso_item=array(
            "buyer_id" => $buyer_id,
            "player_id" => $player_id,
            "seller_id" => $seller_id,
            "sell_date" => $sell_date,
            "sell_price" => $sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "engaged_to" => $engaged_to,
            "added_top_points" => $added_top_points,
            "added_perrunflas_points" => $added_perrunflas_points
        );
 
        array_push($traspasos_arr["records"], $traspaso_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show traspasos data in json format
    echo json_encode($traspasos_arr);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no traspasos found
    echo json_encode(
        array("message" => "No traspasos found.")
    );
}
?>