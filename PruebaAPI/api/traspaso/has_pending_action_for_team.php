<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
 
$database = new Database();
$db = $database->getConnection();
 
$traspaso = new Traspaso($db);

 

$data = json_decode(file_get_contents("php://input"));
/**
* ESPERO:
*    { 
        "cadena_id": 4,
        "team_id": 28
*    }
* DEVUELVO:
* array(
        "hasPendingAction" => 1 // si tiene acción pendiente o 0 si no la tiene.
        )
*/

if(isset($data->cadena_id)
 && isset($data->team_id))
{
    $resultado = $traspaso->has_pending_action_for_team($data->cadena_id, $data->team_id);
    // set response code - 201 created
    http_response_code(201);

    // tell the user
    echo json_encode(array("hasPendingAction" => ($resultado)?1:0));
}
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to check if pending action. Data is incomplete."));
}
?>