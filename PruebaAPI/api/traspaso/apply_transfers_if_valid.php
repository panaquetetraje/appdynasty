<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
include_once '../objects/usuario.php';
include_once '../objects/equipo.php';
include_once '../objects/jugador.php';
include_once '../objects/periodo.php';
 
$database = new Database();
$db = $database->getConnection();
 
$traspaso = new Traspaso($db);

 

$data = json_decode(file_get_contents("php://input"));
/**
* ESPERO:
*    { team_id:[[id_equipo]],
*    traspasos:    
*       [
*          [[id_cadena_1]], 
*          ...
*          [[id_cadena_n]]   
*       ]
*    }
*/

$resultadoBloque = $traspaso->get_is_valid_block($data);

if($resultadoBloque != null)
{
    if($resultadoBloque['bloqueValido'] && $traspaso->apply_block_transfers($resultadoBloque))
    {
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($resultadoBloque);
    }
    else
    {
        // set response code - 503 service unavailable
        http_response_code(400);
 
        // tell the user
        echo json_encode($resultadoBloque);
    }

}
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to aplly transfers. Data is incomplete."));
}
?>