<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/traspaso.php';
include_once '../objects/jugador.php';
 
// instantiate database and traspaso object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$traspaso = new Traspaso($db);
$traspasos_arr=array();

$team_id = isset($_GET["team_id"]) ? $_GET["team_id"] : die();

// query traspasos
$stmt = $traspaso->read_no_chains_from_team($team_id);
$numNoChains = $stmt->rowCount();

// check if more than 0 record found

// error_log("numNoChains de " . $team_id . ": " . $numNoChains);
if($numNoChains>0)
{
 
    $traspasos_arr["no_chains"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        


        $traspaso_item=array(
            "id" => $id,
            "seller_accepted" => $seller_accepted,
            "buyer_accepted" => $buyer_accepted,
            "buyer_id" => $buyer_id,
            "seller_id" => $seller_id,
            "player_id" => $player_id,
            "sell_date" => $sell_date,
            "sell_price" => $sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "engaged_to" => $engaged_to,
            "player_name" => $player_name,
            "is_top" => $is_top,
            "buyer_name" => $buyer_name,
            "seller_name" => $seller_name,
            "added_top_points" => $added_top_points,
            "added_perrunflas_points" => $added_perrunflas_points
        );
 
        array_push($traspasos_arr["no_chains"], $traspaso_item);
    }
}


$stmt = $traspaso->read_chains_from_team($team_id);
$numChains = $stmt->rowCount();
 
// error_log("numChains de " . $team_id . ": " . $numChains);
if($numChains>0){
 
    // traspasos array
    $traspasos_arr["chains"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        


        $traspaso_item=array(
            "id" => $id,
            "seller_accepted" => $seller_accepted,
            "buyer_accepted" => $buyer_accepted,
            "buyer_id" => $buyer_id,
            "seller_id" => $seller_id,
            "player_id" => $player_id,
            "sell_date" => $sell_date,
            "sell_price" => $sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "engaged_to" => $engaged_to,
            "player_name" => $player_name,
            "is_top" => $is_top,
            "buyer_name" => $buyer_name,
            "seller_name" => $seller_name,
            "added_top_points" => $added_top_points,
            "added_perrunflas_points" => $added_perrunflas_points
        );
 
        array_push($traspasos_arr["chains"], $traspaso_item);
    }
}

$stmt = $traspaso->read_updates_from_team($team_id);
$numUpdates = $stmt->rowCount();
 
// error_log("numUpdates de " . $team_id . ": " . $numUpdates);
if($numUpdates>0){
 
    // traspasos array
    $traspasos_arr["updates"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        

        $jugador = new Jugador($db);
        $jugador->id = $player_id;
        $jugador->read_one();

        $traspaso_item=array(
            "id" => $id,
            "seller_accepted" => $seller_accepted,
            "buyer_accepted" => $buyer_accepted,
            "buyer_id" => $buyer_id,
            "seller_id" => $seller_id,
            "player_id" => $player_id,
            "sell_date" => $sell_date,
            "price_paid" => $jugador->price_paid,
            "player_overall" => $jugador->overall_rating,
            "sell_price" => $jugador->real_sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "engaged_to" => $engaged_to,
            "player_name" => $player_name,
            "is_top" => $is_top,
        );
 
        array_push($traspasos_arr["updates"], $traspaso_item);
    }
}

$stmt = $traspaso->read_super_perrun_from_team($team_id);
$numSuperPerrun = $stmt->rowCount();
 
// error_log("numSuperPerrun de " . $team_id . ": " . $numSuperPerrun);
if($numSuperPerrun>0)
{
 
    // traspasos array
    $traspasos_arr["superPerrun"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        

        $jugador = new Jugador($db);
        $jugador->id = $player_id;
        $jugador->read_one();

        $traspaso_item=array(
            "id" => $id,
            "seller_accepted" => $seller_accepted,
            "buyer_accepted" => $buyer_accepted,
            "buyer_id" => $buyer_id,
            "seller_id" => $seller_id,
            "player_id" => $player_id,
            "sell_date" => $sell_date,
            "price_paid" => $jugador->price_paid,
            "player_overall" => $jugador->overall_rating,
            "sell_price" => $jugador->real_sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "is_super_perrun" => $is_super_perrun,
            "engaged_to" => $engaged_to,
            "player_name" => $player_name,
            "is_top" => $is_top
        );
 
        array_push($traspasos_arr["superPerrun"], $traspaso_item);
    }
}

$stmt = $traspaso->read_pelo_a_pelo_from_team($team_id);
$numPeloPelo = $stmt->rowCount();
 
// error_log("numPeloPelo de " . $team_id . ": " . $numPeloPelo);
if($numPeloPelo>0)
{
 
    // traspasos array
    $traspasos_arr["PeloPelo"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        

        $jugador = new Jugador($db);
        $jugador->id = $player_id;
        $jugador->read_one();

        $traspaso_item=array(
            "id" => $id,
            "seller_accepted" => $seller_accepted,
            "buyer_accepted" => $buyer_accepted,
            "buyer_id" => $buyer_id,
            "seller_id" => $seller_id,
            "player_id" => $player_id,
            "sell_date" => $sell_date,
            "price_paid" => $jugador->price_paid,
            "player_overall" => $jugador->overall_rating,
            "sell_price" => $jugador->real_sell_price,
            "is_pending" => ($is_pending!=null)?1:0,
            "seller_name" => $seller_name,
            "buyer_name" => $buyer_name,
            "is_pelo_a_pelo" => $is_pelo_a_pelo,
            "engaged_to" => $engaged_to,
            "player_name" => $player_name,
            "is_top" => $is_top
        );
 
        array_push($traspasos_arr["PeloPelo"], $traspaso_item);
    }
}

if($numNoChains>0 || $numChains>0 || $numUpdates>0 || $numSuperPerrun>0 || $numPeloPelo>0)
{
    // set response code - 200 OK
    http_response_code(200);
 
    // show traspasos data in json format
    echo json_encode($traspasos_arr);
}
else{
 
    http_response_code(204);
 
    // tell the user no traspasos found
    echo json_encode(
        array("message" => "No traspasos found for requested team.")
    );
}
?>