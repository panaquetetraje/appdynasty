<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/traspaso.php';
 
// instantiate database and traspaso object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$traspaso = new Traspaso($db);

$team_id = isset($_GET["id"]) ? $_GET["id"] : die();

// query traspasos
$amount = $traspaso->get_amount($team_id);


// set response code - 200 OK
http_response_code(200);

// show traspasos data in json format
echo json_encode($amount);

?>