<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
 
$database = new Database();
$db = $database->getConnection();
 
$traspaso = new Traspaso($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

$i = 0;
$validData = isset($data[$i]);
while($validData && isset($data[$i]))
{
    $validData = !empty($data[$i]->buyer_id) &&
        !empty($data[$i]->seller_id) &&
        !empty($data[$i]->player_id) &&
        !empty($data[$i]->sell_date) &&
        !empty($data[$i]->sell_price) &&
        isset($data[$i]->is_super_perrun) &&
        isset($data[$i]->is_pelo_a_pelo);
    $i++;
}

if($validData)
{
    $db->beginTransaction();
    $successfullTransaction = true;
    $blockId = isset($data[0]->engaged_to)?$data[0]->engaged_to:0;
    /*
    error_log("blockId");
    error_log($blockId);
    */
    while ($successfullTransaction && ($i > 0))
    {
        $i--;
        $traspaso->buyer_id = $data[$i]->buyer_id;
        $seller_accepted = (isset($data[$i]->seller_accepted))?1:0;
        $buyer_accepted = (isset($data[$i]->buyer_accepted))?1:0;
        $traspaso->seller_accepted = ($data[$i]->seller_id == 1)?1:$seller_accepted;
        $traspaso->buyer_accepted = ($data[$i]->buyer_id == 1)?1:$buyer_accepted;
        $traspaso->seller_id = $data[$i]->seller_id;
        $traspaso->player_id = $data[$i]->player_id;
        $traspaso->sell_date = $data[$i]->sell_date;
        $traspaso->sell_price = $data[$i]->sell_price;
        $traspaso->is_pending = (isset($data[$i]->is_pending))?1:0;
        $traspaso->is_super_perrun = $data[$i]->is_super_perrun;
        $traspaso->is_pelo_a_pelo = $data[$i]->is_pelo_a_pelo;
        $traspaso->added_top_points = (isset($data[$i]->added_top_points))?$data[$i]->added_top_points:0;
        $traspaso->added_perrunflas_points = (isset($data[$i]->added_perrunflas_points))?$data[$i]->added_perrunflas_points:0;

        // error_log("Traspaso creado, added top points: " . $traspaso->added_top_points . " / " . $data[$i]->added_top_points);
        // error_log(", perrun: " . $traspaso->added_perrunflas_points . " / " . $data[$i]->added_perrunflas_points);

        if($blockId != 0)
        {
            
            $traspaso->engaged_to = $blockId;
        }

        $id = $traspaso->create();
        $traspaso->id = $id;
        if($blockId == 0 && $id != -1)
        {
            $blockId = $id;
            $traspaso->engaged_to = $blockId;
            
            $successfullTransaction = $traspaso->update_engaged_id(); // Asocio al traspaso que identifica al bloque a sí mismo.

        }

        $successfullTransaction = $successfullTransaction && ($id != -1); // Falló la inserción de un traspaso.
    }
    if($successfullTransaction)
    {
        $db->commit();

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "traspasos were created."));
    }
    else
    {
        $db->rollBack();
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create traspasos."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create traspasos. Data is incomplete."));
}
?>