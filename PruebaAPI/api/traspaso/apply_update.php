<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
include_once '../objects/usuario.php';
include_once '../objects/equipo.php';
include_once '../objects/jugador.php';
include_once '../objects/periodo.php';
 
$database = new Database();
$db = $database->getConnection();
 
$traspaso = new Traspaso($db);

 

$data = json_decode(file_get_contents("php://input"));
/**
* ESPERO:
*    { 
*       team_id: [[team_id]],
*       update_id: [[update_id]]
*    }
*/


if(isset($data->update_id) 
    && isset($data->team_id))
{
    $result = $traspaso->apply_update($data);
    http_response_code(201);
    echo json_encode($result);
}
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to aplly transfers. Data is incomplete."));
}
?>