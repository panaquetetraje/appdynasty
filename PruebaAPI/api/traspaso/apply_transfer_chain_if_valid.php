<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
include_once '../objects/usuario.php';
include_once '../objects/equipo.php';
include_once '../objects/jugador.php';
include_once '../objects/periodo.php';
 
$database = new Database();
$db = $database->getConnection();
 
$traspaso = new Traspaso($db);

 

$data = json_decode(file_get_contents("php://input"));
/**
* ESPERO:
*    { 
*       cadena_id:[[cadena_id]]
*    }
*/
$resultadoBloque = $traspaso->get_is_valid_chain($data->cadena_id);

// error_log("Revisando si es válida la cadena {$data->cadena_id}: " . (($resultadoBloque)?"Es válida":"No es válida"));
if($resultadoBloque != null)
{
    if($resultadoBloque['cadenaValida'] && $traspaso->apply_chain_transfers($resultadoBloque))
    {
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($resultadoBloque);
    }
    else
    {
        // set response code - 400 service unavailable
        http_response_code(400);
 
        // tell the user
        echo json_encode($resultadoBloque);
    }

}
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to aplly transfers. Data is incomplete."));
}
?>