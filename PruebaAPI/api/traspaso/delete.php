<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate traspaso object
include_once '../objects/traspaso.php';
 
$database = new Database();
$db = $database->getConnection();
 

 
// get posted data
$data = json_decode(file_get_contents("php://input"));

$validData = isset($data->idCadenaTraspaso);

if($validData)
{
    $db->beginTransaction();
    $traspaso = new Traspaso($db);
    $traspaso ->engaged_to = $data->idCadenaTraspaso;
    $res = $traspaso->delete();
    
    if($res)
    {
        $db->commit();

        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "traspasos were deleted."));
    }
    else
    {
        $db->rollBack();
        
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to delete traspasos."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to delete traspasos. Data is incomplete."));
}
?>