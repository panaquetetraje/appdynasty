<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/usuario.php';
include_once '../objects/equipo.php';
 
// instantiate database and usuario object
$database = new Database();
$db = $database->getConnection();
 

$db->beginTransaction();

// initialize object
$usuario = new Usuario($db);

$data = json_decode(file_get_contents("php://input"));
$usuario->name = $data->previous_user_name;
$usuario->read_one();

/**
* Actualizo información del usuario.
*/
if(isset($data->user_name) && $data->user_name != "")
{
    $nuevoNombre = $data->user_name;
}
else
{
    $nuevoNombre = $usuario->name;
}

if(isset($data->pass) && $data->pass != "")
{
    $nuevaClave = $data->pass;
}
else
{
    $nuevaClave = $usuario->pass;
}

$nuevoUsuarioPSN = (isset($data->usuario_psn) && $data->usuario_psn != "")?$data->usuario_psn:$usuario->usuario_psn;
$nuevoCanalYoutube = (isset($data->canal_youtube) && $data->canal_youtube != "")?$data->canal_youtube:$usuario->canal_youtube;

$usuarioActualizado = $usuario->update($nuevoNombre, $nuevaClave, $nuevoUsuarioPSN, $nuevoCanalYoutube);


/**
* Actualizo información del equipo.
*/
$equipo = new Equipo($db);
$equipo->read_from_owner($usuario->name);

if(isset($data->team_name) && $data->team_name != "")
{
    $nuevoNombreEquipo = $data->team_name;
}
else
{
    $nuevoNombreEquipo = $equipo->name;
}
$nuevaUriSofifa = (isset($data->uri_sofifa_team) && $data->uri_sofifa_team != "")?$data->uri_sofifa_team:$equipo->sofifa_team_uri;
$nuevaUrlLogo = (isset($data->url_logo_equipo) && $data->url_logo_equipo != "")?$data->url_logo_equipo:$equipo->logo;

$equipoActualizado = $equipo->update($nuevoNombreEquipo, $nuevaUriSofifa, $nuevaUrlLogo);

if($equipoActualizado && $usuarioActualizado)
{
    $db->commit();
    http_response_code(200);
    echo json_encode(array("result" => "ok"));
}
else
{
    $db->rollBack();
    http_response_code(200);
    echo json_encode(array("result" => "error"));
}

 
// make it json format

?>