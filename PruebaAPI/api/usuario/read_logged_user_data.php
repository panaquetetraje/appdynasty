<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/usuario.php';
include_once '../objects/equipo.php';
 
// instantiate database and usuario object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$usuario = new Usuario($db);
 

$data = json_decode(file_get_contents("php://input"));
$usuario->name = $data->name;

// query usuarios
$stmt = $usuario->read_logged_user_data();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    extract($row);

    $equipo = new Equipo($db);
    $equipo->id = $team_id;
    $equipo->read_one();
    $suma_tops = $equipo->get_suma_jugadores(1);
    $suma_real_tops = $equipo->get_suma_real_jugadores($team_id, 1);
    $suma_perrunflas = $equipo->get_suma_jugadores(0);
    $suma_real_perrunflas = $equipo->get_suma_real_jugadores($team_id, 0);

    $usuario_item = array(
            "result" => true,
            "name" => $name,
            "team_name" => $team_name,
            "team_id" => $team_id,
            "top_budget" => $top_budget,
            "perrunflas_budget" => $perrunflas_budget,
            "suma_tops" => $suma_tops,
            "suma_real_tops" => $suma_real_tops,
            "suma_perrunflas" => $suma_perrunflas,
            "suma_real_perrunflas" => $suma_real_perrunflas,
            "fylyal_id" => $equipo->fylyal_id,
            "fylyal_name" => $equipo->fylyal_name,
            "logo" => $equipo->logo,
            "sofifa_team_uri" => $equipo->sofifa_team_uri,
            "canal_youtube" => $row['canal_youtube'],
            "usuario_psn" => $row['usuario_psn']
        );

    
    http_response_code(200);
 
    // make it json format
    echo json_encode($usuario_item);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no usuarios found
    echo json_encode(
        array("message" => "No usuarios found.")
    );
}
?>