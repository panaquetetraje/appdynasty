<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/usuario.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare usuario object
$usuario = new Usuario($db);

// get product id
$data = json_decode(file_get_contents("php://input"));

// set name property of record to read
$usuario->name = $data->name;
$usuario->pass = $data->pass;

// error_log("Recibí: " . $usuario->name . " y " . $usuario->pass);






/*
$usuario_item=array(
            "result" => false,
            "name" => 'mensaje de error',
            "is_admin" => false
        );
http_response_code(200);
 
// make it json format
echo json_encode($usuario_item);
*/




$stmt = $usuario->login();
$num = $stmt->rowCount();

if($num > 0){
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    extract($row);
    $usuario_item=array(
    		"result" => true,
            "name" => $name,
            "is_admin" => ($is_admin != 0)
        );

    
    http_response_code(200);
 
    // make it json format
    echo json_encode($usuario_item);
}
 
else{
    // set response code - 200 OK
    http_response_code(200);
 
    // tell the user usuario does not exist
    echo json_encode(array(
        "result" => false, 
        "name" => "",
        "is_admin" => false
    ));
}
?>