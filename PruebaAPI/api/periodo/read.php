<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/periodo.php';
 
// instantiate database and periodo object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$periodo = new Periodo($db);
 
// query periodos
$stmt = $periodo->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // periodos array
    $periodos_arr=array();
    $periodos_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['pass'] to
        // just $pass only
        extract($row);
 
        $periodo_item=array(
            "id" => $id,
            "fechaInicio" => $fecha_inicio,
            "fechaFin" => $fecha_fin
        );
 
        array_push($periodos_arr["records"], $periodo_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show periodos data in json format
    echo json_encode($periodos_arr);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no periodos found
    echo json_encode(
        array("message" => "No periodos found.")
    );
}
?>