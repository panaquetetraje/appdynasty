<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/periodo.php';
 
// instantiate database and periodo object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$periodo = new Periodo($db);
 
$periodo_item=array(
        "enPeriodoDeTraspasos" => $periodo->getActualmenteEnPeriodo(),
        "proximoCierre" => $periodo->getProximoCierre(),
        "proximoInicio" => $periodo->getProximoInicio()
    );

// set response code - 200 OK
http_response_code(200);

// show periodos data in json format
echo json_encode($periodo_item);
?>