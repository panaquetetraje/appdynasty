<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/core.php';
include_once '../config/database.php';
include_once '../objects/jugador.php';
 
// instantiate database and jugador object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$jugador = new Jugador($db);
 
// get keywords
$min=isset($_GET["min"]) ? $_GET["min"] : 50;
$max=isset($_GET["max"]) ? $_GET["max"] : 150;
 
// query jugadores
$stmt = $jugador->listar_jugadores_libres($min, $max);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // jugadores array
    $jugadores_arr=array();
    $jugadores_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $jugador_item=array(
            "id" => $player_id,
            "name" => $name,
            "overall_rating" => $overall_rating,
            "team_id" => $team_id,
            "team_name" => $team_name,
            "price_paid" => $price_paid,
            "is_top" => $is_top
        );
 
        array_push($jugadores_arr["records"], $jugador_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show jugadores data
    echo json_encode($jugadores_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no jugadores found
    echo json_encode(
        array("message" => "No jugadores found.")
    );
}
?>