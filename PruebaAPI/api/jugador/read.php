<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/jugador.php';
 
// instantiate database and jugador object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$jugador = new Jugador($db);
 
// query jugadores
$stmt = $jugador->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // jugadores array
    $jugadores_arr=array();
    $jugadores_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $jugador_item=array(
            "id" => $id,
            "name" => $name,
            "overall_rating" => $overall_rating,
            "is_top" => ($is_top!=null)?$is_top:0,
            "team_id" => ($team_id!=null)?$team_id:0,
            "team_name" => ($team_name!=null)?$team_name:"Sin equipo",
            "price_paid" => ($price_paid!=null)?$price_paid:0
        );
 
        array_push($jugadores_arr["records"], $jugador_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show jugadores data in json format
    echo json_encode($jugadores_arr);
}
else{
 
    // set response code - 404 Not found
    http_response_code(200);
 
    // tell the user no jugadores found
    echo json_encode(
        array("message" => "No jugadores found.")
    );
}
?>