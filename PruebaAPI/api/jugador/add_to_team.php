<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate jugador object
include_once '../objects/jugador.php';
 
$database = new Database();
$db = $database->getConnection();
 
$jugador = new Jugador($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

    
// make sure data is not empty
if(
    !empty($data->player_id) &&
    !empty($data->team_id) &&
    !empty($data->price_paid)
){
 
    // set jugador property values
    $jugador->id = $data->player_id;
    $jugador->team_id = $data->team_id;
    $jugador->price_paid = $data->price_paid;
    $jugador->is_top = $data->is_top;


        // create the jugador
    if($jugador->add_to_team(0)){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "jugador was added to team"));
    }
 
    // if unable to create the jugador, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to add jugador to team: " . $data->player_id . " / " . $data->team_id  . " / " . $data->price_paid . " / " . $data->is_top));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to add jugador to team. Data is incomplete."));
}
?>