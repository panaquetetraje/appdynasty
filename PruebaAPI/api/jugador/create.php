<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate jugador object
include_once '../objects/jugador.php';
 
$database = new Database();
$db = $database->getConnection();
 
$jugador = new Jugador($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
/*
error_log("Recibido para crear jugador:");
error_log($data->id);
error_log($data->name);
error_log($data->overall_rating);
*/
// make sure data is not empty
if(
    !empty($data->id) &&
    !empty($data->name) &&
    //!empty($data->valor_de_compra) &&
    !empty($data->overall_rating)
/*
    !empty($data->id)&&
    !empty($data->name)&&
    !empty($data->photo_url)&&
    !empty($data->age)&&
    !empty($data->birth_date)&&
    !empty($data->height)&&
    !empty($data->weight)&&
    !empty($data->overall_rating)&&
    !empty($data->potential)&&
    !empty($data->value)&&
    !empty($data->wage)&&
    !empty($data->preferred_foot)&&
    !empty($data->international_reputation)&&
    !empty($data->weak_foot)&&
    !empty($data->skill_moves)&&
    !empty($data->work_rate)&&
    !empty($data->body_type)&&
    !empty($data->real_face)&&
    !empty($data->release_clause)&&
    !empty($data->crossing)&&
    !empty($data->finishing)&&
    !empty($data->heading_accuracy)&&
    !empty($data->short_passing)&&
    !empty($data->volleys)&&
    !empty($data->dribbling)&&
    !empty($data->curve)&&
    !empty($data->fk_accuracy)&&
    !empty($data->long_passing)&&
    !empty($data->ball_control)&&
    !empty($data->acceleration)&&
    !empty($data->sprint_speed)&&
    !empty($data->agility)&&
    !empty($data->reactions)&&
    !empty($data->balance)&&
    !empty($data->shot_power)&&
    !empty($data->jumping)&&
    !empty($data->stamina)&&
    !empty($data->strength)&&
    !empty($data->long_shots)&&
    !empty($data->aggression)&&
    !empty($data->interceptions)&&
    !empty($data->positioning)&&
    !empty($data->vision)&&
    !empty($data->penalties)&&
    !empty($data->composure)&&
    !empty($data->marking)&&
    !empty($data->standing_tackle)&&
    !empty($data->sliding_tackle)&&
    !empty($data->gk_diving)&&
    !empty($data->gk_handling)&&
    !empty($data->gk_kicking)&&
    !empty($data->gk_positioning)&&
    !empty($data->gk_reflexes)){
*/

){
 
    // set jugador property values
    $jugador->id = $data->id;
    $jugador->name = $data->name;
    $jugador->overall_rating = $data->overall_rating;
    
    //$jugador->valor_de_compra = $data->valor_de_compra;
    // set jugador property values


/*
    $jugador->id = $data->id;
    $jugador->name = $data->name;
    $jugador->photo_url = $data->photo_url;
    $jugador->age = $data->age;
    $jugador->birth_date = $data->birth_date;
    $jugador->height = $data->height;
    $jugador->weight = $data->weight;
    $jugador->overall_rating = $data->overall_rating;
    $jugador->potential = $data->potential;
    $jugador->value = $data->value;
    $jugador->wage = $data->wage;
    $jugador->preferred_foot = $data->preferred_foot;
    $jugador->international_reputation = $data->international_reputation;
    $jugador->weak_foot = $data->weak_foot;
    $jugador->skill_moves = $data->skill_moves;
    $jugador->work_rate = $data->work_rate;
    $jugador->body_type = $data->body_type;
    $jugador->real_face = $data->real_face;
    $jugador->release_clause = $data->release_clause;
    $jugador->crossing = $data->crossing;
    $jugador->finishing = $data->finishing;
    $jugador->heading_accuracy = $data->heading_accuracy;
    $jugador->short_passing = $data->short_passing;
    $jugador->volleys = $data->volleys;
    $jugador->dribbling = $data->dribbling;
    $jugador->curve = $data->curve;
    $jugador->fk_accuracy = $data->fk_accuracy;
    $jugador->long_passing = $data->long_passing;
    $jugador->ball_control = $data->ball_control;
    $jugador->acceleration = $data->acceleration;
    $jugador->sprint_speed = $data->sprint_speed;
    $jugador->agility = $data->agility;
    $jugador->reactions = $data->reactions;
    $jugador->balance = $data->balance;
    $jugador->shot_power = $data->shot_power;
    $jugador->jumping = $data->jumping;
    $jugador->stamina = $data->stamina;
    $jugador->strength = $data->strength;
    $jugador->long_shots = $data->long_shots;
    $jugador->aggression = $data->aggression;
    $jugador->interceptions = $data->interceptions;
    $jugador->positioning = $data->positioning;
    $jugador->vision = $data->vision;
    $jugador->penalties = $data->penalties;
    $jugador->composure = $data->composure;
    $jugador->marking = $data->marking;
    $jugador->standing_tackle = $data->standing_tackle;
    $jugador->sliding_tackle = $data->sliding_tackle;
    $jugador->gk_diving = $data->gk_diving;
    $jugador->gk_handling = $data->gk_handling;
    $jugador->gk_kicking = $data->gk_kicking;
    $jugador->gk_positioning = $data->gk_positioning;
    $jugador->gk_reflexes = $data->gk_reflexes;
*/


        // create the jugador
    if($jugador->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "jugador was created."));
    }
 
    // if unable to create the jugador, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create jugador."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create jugador. Data is incomplete."));
}
?>