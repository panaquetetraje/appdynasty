<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/jugador.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare jugador object
$jugador = new Jugador($db);
 
// get id of jugador to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set ID property of jugador to be edited
$jugador->id = $data->id;
$jugador->name = $data->name;
$jugador->overall_rating = $data->overall_rating;
 

// error_log("Updating: ");
// error_log($jugador->id);
// error_log($jugador->name);
// error_log($jugador->overall_rating);
// update the jugador
if($jugador->update()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "Jugador was updated."));
}
 
// if unable to update the jugador, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update jugador."));
}
?>