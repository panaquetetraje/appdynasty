<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/jugador.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare jugador object
$jugador = new Jugador($db);
 
// set ID property of record to read
$jugador->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of jugador to be edited
$jugador->read_one();
 
if($jugador->name!=null){
    // create array
    $jugador_arr = array(
        "id" =>  $jugador->id,
        "name" => $jugador->name,
        "overall_rating" => $jugador->overall_rating,
        "team_id" => $jugador->team_id,
        "team_name" => $jugador->team_name,
        "team_top_budget" => $jugador->team_top_budget,
        "team_perrunflas_budget" => $jugador->team_perrunflas_budget,
        "price_paid" => $jugador->price_paid,
        "is_top" => $jugador->is_top,
        "esSuperPerrunfla" => $jugador->esSuperPerrunfla,
        "real_sell_price" => $jugador->real_sell_price
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($jugador_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user jugador does not exist
    echo json_encode(array("message" => "Jugador does not exist."));
}
?>