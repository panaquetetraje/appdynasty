SET @idEquipoABorrar = 989002;





/* Desato los intercambios en los que participa */
UPDATE intercambios set engaged_to = null where buyer_id = @idEquipoABorrar or seller_id = @idEquipoABorrar;
/* Desato los intercambios que referencian a intercambios en los que participa */

/* Borro todos los intercambios en los que participa (No anduvo, seleccionar a mano todos y borrarlos sin chequeo de foraneas) */
DELETE from intercambios where buyer_id = @idEquipoABorrar or seller_id = @idEquipoABorrar;
/* Borro todas las relaciones de un jugador con este equipo */
delete from jugadores_equipos where team_id = @idEquipoABorrar;
/* Borro toornament */
delete from log_ingresos_de_resultados where id_equipo = @idEquipoABorrar;
/* Borro toornament */
delete from equipos_en_campeonatos_toornament where id_dynasty_equipo = @idEquipoABorrar;
/* Borro el usuario */
Delete from usuarios where name = (select e.owner from equipos e where id = @idEquipoABorrar);
/* Borro el equipo */
Delete from equipos where id = @idEquipoABorrar;