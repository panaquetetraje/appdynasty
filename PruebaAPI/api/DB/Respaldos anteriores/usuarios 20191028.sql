-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-10-2019 a las 19:36:19
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id10281982_dynasty`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `name` varchar(30) NOT NULL DEFAULT '',
  `pass` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`name`, `pass`, `is_admin`) VALUES
('admin', 'dynastyadmin', 1),
('aek athens', 'aek athens', 0),
('Anonymo', 'Anonymo', 0),
('Anonymo1', 'Anonymo1', 0),
('Anonymo10', 'Anonymo10', 0),
('Anonymo11', 'Anonymo11', 0),
('Anonymo12', 'Anonymo12', 0),
('Anonymo13', 'Anonymo13', 0),
('Anonymo14', 'Anonymo14', 0),
('Anonymo15', 'Anonymo15', 0),
('Anonymo16', 'Anonymo16', 0),
('Anonymo17', 'Anonymo17', 0),
('Anonymo18', 'Anonymo18', 0),
('Anonymo19', 'Anonymo19', 0),
('Anonymo2', 'Anonymo2', 0),
('Anonymo20', 'Anonymo20', 0),
('Anonymo3', 'Anonymo3', 0),
('Anonymo4', 'Anonymo4', 0),
('Anonymo5', 'Anonymo5', 0),
('Anonymo6', 'Anonymo6', 0),
('Anonymo7', 'Anonymo7', 0),
('Anonymo8', 'Anonymo8', 0),
('Anonymo9', 'Anonymo9', 0),
('Arsenal', 'Arsenal', 0),
('Barcelona', 'Barcelona', 0),
('Benevento', 'Benevento', 0),
('Besiktas J.K.', 'Besiktas J.K.', 0),
('Borussia Dortmund', 'Borussia Dortmund', 0),
('Dallas F.C.', 'Dallas F.C.', 0),
('Fenerbahce', 'Fenerbahce', 0),
('Frankfurt', 'Frankfurt', 0),
('Gimnasia LP', 'Gimnasia LP', 0),
('Groningen', 'Groningen', 0),
('Hoffenheim', 'Hoffenheim', 0),
('Kaiser Chiefs', 'Kaiser Chiefs', 0),
('Liverpool', 'Liverpool', 0),
('Lyon', 'Lyon', 0),
('Millonarios', 'Millonarios', 0),
('Millwall F.C.', 'Millwall F.C.', 0),
('Montreal Impact', 'Montreal Impact', 0),
('Nottingham Forest', 'Nottingham Forest', 0),
('Nunez', 'Nunez', 0),
('Panathinaikos', 'Panathinaikos', 0),
('Pumas U.N.A.M', 'Pumas U.N.A.M', 0),
('santiago', 'cecilia', 0),
('Sparta Praga', 'Sparta Praga', 0),
('St. Pauli', 'St. Pauli', 0),
('Torino', 'Torino', 0),
('Tottenham', 'Tottenham', 0),
('Wolfsburgo', 'Wolfsburgo', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
