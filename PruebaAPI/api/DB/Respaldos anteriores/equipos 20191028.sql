-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-10-2019 a las 19:32:58
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id10281982_dynasty`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `top_budget` decimal(8,2) DEFAULT NULL,
  `perrunflas_budget` decimal(8,2) DEFAULT NULL,
  `owner` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `name`, `top_budget`, `perrunflas_budget`, `owner`) VALUES
(1, 'Mercado Asiático', 0.00, 0.00, 'admin'),
(9, 'AEK Athens', 861.50, 652.00, 'AEK Athens'),
(10, 'Arsenal', 852.50, 650.00, 'Arsenal'),
(11, 'Barcelona', 851.00, 650.00, 'Barcelona'),
(12, 'Benevento', 876.50, 652.00, 'Benevento'),
(13, 'Besiktas J.K.', 866.00, 653.00, 'Besiktas J.K.'),
(14, 'Borussia Dortmund', 892.50, 665.00, 'Borussia Dortmund'),
(15, 'Dallas F.C.', 857.00, 652.00, 'Dallas F.C.'),
(16, 'Fenerbahce', 877.00, 665.00, 'Fenerbahce'),
(17, 'Hoffenheim', 864.00, 645.00, 'Hoffenheim'),
(18, 'Gimnasia LP', 873.25, 662.50, 'Gimnasia LP'),
(19, 'Nottingham Forest', 847.50, 650.00, 'Nottingham Forest'),
(20, 'Kaiser Chiefs', 871.50, 655.00, 'Kaiser Chiefs'),
(21, 'Liverpool', 861.50, 646.00, 'Liverpool'),
(22, 'Lyon', 862.00, 657.00, 'Lyon'),
(23, 'Frankfurt', 857.00, 656.00, 'Frankfurt'),
(24, 'Groningen', 850.25, 657.00, 'Groningen'),
(25, 'Millonarios', 860.75, 654.00, 'Millonarios'),
(26, 'Millwall F.C.', 878.25, 665.00, 'Millwall F.C.'),
(27, 'Montreal Impact', 886.25, 656.00, 'Montreal Impact'),
(28, 'Panathinaikos', 877.00, 664.00, 'Panathinaikos'),
(29, 'St. Pauli', 862.25, 652.00, 'St. Pauli'),
(30, 'Pumas U.N.A.M', 853.75, 650.00, 'Pumas U.N.A.M'),
(31, 'Nunez', 867.00, 651.50, 'Nunez'),
(32, 'Sparta Praga', 853.75, 651.00, 'Sparta Praga'),
(33, 'Tottenham', 878.00, 657.00, 'Tottenham'),
(34, 'Torino F.C.', 885.00, 669.00, 'Torino'),
(35, 'Wolfsburgo', 851.00, 652.00, 'Wolfsburgo'),
(36, 'Leicester', 850.00, 650.00, 'santiago'),
(38, 'Anonymo', 850.00, 650.00, 'Anonymo'),
(999001, 'Anonymo1', 850.00, 650.00, 'Anonymo1'),
(999002, 'Anonymo2', 850.00, 650.00, 'Anonymo2'),
(999003, 'Anonymo3', 850.00, 650.00, 'Anonymo3'),
(999004, 'Anonymo4', 850.00, 650.00, 'Anonymo4'),
(999005, 'Anonymo5', 850.00, 650.00, 'Anonymo5'),
(999006, 'Anonymo6', 850.00, 650.00, 'Anonymo6'),
(999007, 'Anonymo7', 850.00, 650.00, 'Anonymo7'),
(999008, 'Anonymo8', 850.00, 650.00, 'Anonymo8'),
(999009, 'Anonymo9', 850.00, 650.00, 'Anonymo9'),
(999010, 'Anonymo10', 850.00, 650.00, 'Anonymo10'),
(999011, 'Anonymo11', 850.00, 650.00, 'Anonymo11'),
(999012, 'Anonymo12', 850.00, 650.00, 'Anonymo12'),
(999013, 'Anonymo13', 850.00, 650.00, 'Anonymo13'),
(999014, 'Anonymo14', 850.00, 650.00, 'Anonymo14'),
(999015, 'Anonymo15', 850.00, 650.00, 'Anonymo15'),
(999016, 'Anonymo16', 850.00, 650.00, 'Anonymo16'),
(999017, 'Anonymo17', 850.00, 650.00, 'Anonymo17'),
(999018, 'Anonymo18', 850.00, 650.00, 'Anonymo18'),
(999019, 'Anonymo19', 850.00, 650.00, 'Anonymo19'),
(999020, 'Anonymo20', 850.00, 650.00, 'Anonymo20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=999021;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `usuarios` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
