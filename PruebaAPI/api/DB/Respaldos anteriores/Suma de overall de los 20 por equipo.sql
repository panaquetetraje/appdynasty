select e.name, sum(j.overall_rating) as "suma20Jugadores"
from jugadores_equipos je
inner join jugadores j on j.id = je.player_id
inner join equipos e on e.id = je.team_id
where e.name not like "%ylyal%"
    AND e.name not like "%nonymo%"
group by e.id
order by suma20Jugadores desc;