-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-08-2020 a las 23:54:18
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id10281982_dynasty`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `top_budget` decimal(8,2) DEFAULT NULL,
  `perrunflas_budget` decimal(8,2) DEFAULT NULL,
  `owner` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `name`, `top_budget`, `perrunflas_budget`, `owner`) VALUES
(1, 'Mercado Asiático', 0.00, 0.00, 'admin'),
(9, 'AEK Athens', 874.00, 666.00, 'Athenas'),
(12, 'Rayo Vallecano', 889.50, 667.00, 'rayoa'),
(14, 'PSG', 911.50, 669.00, 'psg'),
(16, 'Fenerbahce', 881.25, 665.00, 'Fenerbahce'),
(17, 'Racing', 865.00, 665.00, 'Hoffenheim'),
(18, 'Gimnasia LP', 883.50, 667.00, 'Gimnasia LP'),
(20, 'América', 880.00, 665.00, 'America'),
(22, 'Lyon', 872.00, 665.00, 'Lyon'),
(24, 'Groningen', 874.25, 665.00, 'Groningen'),
(26, 'Millwall F.C.', 886.50, 666.00, 'mill'),
(27, 'Montreal Impact', 891.00, 665.00, 'Elimpact'),
(28, 'Panathinaikos', 888.50, 669.00, 'pana'),
(29, 'St. Pauli', 882.50, 667.00, 'St. Pauli'),
(31, 'SBV Vitesse', 878.25, 663.00, 'carpeta'),
(33, 'SM Caen', 894.50, 665.00, 'caen'),
(34, 'Torino F.C.', 905.00, 677.00, 'Torino'),
(36, 'Leicester', 867.75, 670.00, 'Leicester'),
(38, 'Anonymo', 865.00, 665.00, 'Anonymo'),
(989001, 'FC Twente', 770.00, 630.00, 'Twente'),
(989002, 'Napoli', 770.00, 630.00, 'Napoli'),
(989003, 'Sunderland AFC', 770.00, 630.00, 'sunderland'),
(989004, 'St. Pauli II', 770.00, 630.00, 'St. Pauli B'),
(989005, 'Kaiser Chiefs', 770.00, 632.00, 'Kaiser'),
(989006, 'Rayo Vallecano B', 771.50, 630.00, 'rayob'),
(989007, 'Fylyal07', 770.00, 630.00, 'Fylyal07'),
(989008, 'Defensa y Justicia', 770.00, 630.00, 'panafylyal'),
(989009, 'Fylyal09', 770.00, 630.00, 'Fylyal09'),
(989010, 'Cambridge United', 770.00, 630.00, 'carpeta2'),
(989011, 'Bolton Wanderers', 770.00, 630.00, 'Fylyal11'),
(989012, 'Fylyal12', 770.00, 630.00, 'Fylyal12'),
(989013, 'Fylyal13', 770.00, 630.00, 'Fylyal13'),
(989014, 'Racing Fylyal', 770.00, 630.00, 'Fylyal14'),
(989015, 'Deportivo de la Coruña', 773.00, 630.00, 'Depor'),
(989016, 'Sheffield United F. C.', 770.00, 630.00, 'Fylyal16'),
(989017, 'Sparta Praha', 770.00, 630.00, 'Lucca2'),
(989018, 'Real Madrid', 770.00, 630.00, 'lugian2'),
(989019, 'Fylyal19', 770.00, 630.00, 'Fylyal19'),
(989020, 'West Bromwich Albion', 770.00, 630.00, 'Leicester2'),
(989021, 'Milan', 770.00, 630.00, 'Lubi2'),
(989022, 'Midtjylland', 770.00, 630.00, 'Midtjylland'),
(989023, 'Atlético Madrid', 770.00, 630.00, 'Amadrid'),
(989024, 'Real Betis', 770.00, 630.00, 'Athenas2'),
(989025, 'Fylyal25', 770.00, 630.00, 'Fylyal25'),
(989026, 'Fylyal26', 770.00, 630.00, 'Fylyal26'),
(989027, 'Fylyal27', 770.00, 630.00, 'Fylyal27'),
(989028, 'Fylyal28', 770.00, 630.00, 'Fylyal28'),
(989029, 'Fylyal29', 770.00, 630.00, 'Fylyal29'),
(989030, 'Fylyal30', 770.00, 630.00, 'Fylyal30'),
(989031, 'Slavia Praha', 770.00, 630.00, 'Fylyal31'),
(989032, 'Fylyal32', 770.00, 630.00, 'Fylyal32'),
(989033, 'Fylyal33', 770.00, 630.00, 'Fylyal33'),
(989034, 'Fylyal34', 770.00, 630.00, 'Fylyal34'),
(989035, 'Fylyal35', 770.00, 630.00, 'Fylyal35'),
(989036, 'Fylyal36', 770.00, 630.00, 'Fylyal36'),
(989037, 'Fylyal37', 770.00, 630.00, 'Fylyal37'),
(989038, 'Fylyal38', 770.00, 630.00, 'Fylyal38'),
(989039, 'Fylyal39', 770.00, 630.00, 'Fylyal39'),
(989040, 'Independiente', 770.00, 630.00, 'Independiente'),
(989041, 'Fylyal41', 770.00, 630.00, 'Fylyal41'),
(989042, 'Fylyal42', 770.00, 630.00, 'Fylyal42'),
(989043, 'Fylyal43', 770.00, 630.00, 'Fylyal43'),
(989044, 'Fylyal44', 770.00, 630.00, 'Fylyal44'),
(989045, 'Fylyal45', 770.00, 630.00, 'Fylyal45'),
(989046, 'Fylyal46', 770.00, 630.00, 'Fylyal46'),
(989047, 'Fylyal47', 770.00, 630.00, 'Fylyal47'),
(989048, 'Fylyal48', 770.00, 630.00, 'Fylyal48'),
(989049, 'Fylyal49', 770.00, 630.00, 'Fylyal49'),
(989050, 'Fylyal50', 770.00, 630.00, 'Fylyal50'),
(999001, 'Besiktas JK', 871.25, 668.00, 'Besiktas'),
(999002, 'Anonymo2', 865.00, 665.00, 'Anonymo2'),
(999003, 'Birmingham', 870.00, 665.00, 'Lubi'),
(999004, 'Chelsea', 873.75, 665.00, 'lugian'),
(999005, 'Inter', 870.00, 665.00, 'Lucca'),
(999006, 'Roma', 866.25, 665.00, 'Enzo'),
(999007, 'Liverpool', 873.75, 665.00, 'LFC'),
(999008, 'Ajax', 867.50, 665.00, 'Ajax'),
(999009, 'Borussia Dortmund', 865.00, 665.00, 'Boru'),
(999010, 'RB Liepzig', 865.00, 665.00, 'Liepzing'),
(999011, 'Millonarios', 865.00, 665.00, 'Nannes'),
(999012, 'Fiorentina', 865.00, 665.00, 'Fiore'),
(999013, 'Anonymo13', 865.00, 665.00, 'Anonymo13'),
(999014, 'Anonymo14', 865.00, 665.00, 'Anonymo14'),
(999015, 'Anonymo15', 865.00, 665.00, 'Anonymo15'),
(999016, 'Anonymo16', 865.00, 665.00, 'Anonymo16'),
(999017, 'Anonymo17', 865.00, 665.00, 'Anonymo17'),
(999018, 'Anonymo18', 865.00, 665.00, 'Anonymo18'),
(999019, 'Anonymo19', 865.00, 665.00, 'Anonymo19'),
(999020, 'Anonymo20', 865.00, 665.00, 'Anonymo20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=999021;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `usuarios` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
