--
-- ELEGIR TODOS LOS JUGADORES DEL PANA.
--
SELECT j.* 
FROM jugadores_equipos je, jugadores j
WHERE je.player_id = j.id
	AND je.team_id = 28
	ORDER BY j.overall_rating desc;


--
-- ELEGIR TODAS LAS CADENAS DE TRASPASOS.
--
SELECT *
FROM intercambios
GROUP BY engaged_to;

--
-- ELEGIR LOS INTERCAMBIOS CON EL NOMBRE DEL JUGADOR Y LOS NOMBRES DE LOS EQUIPOS.
--
SELECT ev.name as equipo_que_vende, ec.name as equipo_que_compra, j.name , i.sell_price, i.is_pending, i.buyer_accepted, i.seller_accepted
FROM equipos ev, equipos ec, intercambios i, jugadores j
WHERE i.player_id = j.id
	AND ev.id = i.seller_id
	AND ec.id = i.buyer_id

--
-- ELEGIR AL PANA
--
SELECT * FROM `equipos`
WHERE id = 28;


--
-- ELEGIR TODAS LAS CADENAS DEL PANA.
--
SELECT * FROM intercambios
WHERE engaged_to IN(
	SELECT DISTINCT(engaged_to) FROM intercambios
	WHERE (buyer_id != 1
		AND seller_id = 28)
	OR (seller_id != 1
		AND buyer_id = 28)
);

--
-- ELEGIR TODAS LAS NO CADENAS DEL PANA.
--
SELECT * FROM intercambios
WHERE engaged_to NOT IN(
	SELECT DISTINCT(engaged_to) FROM intercambios
	WHERE (buyer_id != 1
		AND seller_id = 28)
	OR (seller_id != 1
		AND buyer_id = 28)
);

--
-- ELEGIR COMO MÁXIMO UN JUGADOR VALOR 75 POR EQUIPO.
--
SELECT j.*, je.price_paid, e.name 
FROM `jugadores_equipos` je, equipos e, jugadores j 
WHERE je.price_paid = 75 
AND j.overall_rating = 75 
AND je.player_id = j.id 
AND je.team_id = e.id 
GROUP by e.name



-- 
-- ACTUALIZAR PRESUPUESTOS A DESPUÉS DE T6 INTERMEDIO.
-- 

UPDATE equipos SET top_budget = 860.5, perrunflas_budget=651 WHERE id=9;
UPDATE equipos SET top_budget = 852, perrunflas_budget=650 WHERE id=10;
UPDATE equipos SET top_budget = 850.5, perrunflas_budget=650 WHERE id=11;
UPDATE equipos SET top_budget = 874.5, perrunflas_budget=652 WHERE id=12;
UPDATE equipos SET top_budget = 866, perrunflas_budget=653 WHERE id=13;
UPDATE equipos SET top_budget = 887.5, perrunflas_budget=665 WHERE id=14;
UPDATE equipos SET top_budget = 856.5, perrunflas_budget=652 WHERE id=15;
UPDATE equipos SET top_budget = 874, perrunflas_budget=665 WHERE id=16;
UPDATE equipos SET top_budget = 864.5, perrunflas_budget=649 WHERE id=17;
UPDATE equipos SET top_budget = 871.25, perrunflas_budget=660.5 WHERE id=18;
UPDATE equipos SET top_budget = 847.5, perrunflas_budget=650 WHERE id=19;
UPDATE equipos SET top_budget = 869, perrunflas_budget=655 WHERE id=20;
UPDATE equipos SET top_budget = 861.5, perrunflas_budget=646 WHERE id=21;
UPDATE equipos SET top_budget = 861.25, perrunflas_budget=657 WHERE id=22;
UPDATE equipos SET top_budget = 853.5, perrunflas_budget=650 WHERE id=23;
UPDATE equipos SET top_budget = 850.25, perrunflas_budget=650 WHERE id=24;
UPDATE equipos SET top_budget = 859.5, perrunflas_budget=654 WHERE id=25;
UPDATE equipos SET top_budget = 875.25, perrunflas_budget=665 WHERE id=26;
UPDATE equipos SET top_budget = 877.75, perrunflas_budget=656 WHERE id=27;
UPDATE equipos SET top_budget = 874, perrunflas_budget=662 WHERE id=28;
UPDATE equipos SET top_budget = 859.5, perrunflas_budget=652 WHERE id=29;
UPDATE equipos SET top_budget = 852.25, perrunflas_budget=650 WHERE id=30;
UPDATE equipos SET top_budget = 865, perrunflas_budget=651.5 WHERE id=31;
UPDATE equipos SET top_budget = 853.25, perrunflas_budget=651 WHERE id=32;
UPDATE equipos SET top_budget = 873, perrunflas_budget=658 WHERE id=33;
UPDATE equipos SET top_budget = 883, perrunflas_budget=668 WHERE id=34;



--
-- SELECCIONAR LOS NOMBRES Y LAS CANTIDADES DE JUGADORES DE TODOS LOS EQUIPOS
--
SELECT count(je.player_id) as cant_jug, e.name 
	FROM equipos e, jugadores_equipos je 
	WHERE e.id = je.team_id 
	GROUP BY je.team_id;

--
-- SELECCIONAR LOS NOMBRES DE JUGADORES DEL ARSENAL.
--
SELECT j.name, e.name 
	FROM equipos e, jugadores j, jugadores_equipos je 
	WHERE e.name = "Arsenal" 
	AND e.id = je.team_id 
	AND j.id = je.player_id;


--
-- Contar todos los traspasos de todos los equipos en un período separados por top, perrun y desconocidos.
--
SELECT e.name AS Equipo, if(je.is_top is null, "Desconocido", if(je.is_top, "TOP", "Perrun")) AS TOP_PERRUN, count(*) total_traspasos
FROM intercambios i
INNER JOIN equipos e ON i.buyer_id = e.id
LEFT JOIN jugadores_equipos je on je.player_id = i.player_id
WHERE i.is_pending = false
AND i.sell_date BETWEEN '2020-10-01' AND '2020-11-01'
and i.buyer_id != 1
GROUP BY i.buyer_id, je.is_top
ORDER BY e.name asc, total_traspasos desc
LIMIT 100



--
-- Detectar los que tienen más de 5 traspasos perrun
--
SELECT count(*) demasiados_perrun, e.name
FROM intercambios i, equipos e, jugadores_equipos je
WHERE i.is_pending = false
AND i.buyer_id = e.id
AND i.player_id = je.player_id
AND je.is_top = false
GROUP BY (i.buyer_id)
HAVING demasiados_perrun > 5


--
-- Detectar los que tienen más de 5 traspasos top
--
SELECT count(*) demasiados_top, e.name
FROM intercambios i, equipos e, jugadores_equipos je
WHERE i.is_pending = false
AND i.buyer_id = e.id
AND i.player_id = je.player_id
AND je.is_top = true
GROUP BY (i.buyer_id)
HAVING demasiados_top > 5



---
--- Liberar jugadores de equipos por nombre.
---
DELETE je.* 
FROM jugadores_equipos je, equipos e
WHERE je.team_id = e.id
AND 
	(e.name = 'Arsenal'
	OR e.name = 'Pumas U.N.A.M'
	OR e.name = 'Besiktas J.K.'
	OR e.name = 'Millonarios'
	OR e.name = 'Sparta Praga'
	OR e.name = 'Barcelona'
)	

---
--- Borrar equipos por nombre.
---
DELETE 
FROM equipos
WHERE
	(name = 'Arsenal'
	OR name = 'Pumas U.N.A.M'
	OR name = 'Besiktas J.K.'
	OR name = 'Millonarios'
	OR name = 'Sparta Praga'
	OR name = 'Barcelona'
)



---
--- Borrar FK y Key.
---
ALTER TABLE equipos
DROP FOREIGN KEY equipos_ibfk_1,
DROP KEY owner;

---
--- Agregar FK con borrado y actualizado en cascada.
---
ALTER TABLE equipos
ADD FOREIGN KEY (owner) REFERENCES usuarios(name) 
ON UPDATE CASCADE 
ON DELETE CASCADE



ALTER TABLE jugadores_equipos
DROP FOREIGN KEY jugadores_equipos_ibfk_2;

ALTER TABLE jugadores_equipos
DROP KEY team_id;

ALTER TABLE jugadores_equipos
ADD FOREIGN KEY (team_id) REFERENCES equipos(id) 
ON UPDATE CASCADE 
ON DELETE CASCADE


ALTER TABLE intercambios
DROP FOREIGN KEY intercambios_ibfk_1,
DROP FOREIGN KEY intercambios_ibfk_2;

ALTER TABLE intercambios
DROP KEY seller_id,
DROP KEY buyer_id;

ALTER TABLE intercambios
ADD FOREIGN KEY (seller_id) REFERENCES equipos(id)
ON UPDATE CASCADE 
ON DELETE CASCADE;
ALTER TABLE intercambios
ADD FOREIGN KEY (buyer_id) REFERENCES equipos(id) 
ON UPDATE CASCADE 
ON DELETE CASCADE



---
--- REPONER UN ANÓNYMO (Ajustar usuario, equipo y jugadores).
---
INSERT INTO `usuarios` (`name`, `pass`, `is_admin`) VALUES
('Anonymo18', 'Anonymo18', 0);

INSERT INTO `equipos` (`id`, `name`, `top_budget`, `perrunflas_budget`, `owner`) VALUES
(999018, 'Anonymo18', 850.00, 650.00, 'Anonymo18');

INSERT INTO `jugadores_equipos` (`player_id`, `team_id`, `price_paid`, `is_top`) VALUES
(999342, 999018, 66, 0),
(999343, 999018, 66, 1),
(999344, 999018, 66, 1),
(999345, 999018, 66, 1),
(999346, 999018, 66, 1),
(999347, 999018, 66, 1),
(999348, 999018, 66, 1),
(999349, 999018, 66, 1),
(999350, 999018, 66, 1),
(999351, 999018, 66, 1),
(999352, 999018, 66, 1),
(999353, 999018, 66, 1),
(999354, 999018, 66, 0),
(999355, 999018, 66, 0),
(999356, 999018, 66, 0),
(999357, 999018, 66, 0),
(999358, 999018, 66, 0),
(999359, 999018, 66, 0),
(999360, 999018, 66, 0);


---
--- Traer los traspasos hechos y aplicados antes de un día determinado y después de otro.
---
SELECT i.*, e.name FROM intercambios i, equipos e
WHERE i.sell_date BETWEEN '2020-03-08' and '2020-03-27'
and (buyer_id = e.id
or seller_id = e.id)
and is_pending = 0
and e.name != 'Mercado Asiatico'

--- Para que sea sin considerar al birmin ni a chelsea ni al Inter ni a Anonymo6.
SELECT e.name, i.* FROM intercambios i, equipos e
WHERE i.sell_date BETWEEN '2020-03-08' and '2020-03-27'
and (buyer_id = e.id
or seller_id = e.id)
and is_pending = 0
and e.name != 'Mercado Asiatico'
and e.name not like 'Birmin%'
and e.name not like 'Chels%'
and e.name not like 'Inter%'
and e.name not like 'Roma%'