Drop TABLE IF EXISTS equipos_en_campeonatos_toornament;
Drop TABLE IF EXISTS campeonatos_toornament;

CREATE TABLE `campeonatos_toornament` (
 `id` BIGINT(11) NOT NULL,
 `nombre` varchar(60) DEFAULT NULL,
 `fecha_inicio` date DEFAULT NULL,
 `fecha_fin` date DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `equipos_en_campeonatos_toornament` (
 `id_dynasty_equipo` int(11) NOT NULL,
 `id_campeonato` BIGINT(11) NOT NULL,
 `id_toornament_equipo` BIGINT(11) DEFAULT NULL,
 PRIMARY KEY (`id_dynasty_equipo`,`id_campeonato`),
 KEY `id_campeonato` (`id_campeonato`),
 CONSTRAINT `equipos_en_campeonatos_toornament_ibfk_1` FOREIGN KEY (`id_dynasty_equipo`) REFERENCES `equipos` (`id`),
 CONSTRAINT `equipos_en_campeonatos_toornament_ibfk_2` FOREIGN KEY (`id_campeonato`) REFERENCES `campeonatos_toornament` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO campeonatos_toornament VALUES (3452215170398781440, "DYT T8", '2020-04-01', '2020-08-01');
INSERT INTO equipos_en_campeonatos_toornament VALUES(28, 3452215170398781440, 3452247846597689384);
INSERT INTO equipos_en_campeonatos_toornament VALUES(34, 3452215170398781440, 3452247846597689376);
INSERT INTO equipos_en_campeonatos_toornament VALUES(33, 3452215170398781440, 3452247846597689344);
INSERT INTO equipos_en_campeonatos_toornament VALUES(14, 3452215170398781440, 3452247846597689348);
INSERT INTO equipos_en_campeonatos_toornament VALUES(27, 3452215170398781440, 3452247846597689352);
INSERT INTO equipos_en_campeonatos_toornament VALUES(22, 3452215170398781440, 3452247846597689356);
INSERT INTO equipos_en_campeonatos_toornament VALUES(29, 3452215170398781440, 3452247846597689360);
INSERT INTO equipos_en_campeonatos_toornament VALUES(31, 3452215170398781440, 3452247846597689364);
INSERT INTO equipos_en_campeonatos_toornament VALUES(12, 3452215170398781440, 3452247846597689368);
INSERT INTO equipos_en_campeonatos_toornament VALUES(26, 3452215170398781440, 3452247846597689372);
INSERT INTO equipos_en_campeonatos_toornament VALUES(24, 3452215170398781440, 3452247846597689380);
INSERT INTO equipos_en_campeonatos_toornament VALUES(18, 3452215170398781440, 3452247846597689388);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999001, 3452215170398781440, 3452247846597689400);
INSERT INTO equipos_en_campeonatos_toornament VALUES(17, 3452215170398781440, 3452247846597689404);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999005, 3452215170398781440, 3452247846597689408);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999004, 3452215170398781440, 3452247846597689416);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999003, 3452215170398781440, 3452247846597689420);
INSERT INTO equipos_en_campeonatos_toornament VALUES(9, 3452215170398781440, 3452247846597689424);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999007, 3452215170398781440, 3457591764361125888);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999008, 3452215170398781440, 3457592234715037696);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999012, 3452215170398781440, 3801208006027280384);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999011, 3452215170398781440, 3801209779873882112);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999009, 3452215170398781440, 3801211726146732032);
INSERT INTO equipos_en_campeonatos_toornament VALUES(16, 3452215170398781440, 3801286053032787968);
INSERT INTO equipos_en_campeonatos_toornament VALUES(999014, 3452215170398781440, 3801287405146963968);


CREATE TABLE `log_ingresos_de_resultados` (
 `id_equipo` int NOT NULL,
 `id_partido` BIGINT(11) NOT NULL,
 `res_local` INT NOT NULL,
 `res_visitante` INT NOT NULL,
 `fecha` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`fecha`),
 FOREIGN KEY (`id_equipo`) REFERENCES `equipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;