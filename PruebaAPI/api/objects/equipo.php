<?php
class Equipo{
 
    // database connection and table name
    private $conn;
    private $table_name = "equipos";
    
    // object properties
    public $id;
    public $name;
    public $top_budget;
    public $perrunflas_budget;
    public $owner;
    public $sumaRealTops;
    public $sumaRealPerrunflas;
    public $fylyal_id;
    public $fylyal_name;
    public $logo;
    public $sofifa_team_uri;
    public $canal_youtube;
    public $usuario_psn;
        
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    function read_one()
    {
        $query = "
            SELECT e.*,
                fy.id as fylyal_id,
                fy.name as fylyal_name,
                u.canal_youtube,
                u.usuario_psn
            from equipos e
            LEFT JOIN equipos_y_fylyales ef on e.id = ef.id_equipo
            LEFT JOIN equipos fy on ef.id_fylyal = fy.id
            LEFT JOIN usuarios u ON e.owner = u.name
            WHERE e.id =:id";

        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        
        // error_log("Cargando equipo");
        // error_log($this->id);

        if($stmt->execute()){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->name = $row["name"];
            $this->top_budget = $row["top_budget"];
            $this->perrunflas_budget = $row["perrunflas_budget"];
            $this->owner = $row["owner"];
            $this->sumaRealTops = $this->get_suma_real_jugadores($this->id, 1);
            $this->sumaRealPerrunflas = $this->get_suma_real_jugadores($this->id, 0);
            $this->fylyal_id = $row["fylyal_id"];
            $this->fylyal_name = $row["fylyal_name"];
            $this->logo = $row["logo"];
            $this->sofifa_team_uri = $row["sofifa_team_uri"];
            $this->canal_youtube = $row["canal_youtube"];
            $this->usuario_psn = $row["usuario_psn"];

            return true;
        }
     
        return false;
    }

    // update the Equipo
    function update($nuevoNombre, $nuevaUriSofifa, $nuevaUrlLogo)
    {
        // error_log("Actualizando equipo id " . $this->id . " a nombre: " . $nuevoNombre);
        // update query
        $query = "
            UPDATE equipos
            SET name = :new_name,
                sofifa_team_uri = :sofifa_team_uri,
                logo = :logo
            WHERE id = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $nuevoNombre=htmlspecialchars(strip_tags($nuevoNombre));
        $nuevaUriSofifa=htmlspecialchars(strip_tags($nuevaUriSofifa));
        $nuevaUrlLogo=htmlspecialchars(strip_tags($nuevaUrlLogo));
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind new values
        $stmt->bindParam(':new_name', $nuevoNombre);
        $stmt->bindParam(':sofifa_team_uri', $nuevaUriSofifa);
        $stmt->bindParam(':logo', $nuevaUrlLogo);
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            $this->name = $nuevoNombre;
            $this->sofifa_team_uri = $nuevaUriSofifa;
            $this->logo = $nuevaUrlLogo;
            return true;
        }
     
        return false;
    }

    function update_budget($newBudget, $is_top)
    {
        // error_log("equipo->update_budget");
        // error_log("$this->name");
        // error_log("newBudget: $newBudget");
        $budget_field = ($is_top == 1)?"top_budget":"perrunflas_budget";
        // query to insert record
        $query = "UPDATE " . $this->table_name . "
            SET $budget_field=:newBudget
            WHERE id=:id";
                    
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $newBudget=htmlspecialchars(strip_tags($newBudget));
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":newBudget", $newBudget);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function update_budgets(){
        
        $query = "UPDATE `equipos` SET `top_budget`=:top_budget, `perrunflas_budget`=:perrunflas_budget  WHERE `id`=:id";

        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        //$newBudget=htmlspecialchars(strip_tags($newBudget));
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->top_budget=htmlspecialchars(strip_tags($this->top_budget));
        $this->perrunflas_budget=htmlspecialchars(strip_tags($this->perrunflas_budget));
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":top_budget", $this->top_budget);
        $stmt->bindParam(":perrunflas_budget", $this->perrunflas_budget);
        
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;

    }

    function read()
    {
     
        // select all query
        $query = "SELECT * FROM
                    " . $this->table_name . " j
                    WHERE owner != 'admin'";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function read_team_players()
    {
     
        // select all query
        $query = "SELECT p.*, je.team_id, e.name as team_name, je.is_top , je.price_paid FROM jugadores p, jugadores_equipos je, equipos e 
                WHERE e.id = je.team_id
                AND p.id = je.player_id
                AND e.id = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
    function getMaxPerrunfla()
    {
        // select all query
        $query = "SELECT max(je.price_paid) as maxPerrunfla
            FROM jugadores_equipos je
            WHERE je.team_id = :id
            AND je.is_top = 0;";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        if($stmt->execute())
        {
            $num = $stmt->rowCount();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row['maxPerrunfla'];
        }
     
        return 0;
    }

    function getMinTop()
    {
        // select all query
        $query = "SELECT min(je.price_paid) as minTop
            FROM jugadores_equipos je
            WHERE je.team_id = :id
            AND je.is_top = 1;";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        if($stmt->execute())
        {
            $num = $stmt->rowCount();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row['minTop'];
        }
     
        return 0;
    }

    function get_suma_jugadores($is_top)
    {
        // select all query
        $query = "SELECT SUM(je.price_paid) as suma FROM jugadores_equipos je WHERE je.team_id =:team_id AND je.is_top =:is_top";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        $is_top=htmlspecialchars(strip_tags($is_top));
        
        // bind values
        $stmt->bindParam(":team_id", $this->id);
        $stmt->bindParam(":is_top", $is_top);
        
        // execute query
        if($stmt->execute())
        {
            $num = $stmt->rowCount();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row['suma'];
        }
     
        return 0;
    }

    function get_suma_real_jugadores($team_id, $is_top)
    {

        // select all query
        $query = "
            SELECT SUM(j.overall_rating) as suma 
            FROM jugadores_equipos je 
            inner join jugadores j on j.id = je.player_id
            WHERE je.team_id =:team_id AND je.is_top =:is_top"
        ;
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // sanitize
        $team_id=htmlspecialchars(strip_tags($team_id));
        $is_top=htmlspecialchars(strip_tags($is_top));
        
        // bind values
        $stmt->bindParam(":team_id", $team_id);
        $stmt->bindParam(":is_top", $is_top);
        
        // execute query
        if($stmt->execute())
        {
            $num = $stmt->rowCount();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row['suma'];
        }
     
        return 0;
    }

    // create Usuario
    function create(){
     
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . "
                (`name`, `top_budget`, `perrunflas_budget`, `owner`)
                VALUES
                (:name, :top_budget, :perrunflas_budget, :owner)";
                    
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->top_budget=htmlspecialchars(strip_tags($this->top_budget));
        $this->perrunflas_budget=htmlspecialchars(strip_tags($this->perrunflas_budget));
        $this->owner=htmlspecialchars(strip_tags($this->owner));
        
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":top_budget", $this->top_budget);
        $stmt->bindParam(":perrunflas_budget", $this->perrunflas_budget);
        $stmt->bindParam(":owner", $this->owner);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }

    function read_owner()
    {
     
        // select all query
        $query = "SELECT owner FROM equipos where id =:id;";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row['owner'];
    }

    function read_from_owner($owner)
    {
        $query = "
            SELECT e.*,
                fy.id as fylyal_id,
                fy.name as fylyal_name
            from equipos e
            LEFT JOIN equipos_y_fylyales ef on e.id = ef.id_equipo
            LEFT JOIN equipos fy on ef.id_fylyal = fy.id
            WHERE e.owner =:owner";

        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $owner=htmlspecialchars(strip_tags($owner));
        
        // bind values
        $stmt->bindParam(":owner", $owner);
        
        // execute query
        
        // error_log("Cargando equipo");
        // error_log($owner);

        if($stmt->execute()){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row["id"];
            $this->name = $row["name"];
            $this->top_budget = $row["top_budget"];
            $this->perrunflas_budget = $row["perrunflas_budget"];
            $this->owner = $row["owner"];
            $this->sumaRealTops = $this->get_suma_real_jugadores($this->id, 1);
            $this->sumaRealPerrunflas = $this->get_suma_real_jugadores($this->id, 0);
            $this->fylyal_id = $row["fylyal_id"];
            $this->fylyal_name = $row["fylyal_name"];
            return true;
        }
     
        return false;
    }
}
?>