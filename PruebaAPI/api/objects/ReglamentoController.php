<?php
  include_once '../config/database.php';
  include_once '../datatypes/DtSeccion.php';

  class ReglamentoController
  {
   
    private $conn;
    static private $instance /*ReglamentoController*/;
    
    // constructor with $db as database connection
    private function __construct()
    {
        // instantiate database and equipo object
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    public static function getInstance()
    {
        if(!isset($instance))
        {
            $instance = new ReglamentoController();
        }
        return $instance;
    }

    public function actualizarSeccion($indice /*String*/, $titulo /*String*/, $contenido /*String*/) /*: Boolean*/
    {
      $query = "
          UPDATE secciones_reglamento
          SET titulo = '{$titulo}', 
            contenido = '{$contenido}'
          WHERE indice = '{$indice}';
      ";
      
      // prepare query statement
      $stmt = $this->conn->prepare($query);
      
      // execute query
      return $stmt->execute();
    }

    public function ingresarNuevaSeccion($indice /*String*/, $titulo /*String*/, $contenido /*String*/) /*: Boolean*/
    {
      $query = "
          INSERT INTO secciones_reglamento (indice, titulo, contenido)
          VALUES('{$indice}', '{$titulo}', '{$contenido}');
      ";
      // echo "query: " . $query . "\n";

      // prepare query statement
      $stmt = $this->conn->prepare($query);
      
      // execute query
      return $stmt->execute();
    }

    public function listarTodasLasSecciones() /*Array<DtSeccion>*/
    {
      $query = "
          SELECT * FROM secciones_reglamento;
      ";
      
      // prepare query statement
      $stmt = $this->conn->prepare($query);
      
      // execute query
      if(!$stmt->execute())
      {
        return false;
      }
      else
      {
        $secciones = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
          $seccion = new DtSeccion($row['indice'], $row['titulo'], $row['contenido']);
          array_push($secciones, $seccion);
        }

        return $secciones;
      }
    }
  }
?>