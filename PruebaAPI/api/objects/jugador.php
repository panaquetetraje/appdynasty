<?php
include_once 'equipo.php';
class Jugador
{
 
    // database connection and table name
    private $conn;
    private $table_name = "jugadores";
    private $jugadores_equipos_table_name = "jugadores_equipos";
 
    // object properties
    public $id;
    public $name;
    //public $valor_de_compra;
    public $overall_rating;
    public $team_id;
    public $team_name;
    public $price_paid;
    public $is_top;
    public $esSuperPerrunfla;
    public $real_sell_price;
    public $team_top_budget;
    public $team_perrunflas_budget;
        
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read Jugadores
    function read()
    {
        // select all query
        $query = "
            SELECT j.id, j.name, j.overall_rating, je.team_id, je.is_top, e.name as team_name, je.price_paid
            FROM jugadores j left join jugadores_equipos je on j.id = je.player_id left join equipos e on je.team_id = e.id";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
        
        return $stmt;
    }

    // read Jugadores
    function read_one()
    {
        // select all query
        $query = "
            SELECT j.name, j.overall_rating, je.team_id, je.is_top, e.name as team_name, e.top_budget, e.perrunflas_budget, je.price_paid FROM jugadores j left join jugadores_equipos je on j.id = je.player_id left join equipos e on je.team_id = e.id WHERE j.id =:id;";

        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        $stmt->execute();
     
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        


        //error_log("LLEGA, es top " . $row['name'] . " id: " . $this->id . " en el equipo: " . $row['team_name']);
        //error_log(($row['is_top'] == 1)?1:0);
        // set values to object properties
        $this->name = $row['name'];
        $this->overall_rating = max(Database::VALOR_MÍNIMO_TRASPASOS, $row['overall_rating']);
        $this->team_id = $row['team_id'];
        $this->team_name = $row['team_name'];
        $this->team_top_budget = $row['top_budget'];
        $this->team_perrunflas_budget = $row['perrunflas_budget'];
        $this->price_paid = $row['price_paid'];
        $this->is_top = ($row['is_top'] == 1)?1:0;
        $this->esSuperPerrunfla = $this->obtenerEsSuperPerrun();
        $this->real_sell_price = $this->calcularValorDeVenta();
        // error_log("Jugador::Read One");
        // error_log($this->id);
        // error_log($this->name);
        // error_log($this->overall_rating);
        if($this->team_name == "")$this->team_name = Database::NOMBRE_MERCADO_ASIATICO;
    }
    
    private function obtenerEsSuperPerrun()
    {
        $equipoOwner = new Equipo($this->conn);
        $equipoOwner->id = $this->team_id;
        $equipoOwner->read_one();
        $minTopPricePaid = $equipoOwner->getMinTop();
        return $this->team_id!=NULL && !$this->is_top && $this->overall_rating > $minTopPricePaid + 2;
    }

    public function calcularValorDeVenta()
    {
        /**
        * Elimino el max para que topee en crecimiento y decrecimiento.

        * Caso en que un top decrece 3, de 75 a 72 y está topeado:
        * $crecimiento = 72 - 75 = -3.
        * $faltanteParaTopear = 865 - 1 - 878 = 865 - 879 = -14;
        * if(($crecimiento = -3 > $faltanteParaTopear = -14) = true)
        * Entonces pasa: $gananciaJT = max($faltanteParaTopear, 0) + $crecimiento / abs($crecimiento) = 0 + (-3) / 3 = 0 + 1 = 1

        * Caso en que un top decrece 5, de 75 a 70 y sale del tope:
        * $crecimiento = 70 - 75 = -5.
        * $faltanteParaTopear = 865 - 1 - 866 = 865 - 867 = -2;
        * if(($crecimiento = -5 > $faltanteParaTopear = -2) = false)
        * Entonces pasa: $gananciaJT = $crecimiento = -5

        * Caso en que un top crece 3, de 72 a 75 y queda topeado:
        * $crecimiento = 75 - 72 = 3.
        * $faltanteParaTopear = 865 - 1 - 863 = 865 - 864 = 1;
        * if(($crecimiento = 3 > $faltanteParaTopear = 1) = true)
        * Entonces pasa: $gananciaJT = max($faltanteParaTopear = 1, 0) + $crecimiento / abs($crecimiento) = 1 + 3 / 3 = 1 + 1 = 2
        */
        // $crecimiento = $this->overall_rating - $this->price_paid; // $crecimiento o de$crecimiento
        // $presupuesto = ($this->is_top == 1)?$this->team_top_budget:$this->team_perrunflas_budget;
        // $tope = ($this->is_top == 1)?Database::TOPE_TOP:Database::TOPE_PERRUNFLAS;
        

        // $faltanteParaTopear = $tope - 1 - $presupuesto;
        
        /*if ($crecimiento > $faltanteParaTopear && $crecimiento != 0) // Me paso del tope.
        {    
            $gananciaJT = (max($faltanteParaTopear, 0) + $crecimiento / abs($crecimiento));
        }
        else
        {*/    
            // $gananciaJT = $crecimiento; // Crecen y decrecen libremente.
        //}
        // return $this->price_paid + $gananciaJT; // el valor al que realmente voy a vender al jugador
        return $this->overall_rating; // Con el sacado del tope se devuelve directamente el overall.
    }

    // create Jugador
    function create()
    {
     
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    id=:id, name=:name, overall_rating=:overall_rating";
                    //id=:id, name=:name, overall_rating=:overall_rating, valor_de_compra=:valor_de_compra";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->overall_rating=htmlspecialchars(strip_tags($this->overall_rating));
        //$this->valor_de_compra=htmlspecialchars(strip_tags($this->valor_de_compra));
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":overall_rating", $this->overall_rating);
        //$stmt->bindParam(":valor_de_compra", $this->valor_de_compra);
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }


    
    function add_to_team()
    {
     
        $this->remove_from_team();
        // query to insert record

        $query = "INSERT INTO " . $this->jugadores_equipos_table_name . "
                SET
                    player_id=:player_id, team_id=:team_id, price_paid=:price_paid, is_top=:is_top;";
                    
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->team_id=htmlspecialchars(strip_tags($this->team_id));
        $this->price_paid=htmlspecialchars(strip_tags($this->price_paid));
        $this->is_top=$this->is_top==1?1:0;
        /*
        error_log("Insertando is top");
        error_log($this->is_top);
        */
        // bind values
        $stmt->bindParam(":player_id", $this->id);
        $stmt->bindParam(":team_id", $this->team_id);
        $stmt->bindParam(":price_paid", $this->price_paid);
        $stmt->bindParam(":is_top", $this->is_top);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }


    function remove_from_team()
    {
     
        $query = "DELETE FROM " . $this->jugadores_equipos_table_name . "
                WHERE player_id=:player_id;";
                    
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":player_id", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }


    // updates the Jugador
    function update()
    {
     
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    overall_rating = :overall_rating
                WHERE
                    id = :id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->overall_rating=htmlspecialchars(strip_tags($this->overall_rating));
     
        // bind new values
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':overall_rating', $this->overall_rating);
        // error_log("Jugador::Update");
        // error_log($this->id);
        // error_log($this->name);
        // error_log($this->overall_rating);
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    // search Jugadores
    function search($keywords)
    {
        // select all query
        $query = "SELECT j.name, j.id as player_id, j.overall_rating, je.team_id, je.is_top, e.name as team_name, je.price_paid 
            FROM jugadores j left join jugadores_equipos je on j.id = je.player_id left join equipos e on je.team_id = e.id 
            WHERE j.name LIKE ? OR j.id LIKE ?
            ORDER BY j.overall_rating DESC";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
     
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    // search Jugadores
    function listar_jugadores_libres($min, $max)
    {
        // select all query
        $query = "SELECT j.name, j.id as player_id, j.overall_rating, je.team_id, je.is_top, e.name as team_name, je.price_paid 
            FROM jugadores j left join jugadores_equipos je on j.id = je.player_id left join equipos e on je.team_id = e.id 
            WHERE je.team_id is null /* Pido los libres */
                AND j.overall_rating BETWEEN {$min} AND {$max}
            ORDER BY j.overall_rating DESC";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
}
?>