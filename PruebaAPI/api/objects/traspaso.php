<?php

class Traspaso{
 
    // database connection and table buyer_id
    private $conn;
    private $table_name = "intercambios";
    // object properties
    public $buyer_accepted;
    public $seller_accepted;
    public $seller_id;
    public $buyer_id;
    public $player_id;
    public $sell_date;
    public $sell_price;
    public $is_pending;
    public $is_super_perrun;
    public $is_pelo_a_pelo;
    public $id;
    public $engaged_to;
    public $added_top_points;
    public $added_perrunflas_points;
        
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function has_pending_action_for_team($cadena_id, $team_id)
    {
        $query = "SELECT * FROM `intercambios`
            WHERE (buyer_id =:buyer_id
            OR seller_id =:seller_id)
            AND engaged_to =:cadena_id;";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $cadena_id=htmlspecialchars(strip_tags($cadena_id));
        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":cadena_id", $cadena_id);
        $stmt->bindParam(":seller_id", $team_id);
        $stmt->bindParam(":buyer_id", $team_id);
        
        // execute query
        $stmt->execute();
        $cantidadDeTraspasos = $stmt->rowCount();
        //error_log("Consulta hecha, cant filas");
        //error_log($stmt->rowCount());
        
        $query = "SELECT * FROM `intercambios`
            WHERE buyer_id =:buyer_id
            AND seller_id = buyer_id
            AND engaged_to =:cadena_id;";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $cadena_id=htmlspecialchars(strip_tags($cadena_id));
        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":cadena_id", $cadena_id);
        $stmt->bindParam(":buyer_id", $team_id);
        
        // execute query
        $stmt->execute();
        $isUpdate = ($stmt->rowCount() != 0);
        

        /**
        * Si no tiene 2 filas es porque necesita tomar acción para que haya
        * una venta y una compra en la cadena. Salvo que sea una actualización
        * que en ese caso se repetirá el buyer y el seller.
        */
        return $cantidadDeTraspasos != 2 && !$isUpdate;
    }


    function realizar_traspaso_pelo_a_pelo($jugadorAComprar, $jugadorAVender)
    {
        // error_log(">>realizar_traspaso_pelo_a_pelo<<");
        $idJugAComprar = $jugadorAComprar->id;
        $equipoJugAComprar = $jugadorAComprar->team_id;
        $idJugAVender = $jugadorAVender->id;
        $equipoJugAVender = $jugadorAVender->team_id;

        // error_log("idJugAComprar: {$idJugAComprar}");
        // error_log("equipoJugAComprar: {$equipoJugAComprar}");
        // error_log("idJugAVender: {$idJugAVender}");
        // error_log("equipoJugAVender: {$equipoJugAVender}");

        /**
        * Meto el vendido en lugar del comprado.
        */
        $query = "
            UPDATE jugadores_equipos je
            SET player_id =?
            WHERE team_id =?
                AND player_id =?
        ";
                    
        // prepare query
        $anduvoPrimera = $this->conn->prepare($query)->execute([$idJugAVender, $equipoJugAComprar, $idJugAComprar]);

        /**
        * Meto el comprado en lugar del vendido.
        */
        $query = "
            UPDATE jugadores_equipos je
            SET player_id =?
            WHERE team_id =?
                AND player_id =?
        ";
                    
        // prepare query
        $anduvoSegunda = $this->conn->prepare($query)->execute([$idJugAComprar, $equipoJugAVender, $idJugAVender]);

        return $anduvoPrimera && $anduvoSegunda;
    }


    /**
    * No sirve para realizar pelo a pelos, para eso usar realizar_traspaso_pelo_a_pelo
    */
    function realizar_traspaso($jugadorAComprar, $jugadorAVender, $team_id, $team_to_sell_to, $esTraspasoDeUnTop)
    {
        
        // error_log("realizar_traspaso, jugadorAVender->is_top: ");
        // error_log($jugadorAVender->is_top);
        // error_log("jugadorAVender: $jugadorAVender->name");
        // error_log("ex team:");
        // error_log($jugadorAComprar->team_id);

        
        $jugadorAComprar->team_id = $team_id;
        $jugadorAComprar->is_top = $esTraspasoDeUnTop;
        $jugadorAComprar->price_paid = $jugadorAComprar->overall_rating;
        
        $compraRealizada = $jugadorAComprar->add_to_team();

        $ventaResuelta = true;
        if($team_to_sell_to == Database::ID_MERCADO_ASIATICO)
        {
            $ventaResuelta = $jugadorAVender->remove_from_team();
        }
        /**
        * No estoy haciendo nada en caso contrario porque asumo que luego se realizará cuando sea considerado jugador a comprar por el equipo que realiza la compra.
        * Sólo atendí el caso en que se venda al mercado asiático porque no habrá tal cosa en ese caso.
        */
        /*else
        {

            
            $jugadorAVender->is_top = $is_top;
            $jugadorAVender->price_paid = $jugadorAComprar->overall_rating;
            $jugadorAVender->team_id = $team_id;
            $jugadorAVender->add_to_team();
            
        }
        */

        if($compraRealizada && $ventaResuelta){
            return true;
        }
        return false;
    }


    function sePasaDeFrontera($equipo, $player)
    {
        $pasaFrontera=false;
        if($player->is_top)
        {
            $query = "SELECT MAX(price_paid) FROM jugadores_equipos WHERE team_id=:equipo AND !is_top";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":equipo", $equipo->id);
            $exec = $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if($player->overall_rating<$row['MAX(price_paid)']){
                $pasaFrontera= true;
            }
        }
        else
        {
            $query = "SELECT MIN(price_paid) FROM jugadores_equipos WHERE team_id=:equipo AND is_top";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":equipo", $equipo->id);
            $exec = $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if($player->overall_rating>$row['MIN(price_paid)']){
                $pasaFrontera= true;
            }
        }
        return $pasaFrontera;
    }

    function apply_update($data)
    {
        $this->conn->beginTransaction();

        $stmt = $this->read_cadena_for_team($data->update_id, $data->team_id);

        $traspaso = $stmt->fetch(PDO::FETCH_ASSOC);

        $equipo = new Equipo($this->conn);
        $equipo->id = $traspaso['seller_id'];
        $equipo->read_one();


        $player = new Jugador($this->conn);
        $player->id = $traspaso["player_id"];
        $player->read_one();

        $pasaFrontera = $this->sePasaDeFrontera($equipo, $player);
        
        
        $query = "UPDATE intercambios
            SET is_pending = 0
            WHERE engaged_to =:engaged_to;";

        $stmt = $this->conn->prepare($query);
     
        // bind values
        $stmt->bindParam(":engaged_to", $traspaso["engaged_to"]);

        $updateRealizado = $stmt->execute();




        /*
        error_log("player->real_sell_price");
        error_log($player->real_sell_price);
        error_log("player->price_paid");
        error_log($player->price_paid);
        */

        $ganancia = $player->real_sell_price - $player->price_paid;
        $costoExtra = $player->overall_rating - $player->real_sell_price;
        
        if($player->is_top)
        {
            $viejoPresupuesto = $equipo->top_budget;
        }
        else
        {
            $viejoPresupuesto = $equipo->perrunflas_budget;
        }
        $nuevoPresupuesto = $viejoPresupuesto + $ganancia;

        $player->price_paid = $player->overall_rating;

        $jugadorEnEquipoActualizado = $player->add_to_team(0); // lo remueve adentro y lo vuelve a agregar.

        $suma_jugadores = $equipo->get_suma_jugadores($player->is_top);
        
        /*
        error_log("viejo presupuesto $viejoPresupuesto");
        error_log("nuevo presupuesto $nuevoPresupuesto");
        error_log("ganancia $ganancia");
        //error_log("costoExtra $costoExtra");
        error_log("suma_jugadores $suma_jugadores");
        error_log("player->is_top $player->is_top");
        */

        if($nuevoPresupuesto >= $suma_jugadores/*+$costoExtra*/)
            $presupuestoActualizado = $equipo->update_budget($nuevoPresupuesto, $player->is_top);
        else
            $presupuestoActualizado = false;


        $periodo = new Periodo($this->conn);
        // error_log("periodo->getActualmenteEnPeriodo()");
        // error_log($periodo->getActualmenteEnPeriodo());
        
        $message = "";

        if($jugadorEnEquipoActualizado && $updateRealizado && $presupuestoActualizado && $periodo->getActualmenteEnPeriodo() && !$pasaFrontera)
        {
            $this->conn->commit();
            //error_log("creando message");
            $message .= "Actualización concretada, ganó $ganancia puntos ";
            $message .= ($player->is_top)?"de top":"de perrunflas";
            $message .= ($costoExtra > 0)?" y gastó $costoExtra arriba.":" y no gastó puntos extra para actualizar. ";
            //error_log($message);

            return array("success" => true, "message" => $message);
        }
        else
        {
            $this->conn->rollback();
            
            if(!$jugadorEnEquipoActualizado)
                $message .= "Error: No se pudo actualizar el jugador en el equipo. ";
            if(!$updateRealizado)
                $message .= "Error: No se pudo efectuar la actualización. ";
            if(!$presupuestoActualizado)
            {
                $message .= "Error: Problemas con el presupuesto.";
                $message .= "El nuevo presupuesto quedaría: $nuevoPresupuesto que es menor que la suma de jugadores, que quedaría: $suma_jugadores. Se cancela la actualización y no se podrá hacer hasta que disponga de un resto de $costoExtra. ";
            }
            if(!$periodo->getActualmenteEnPeriodo())
            {
                $message = "Se podría realizar el traspaso pero no está en período de traspasos, el próximo período comenzará el: " . $periodo->getProximoInicio() . " ";
            }
            if($pasaFrontera)
            {
                $message = "La actualizacion no verifica la regla de frontera";
            }

            return array("success" => false, "message" => $message);
        }
    }

    function apply_chain_transfers($resultadoBloque)
    {
        $this->conn->beginTransaction();

        $periodo = new Periodo($this->conn);
        // error_log("periodo->getActualmenteEnPeriodo()");
        // error_log($periodo->getActualmenteEnPeriodo());

        $bloqueValido = true;
        $cadena_id = $resultadoBloque["resultados"][0]["traspasos"][0]["cadenaId"];
        for ($i=0; $i < sizeof($resultadoBloque["resultados"]); $i++)
        {
            $traspaso = $resultadoBloque["resultados"][$i]["traspasos"][0];
            // error_log("---NUEVO TRASPASO---");
            // error_log(json_encode($traspaso));
            
            $team_id = $traspaso['team_id'];

            $jugadorAComprar = new Jugador($this->conn);
            $jugadorAComprar->id = $traspaso["id_jugador_a_comprar"];
            $jugadorAComprar->read_one();


            $jugadorAVender = new Jugador($this->conn);
            $jugadorAVender->id = $traspaso["id_jugador_a_vender"];
            $jugadorAVender->read_one();

            
            // error_log("---");
            // error_log("Comprando a:");
            // error_log($jugadorAComprar->name);
            // error_log($jugadorAComprar->is_top);
            // error_log("Vendiendo a:");
            // error_log($jugadorAVender->name);
            // error_log($jugadorAVender->is_top);
            // error_log("--- vuelta: {$i}");

            /**
            * Actualizo la tabla jugadores_equipos efectivizando el traspaso.
            */
            if(!$traspaso["isPeloPelo"])
            {
                $traspasoRealizado = $this->realizar_traspaso($jugadorAComprar, $jugadorAVender, $team_id, $traspaso['team_to_sell_to'], $traspaso["is_top"]);
            }
            else if($i == 0) // Cuando es pelo a pelo, aplico el primero de los intercambios e ignoro el segundo.
            {
                $traspasoRealizado = $this->realizar_traspaso_pelo_a_pelo($jugadorAComprar, $jugadorAVender);
            }
            else
            {
                $traspasoRealizado = true;
            }


            


            
            if(!$traspaso["isPeloPelo"]) // Si es pelo a pelo no se actualiza el presupuesto.
            {
                $equipo = new Equipo($this->conn);
                $equipo->id = $traspaso['team_id'];
                $equipo->read_one();

                $presupuestoActualizado = $equipo->update_budget($traspaso['presupuesto'], $traspaso['is_top']);
                $presupuestoOtraCategoriaActualizado = $equipo->update_budget($traspaso['presupuestoOtraCategoria'], !$traspaso['is_top']);
            }
            else
            {
                $presupuestoActualizado = true;
                $presupuestoOtraCategoriaActualizado = true;
            }

            // $equipo->read_one(); // Actualizo el equipo para que se registren los nuevos datos de presupuestos y sus consecuencias.
            // if($jugadorAComprar->esSuperPerrunfla && isset($traspaso['isSuperPerrun']) && $traspaso['isSuperPerrun'] == 1 )
            // {
            //     $presupuestoOtraCategoriaActualizado = $equipo->update_budget($equipo->top_budget-2, 1);
            // }
            // else
            // {
            //     error_log("ELSE, no es super perrunfla el que compra");
            //     error_log("jugadorAComprar->esSuperPerrunfla: $jugadorAComprar->esSuperPerrunfla");
            //     if(isset($traspaso['isSuperPerrun']))
            //     {
            //         error_log("isset(traspaso[isSuperPerrun])");
            //         error_log("is_super_perrun: {$traspaso['isSuperPerrun']}");
            //     }
            //     else
            //     {
            //         error_log("NO isset(traspaso[isSuperPerrun])");
            //     }

            // }

            // /**
            // * Si estoy vendiendo a un superPerrunfla reduzco el presupuesto del comprador de dicho superPerrunfla en 2.
            // */
            // if(isset($traspaso['is_super_perrun']) && $traspaso['is_super_perrun'] == 1 && $jugadorAVender->esSuperPerrunfla)
            // {
            //     $equipoCompradorDelSuperPerrun = new Equipo($this->conn);
            //     $equipoCompradorDelSuperPerrun->id = $traspaso['idCompradorDeSuperPerrun'];
            //     $presupuestoActualizado = $presupuestoActualizado && $equipo->update_budget($equipo->top_budget - 2 , 1);
            //     error_log("Actualizado el presupuesto del comprador del super perrun");
            // }
            

            $bloqueValido = $bloqueValido 
                && $traspasoRealizado 
                && $presupuestoActualizado
                && $presupuestoOtraCategoriaActualizado
                && $periodo->getActualmenteEnPeriodo()
            ;
        }
        // error_log("Salió del for, cadena {$cadena_id}, ¿bloque válido? " . ($bloqueValido?"Sí":"No"));
        /**
        * Actualizo la tabla intercambios para que el traspaso no esté más pendiente.
        */
        $query = "UPDATE intercambios
            SET is_pending = 0
            WHERE engaged_to =:cadena_id;";

        $stmt = $this->conn->prepare($query);
     
        // bind values
        $stmt->bindParam(":cadena_id", $cadena_id);

        $ventaActualizada = $stmt->execute();

        $bloqueValido = $bloqueValido 
                && $ventaActualizada
        ; 
        
        $resultadoBloque['bloqueValido'] = $bloqueValido;
        if($bloqueValido)
        {
            // error_log("Commiteando");
            $this->conn->commit();
            return true;
        }
        else
        {
            $this->conn->rollback();
            return false;
        }
    }



    function apply_block_transfers($resultadoBloque)
    {
        $this->conn->beginTransaction();

        $periodo = new Periodo($this->conn);
        // error_log("periodo->getActualmenteEnPeriodo()");
        // error_log($periodo->getActualmenteEnPeriodo());

        $bloqueValido = true;
        for ($i=0; $i < sizeof($resultadoBloque["traspasos"]); $i++)
        {
            $traspaso = $resultadoBloque["traspasos"][$i];
            $team_id = $traspaso['team_id'];

            $jugadorAComprar = new Jugador($this->conn);
            $jugadorAComprar->id = $traspaso["id_jugador_a_comprar"];
            $jugadorAComprar->read_one();


            $jugadorAVender = new Jugador($this->conn);
            $jugadorAVender->id = $traspaso["id_jugador_a_vender"];
            $jugadorAVender->read_one();

            
            /**
            * Actualizo la tabla jugadores_equipos efectivizando el traspaso.
            */
            $traspasoRealizado = $this->realizar_traspaso($jugadorAComprar, $jugadorAVender, $team_id, $traspaso['team_to_sell_to'], $traspaso["is_top"]);



            /**
            * Actualizo la tabla intercambios para que el traspaso no esté más pendiente.
            */
            $query = "UPDATE intercambios
                SET is_pending = 0
                WHERE engaged_to =:engaged_to
                AND (seller_id =:team_id";
            if($traspaso['team_to_sell_to'] == Database::ID_MERCADO_ASIATICO)
                $query .= " OR buyer_id =:team_id);";
            else
                $query .= ");";
            $stmt = $this->conn->prepare($query);
         
            // sanitize
            $cadena_id=htmlspecialchars(strip_tags($traspaso['cadenaId']));
            
            // bind values
            $stmt->bindParam(":engaged_to", $cadena_id);
            if($traspaso['team_to_sell_to'] == Database::ID_MERCADO_ASIATICO)
            {
                $team_id=htmlspecialchars(strip_tags($team_id));
                $stmt->bindParam(":team_id", $team_id);
            }

            $ventaActualizada = $stmt->execute();



            $equipo = new Equipo($this->conn);
            $equipo->id = $traspaso['team_id'];
            $presupuestoActualizado = $equipo->update_budget($traspaso['presupuesto'], $traspaso['is_top']);

            $bloqueValido = $bloqueValido 
                && $traspasoRealizado 
                && $ventaActualizada
                && $presupuestoActualizado
                && $periodo->getActualmenteEnPeriodo()
            ;
        }
        
        $resultadoBloque['bloqueValido'] = $bloqueValido;
        if($bloqueValido)
        {
            $this->conn->commit();
            return true;
        }
        else
        {
            $this->conn->rollback();
            return false;
        }
    }

    function get_is_valid_chain($cadena_id)
    {
        $resultadoBloque = array();
        $resultadoBloque['cadenaValida'] = true;
        $resultadoBloque['resultados'] = array();
        
        $mercado_asiatico_id = Database::ID_MERCADO_ASIATICO;

        // Busco todos los equipos involucrados como compradores en la cadena entera.
        $query = "SELECT buyer_id as team_id from intercambios
            WHERE engaged_to =:engaged_to
            AND buyer_id!=:mercado_asiatico;";

        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $cadena_id=htmlspecialchars(strip_tags($cadena_id));
        
        // bind values

        $stmt->bindParam(":engaged_to", $cadena_id);
        $stmt->bindParam(":mercado_asiatico", $mercado_asiatico_id);

        if($stmt->execute())
        {
            // Recorro los equipos involucrados en la cadena.
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
            {
                $data = '{"team_id":' . $row['team_id'] . ', "traspasos":['.$cadena_id.']}';
                $dataObject = json_decode($data);
                $resultadoParaElEquipo = $this->get_is_valid_block($dataObject);
                /*
                error_log("---Resultado para el equipo: ---");
                error_log($row['team_id']);
                error_log(json_encode($resultadoParaElEquipo));
                */

                array_push($resultadoBloque['resultados'], $resultadoParaElEquipo);
                //error_log("Cadena valida era:");
                //error_log($resultadoBloque['cadenaValida']?"verdad":"falsedad");
                $resultadoBloque['cadenaValida'] = ($resultadoBloque['cadenaValida'] && $resultadoParaElEquipo['bloqueValido']);
                //error_log("Cadena valida ahora es:");
                //error_log($resultadoBloque['cadenaValida']?"verdad":"falsedad");
            }
        }

        return $resultadoBloque;
    }


    function get_is_valid_block($data)
    {
        // error_log("en periodo de traspaso?");
        /*
        error_log("get_is_valid_block");
        error_log(json_encode($data));
        */


        $usuario = new Usuario($this->conn);
        $equipo = new Equipo($this->conn);
        $periodo = new Periodo($this->conn);
        
        $equipo->id = $data->team_id;
        $usuario->name = $equipo->read_owner();
        // query usuarios
        $stmt = $usuario->read_logged_user_data();
        $num = $stmt->rowCount();
        if($num>0)
        {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);
            
            $suma_tops = $equipo->get_suma_jugadores(1);
            $suma_perrunflas = $equipo->get_suma_jugadores(0);
            $bloqueValido = true;
            $resultadoBloque = array();
            $resultadoBloque['traspasos'] = array();
            for ($i=0; $i < sizeof($data->traspasos); $i++)
            {
                $stmt = $this->read_cadena_for_team($data->traspasos[$i], $data->team_id);
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $row2 = $stmt->fetch(PDO::FETCH_ASSOC);
                
                if($row['seller_id'] == $data->team_id)
                {
                    $venta = $row;
                    $compra = $row2;
                }
                else
                {
                    $venta = $row2;
                    $compra = $row;
                }

                $team_to_sell_to = $venta['buyer_id']; // Encuentro el equipo que está comprando mi jugador.

                
                /*
                error_log("compra['id']");
                error_log($compra['id']);
                error_log("compra['seller_accepted']");
                error_log($compra['seller_accepted']);
                error_log("compra['buyer_accepted']");
                error_log($compra['buyer_accepted']);
                error_log("compra['added_top_points']");
                error_log($compra['added_top_points']);
                error_log("compra['added_perrunflas_points']");
                error_log($compra['added_perrunflas_points']);
                error_log("compra['seller_id']");
                error_log($compra['seller_id']);
                error_log("compra['buyer_id']");
                error_log($compra['buyer_id']);
                error_log("compra['player_id']");
                error_log($compra['player_id']);
                
                error_log("venta['id']");
                error_log($venta['id']);
                error_log("venta['seller_accepted']");
                error_log($venta['seller_accepted']);
                error_log("venta['buyer_accepted']");
                error_log($venta['buyer_accepted']);
                error_log("venta['added_top_points']");
                error_log($venta['added_top_points']);
                error_log("venta['added_perrunflas_points']");
                error_log($venta['added_perrunflas_points']);
                error_log("venta['seller_id']");
                error_log($venta['seller_id']);
                error_log("venta['buyer_id']");
                error_log($venta['buyer_id']);
                error_log("venta['player_id']");
                error_log($venta['player_id']);
                */

                $jugadorAComprar = new Jugador($this->conn);
                $jugadorAComprar->id = $compra['player_id'];
                $jugadorAComprar->read_one();


                $jugadorAVender = new Jugador($this->conn);
                $jugadorAVender->id = $venta['player_id'];
                $jugadorAVender->read_one();
                
                $vendedorAcepto = $compra['seller_accepted'];
                $compradorAcepto = $venta['buyer_accepted'];
                $jugadorAcepto = $compra['buyer_accepted'] && $venta['seller_accepted'];


                if($jugadorAVender->is_top == 1)
                {
                    $resCadena = $this->es_valido_traspaso_top($top_budget, $perrunflas_budget, $suma_tops, $suma_perrunflas, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $compra['added_top_points'], $compra['added_perrunflas_points'], $venta['added_top_points'], $venta['added_perrunflas_points'], $venta['is_super_perrun'], $venta['is_pelo_a_pelo']);
                    $top_budget = $resCadena['presupuesto'];
                    $suma_tops = $resCadena['suma_jugadores'];
                }
                else
                {
                    // error_log("Venta:");
                    // error_log("id:" . $venta['id']);
                    // error_log("is_super_perrun:" . $venta['is_super_perrun']);
                    // error_log("buyer_id:" . $venta['buyer_id']);
                    // error_log("seller_id:" . $venta['seller_id']);
                    $resCadena = $this->es_valido_traspaso_perrunfla($perrunflas_budget, $top_budget, $suma_perrunflas, $suma_tops, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $compra['added_top_points'], $compra['added_perrunflas_points'], $venta['added_top_points'], $venta['added_perrunflas_points'], $venta['is_super_perrun'], $venta['is_pelo_a_pelo']);
                    $perrunflas_budget = $resCadena['presupuesto'];
                    $suma_perrunflas = $resCadena['suma_jugadores'];
                }
                $resCadena['id1'] = $row['id'];
                $resCadena['id2'] = $row2['id'];
                $resCadena['cadenaId'] = $data->traspasos[$i];
                $resCadena['id_jugador_a_vender'] = $venta['player_id'];
                $resCadena['id_jugador_a_comprar'] = $compra['player_id'];
                $resCadena['team_id'] = $data->team_id;
                $resCadena['team_to_sell_to'] = $team_to_sell_to;
                $resCadena['is_top'] = $jugadorAVender->is_top;
                $resCadena['isSuperPerrun'] = $venta['is_super_perrun'];
                $resCadena['isPeloPelo'] = $venta['is_pelo_a_pelo'];
                $bloqueValido = $bloqueValido && $resCadena['result'];
                array_push($resultadoBloque['traspasos'], $resCadena);
            }
            
            $resultadoBloque['bloqueValido'] = $bloqueValido;
            return $resultadoBloque;
        }
        else
        {
            return null;
        }
    }

    function read_cadena_for_team($cadena_id, $team_id)
    {
        $query = "SELECT * FROM intercambios
            WHERE engaged_to =:cadena_id AND (buyer_id=:buyer_id OR seller_id=:seller_id);";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $cadena_id=htmlspecialchars(strip_tags($cadena_id));
        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":cadena_id", $cadena_id);
        $stmt->bindParam(":seller_id", $team_id);
        $stmt->bindParam(":buyer_id", $team_id);
        
        /*
        error_log($query);
        error_log($cadena_id);
        error_log($team_id);
        */
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function es_valido_traspaso_top($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_top_points_compra, $added_perrunflas_points_compra, $added_top_points_venta, $added_perrunflas_points_venta, $esSuperPerrunfla, $esPeloPelo)
    {
        return $this->es_valido_traspaso($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, Database::TOPE_TOP, "Top", $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_top_points_compra, $added_perrunflas_points_compra, $added_top_points_venta, $added_perrunflas_points_venta, $esSuperPerrunfla, $esPeloPelo);
    }

    function es_valido_traspaso_perrunfla($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_top_points_compra, $added_perrunflas_points_compra, $added_top_points_venta, $added_perrunflas_points_venta, $esSuperPerrunfla, $esPeloPelo)
    {
        /** 
        * Reviso que la cadena sea un super perrun pero además que esta venta en particular sea de un súper perrun, sólo en ese
        * caso se aplica la regla especial de venta de súper perrunfla.
        * NOTA: Si fuese una cadena done hay dos ventas de jugadores que son súperPerrun y se inició como una súperPerrun, entonces 
        * asumo que ambos van a tratarse con la regla de súper perrun. Es un caso muy raro pero podría pasar, para diferenciarlo 
        * habría que tener una forma de diferenciar cuál de los dos fue el iniciado como súper perrun y encima dejar ofrecer en cada
        * venta que se encadene a la cadena, si quiere ser tratada como súperPerrun... demasiado pana algo que nunca va a usarse.
        */
        if($esSuperPerrunfla && $jugadorAVender->esSuperPerrunfla && $team_to_sell_to != Database::ID_MERCADO_ASIATICO)
        {
            return $this->esValidoTraspasoSuperPerrun($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, Database::TOPE_PERRUNFLAS, "Perrunfla", $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_perrunflas_points_compra, $added_top_points_compra, $added_perrunflas_points_venta, $added_top_points_venta);
        }
        else
        {
            return $this->es_valido_traspaso($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, Database::TOPE_PERRUNFLAS, "Perrunfla", $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_perrunflas_points_compra, $added_top_points_compra, $added_perrunflas_points_venta, $added_top_points_venta, $esSuperPerrunfla, $esPeloPelo);
        }
    }

    function esValidoTraspasoSuperPerrun($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $tope, $categoria, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_points_compra, $added_points_compraOtraCategoria, $added_points_venta, $added_points_ventaOtraCategoria)
    {

        /**
        * TODO falta el pago de 2 arriba por parte del comprador. Podría hacerse subiendo 2 el added_top_points y luego restando 2
        * del incremento del presupuesto top del vendedor para que dichos added points no le lleguen ya que así es la regla. Por lo 
        * visto faltaría agregar acá que disminuya el presupuesto top del comprador cosa que no parece estar muy preparada, ver cómo
        * es la mejor forma de hacerlo.
        */


        $resto = $presupuesto - $suma_jugadores;

        $periodo = new Periodo($this->conn);

        /**
        * No anulo los added points que son al o del mercado asiático porque el jugador siendo vendido es de un Dynastyer (porque 
        * las compras de súperPerrun son exclusivas a Dynastyers) y el que está comprando (al que le venden) tiene que ser otro 
        * Dynastyer, de lo contrario sería sólo un perrunfla normalmente vendido al MA.
        */
        // if($jugadorAComprar->team_id == "" ||  $jugadorAComprar->team_id == Database::ID_MERCADO_ASIATICO)
        // {
        //     //error_log("En el 1 if");
        //     $added_points_compra = 0;
        //     $added_points_compraOtraCategoria = 0;
        // }

        // if($team_to_sell_to == "" || $team_to_sell_to == Database::ID_MERCADO_ASIATICO)
        // {
        //     //error_log("En el 2 if");
        //     $added_points_venta = 0;
        //     $added_points_ventaOtraCategoria = 0;
        // }


        // Les pongo 66 como mínimo overall_rating admitido por las dudas.
        $jugadorAVender->overall_rating = max(Database::VALOR_MÍNIMO_TRASPASOS, $jugadorAVender->overall_rating);
        $jugadorAComprar->overall_rating = max(Database::VALOR_MÍNIMO_TRASPASOS, $jugadorAComprar->overall_rating);


        $valorJugadorAComprar   = $jugadorAComprar->overall_rating;
        $overallJugadorAVender  = $jugadorAVender->overall_rating;
        $pagadoJugadorAVender   = $jugadorAVender->price_paid;        

        $falloIntercambio       = false;
        $resultadoIntercambio   = "N / A";


        /**
        * Reviso la frontera.
        */
        $equipoUsuario = new Equipo($this->conn);
        $equipoUsuario->id = $jugadorAVender->team_id;
        // Tiene que ser perrun
        // if ($categoria == "Top")
        // {
        //     $frontera = $equipoUsuario->getMaxPerrunfla();
        //     $violaFrontera = $valorJugadorAComprar < $frontera;
        // }
        // else
        // {
            $frontera = $equipoUsuario->getMinTop();
            $violaFrontera = $valorJugadorAComprar > $frontera;
        // }
        

        $equipoCompradorDelSuperPerrun = new Equipo($this->conn);
        $equipoCompradorDelSuperPerrun->id = $team_to_sell_to;
        // error_log("jugadorAComprar->id: " . $jugadorAComprar->id);
        // error_log("jugadorAComprar->name: " . $jugadorAComprar->name);
        // error_log("jugadorAComprar->team_id: " . $jugadorAComprar->team_id);
        $equipoCompradorDelSuperPerrun->read_one();

        // $crecimiento = $overallJugadorAVender - $pagadoJugadorAVender; // $crecimiento o de$crecimiento
        
        // $faltanteParaTopear = $tope - 1 - $presupuesto;
        // if ($crecimiento  > $faltanteParaTopear) // Me paso del tope.
            // $gananciaJT = max($faltanteParaTopear, 0) + $crecimiento / abs($crecimiento);
        // else
            // $gananciaJT = $crecimiento; // Crecen y decrecen libremente.

        // $valorVentaJugador = $pagadoJugadorAVender + $gananciaJT; // el valor al que realmente voy a vender al jugador
        // $valorVentaJugador = $jugadorAVender->calcularValorDeVenta(); // el valor al que realmente voy a vender al jugador


        // $gananciaJT = $valorVentaJugador - $pagadoJugadorAVender;
        $gananciaPerrun = $frontera - $pagadoJugadorAVender; // Genera tantos perrunflas como haya crecido hasta la frontera
        $gananciaTop = $jugadorAVender->overall_rating - $frontera; // Los puntos que crece por arriba de la frontera son generados como tops.

        $restoOtraCategoria = $presupuestoOtraCategoria - $suma_jugadoresOtraCategoria;
        $perdidaOtraCategoriaPorTraspaso = $added_points_compraOtraCategoria - $added_points_ventaOtraCategoria - $gananciaTop;
        $otraCategoriaFalla = $perdidaOtraCategoriaPorTraspaso > $restoOtraCategoria;
        
        // $costoIntercambio = $valorJugadorAComprar - $valorVentaJugador; // cuánto me sale la compra
        $costoIntercambio = $valorJugadorAComprar - $frontera; // cuánto me sale la compra, uso frontera porque es lo que realmente está valiendo el perrunfla en puntos perrunflas. Como el perrunfla que voy a traer tiene que costar lo mismo o menos que el top más barato, entonces costoIntercambio debería ser menor o igual a 0 siempre, salvo que violaFrontera sea true.

        $presupuestoViejo = $presupuesto;
        $presupuestoViejoOtraCategoria = $presupuestoOtraCategoria;
        // $otraCategoria = ($categoria == "Top")?"Perrunflas":"Top";
        $otraCategoria = "Top";

        $restoTopComprador = $equipoCompradorDelSuperPerrun->top_budget - $equipoCompradorDelSuperPerrun->get_suma_jugadores(1);
        $leAlcanzaAlComprador = $restoTopComprador >= 2;

        // error_log("restoTopComprador: {$restoTopComprador}");
        // error_log("leAlcanzaAlComprador: ". ($leAlcanzaAlComprador?'Sí':'No'));

        $resultadoIntercambio = "";
        if(!$leAlcanzaAlComprador)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: {$equipoCompradorDelSuperPerrun->name} quiso comprar un súper perrunfla pero su resto top es de {$restoTopComprador} y precisaría pagar 2 top para efectuar la compra. ";
        }
        if($otraCategoriaFalla)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: No se puede hacer la venta súper perrunfla porque a $jugadorAVender->team_name le falló la categoría $otraCategoria.";
            $resultadoIntercambio .= " Ingresó con resto de $otraCategoria: $restoOtraCategoria y pagó $added_points_compraOtraCategoria para comprar y recibió $added_points_ventaOtraCategoria por la venta.";
            $resultadoIntercambio .= " Esto le generó una pérdida de $perdidaOtraCategoriaPorTraspaso y por ser mayor al resto, falló la operación. ";
        }
        if($resto + $added_points_venta - $costoIntercambio - $added_points_compra < 0)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar;
            $resultadoIntercambio .= ($added_points_compra != 0)?", pagando arriba DY$$added_points_compra":"";
            $resultadoIntercambio .= " y le quiso vender a " . $jugadorAVender->name . " a DY$"  . $jugadorAVender->overall_rating . "";
            $resultadoIntercambio .= ($added_points_venta != 0)?", recibiendo arriba DY$$added_points_venta,":"";
            $resultadoIntercambio .= " a ". $jugadorAComprar->team_name ." con un presupuesto $categoria de " . $presupuestoViejo . " y un resto de $resto. Por esto la cuenta daba: " . ($resto + $added_points_venta - $costoIntercambio - $added_points_compra) . " y no puede quedar en resto negativo. ";
        }
        if($violaFrontera)
        {
            $falloIntercambio = true;
            // if($categoria == "Top")
            // {
                // $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar . ", vendiendo a un top, pero su máximo perrunfla vale $frontera y no puede adquirir a un top más barato que el perrunfla más caro. ";
            // }
            // else
            // {
                $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar . ", vendiendo a un súper perrunfla, pero su mínimo top vale $frontera y no puede adquirir a un perrunfla más caro que su top más barato. ";
            // }
        }
        if(!$vendedorAcepto)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el vendedor no acepto aún. ";
        }
        if(!$jugadorAcepto) // ¿? No tengo idea de por qué hay 3 dynastyers aceptando, jajaja.
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el jugador no acepto aún. ";
        }
        if(!$compradorAcepto)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el comprador no acepto aún. ";
        }
        if(!$periodo->getActualmenteEnPeriodo())
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: No está en período de traspasos, el próximo período comenzará el: " . $periodo->getProximoInicio();
        }
        if(!$falloIntercambio) 
        {
            // Es válida la cadena de traspasos.
            $presupuesto = $presupuestoViejo + $gananciaPerrun + $added_points_venta - $added_points_compra;
            $presupuestoOtraCategoria = $presupuestoViejoOtraCategoria + $gananciaTop + $added_points_ventaOtraCategoria - $added_points_compraOtraCategoria;
            $suma_jugadores = $suma_jugadores - $jugadorAVender->overall_rating + $valorJugadorAComprar;

            $resultadoIntercambio = $jugadorAVender->team_name . " le compra a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar;
            $resultadoIntercambio .= ($added_points_compra != 0)?" y pone arriba DY$$added_points_compra":"";
            $resultadoIntercambio .= " a ". $jugadorAComprar->team_name ." y vende al súper perrun " . $jugadorAVender->name . " a DY$" . $jugadorAVender->overall_rating . " que le salió DY$" . $pagadoJugadorAVender;
            $resultadoIntercambio .= ($added_points_venta != 0)?" y recibe arriba DY$$added_points_venta":"";
            $resultadoIntercambio .= ", con un resto de " . $resto . " y un presupuesto $categoria de " . $presupuestoViejo . ".";
            $resultadoIntercambio .= " Los puntos generados por el súper perrun son: {$gananciaPerrun} puntos perrun (desde su precio de compra {$jugadorAVender->price_paid} hasta la fronteraTop de {$frontera}) y {$gananciaTop} puntos top (desde la fronteraTop de {$frontera} hasta el valor actual del súper perrun {$jugadorAVender->overall_rating})";
            $resultadoIntercambio .= ($presupuesto != $presupuestoViejo)?" Su nuevo presupuesto $categoria es: " . $presupuesto . ".":" No varió el presupuesto $categoria.";
            $resultadoIntercambio .= ($presupuestoOtraCategoria != $presupuestoViejoOtraCategoria)?" Varió el presupuesto $otraCategoria. Tenía $presupuestoViejoOtraCategoria de presupuesto $otraCategoria y ahora tiene: $presupuestoOtraCategoria.":" No varió el presupuesto $otraCategoria.";
            $resultadoIntercambio .= " Disminuiría el presupuesto top de {$equipoCompradorDelSuperPerrun->name} en 2 por el costo de la compra de un súper perrunfla.";

        }

        $resultadoSuperPerrun = array(
            "result" => !$falloIntercambio, 
            "message" => $resultadoIntercambio,
            "presupuesto" => $presupuesto,
            "presupuestoViejo" => $presupuestoViejo,
            "presupuestoOtraCategoria" => $presupuestoOtraCategoria,
            "presupuestoViejoOtraCategoria" => $presupuestoViejoOtraCategoria,
            "idCompradorDeSuperPerrun" => $equipoCompradorDelSuperPerrun->id,
            "is_super_perrun" => 1,
            "suma_jugadores" => $suma_jugadores,
            "suma_jugadoresOtraCategoria" => $suma_jugadoresOtraCategoria
        );
        // error_log("resultadoSuperPerrun");
        // error_log(json_encode($resultadoSuperPerrun));
        return $resultadoSuperPerrun;
    }


    function es_valido_traspaso($presupuesto, $presupuestoOtraCategoria, $suma_jugadores, $suma_jugadoresOtraCategoria, $jugadorAComprar, $jugadorAVender, $team_to_sell_to, $tope, $categoria, $vendedorAcepto, $compradorAcepto, $jugadorAcepto, $added_points_compra, $added_points_compraOtraCategoria, $added_points_venta, $added_points_ventaOtraCategoria, $esSuperPerrunfla, $esPeloPelo)
    {
        $resto = $presupuesto - $suma_jugadores;



        $periodo = new Periodo($this->conn);
        // error_log("periodo->getActualmenteEnPeriodo()");
        // error_log($periodo->getActualmenteEnPeriodo());

        /**
        * Anulo los added points que son al o del mercado asiático
        */
        /*
        error_log("jugadorAComprar->team_id isset($jugadorAComprar->team_id)");
        error_log("team_to_sell_to $team_to_sell_to");
        */
        if($jugadorAComprar->team_id == "" ||  $jugadorAComprar->team_id == Database::ID_MERCADO_ASIATICO)
        {
            //error_log("En el 1 if");
            $added_points_compra = 0;
            $added_points_compraOtraCategoria = 0;
        }

        if($team_to_sell_to == "" || $team_to_sell_to == Database::ID_MERCADO_ASIATICO)
        {
            //error_log("En el 2 if");
            $added_points_venta = 0;
            $added_points_ventaOtraCategoria = 0;
        }



        $jugadorAVender->overall_rating = max(Database::VALOR_MÍNIMO_TRASPASOS, $jugadorAVender->overall_rating);
        $jugadorAComprar->overall_rating = max(Database::VALOR_MÍNIMO_TRASPASOS, $jugadorAComprar->overall_rating);
        $valorJugadorAComprar   = $jugadorAComprar->overall_rating;
        $overallJugadorAVender  = $jugadorAVender->overall_rating;
        $pagadoJugadorAVender   = $jugadorAVender->price_paid;

        $falloIntercambio       = false;
        $resultadoIntercambio   = "N / A";


        /**
        * Reviso la frontera.
        */
        $equipoUsuario = new Equipo($this->conn);
        $equipoUsuario->id = $jugadorAVender->team_id;
        if ($categoria == "Top")
        {
            $frontera = $equipoUsuario->getMaxPerrunfla();
            $violaFrontera = $valorJugadorAComprar < $frontera;
        }
        else
        {
            $frontera = $equipoUsuario->getMinTop();
            $violaFrontera = $valorJugadorAComprar > $frontera;
        }
        



        // $crecimiento = $overallJugadorAVender - $pagadoJugadorAVender; // $crecimiento o de$crecimiento
        
        // $faltanteParaTopear = $tope - 1 - $presupuesto;
        // if ($crecimiento  > $faltanteParaTopear) // Me paso del tope.
            // $gananciaJT = max($faltanteParaTopear, 0) + $crecimiento / abs($crecimiento);
        // else
            // $gananciaJT = $crecimiento; // Crecen y decrecen libremente.

        // $valorVentaJugador = $pagadoJugadorAVender + $gananciaJT; // el valor al que realmente voy a vender al jugador
        $valorVentaJugador = $jugadorAVender->calcularValorDeVenta(); // el valor al que realmente voy a vender al jugador
        $gananciaJT = $valorVentaJugador - $pagadoJugadorAVender;

        $restoOtraCategoria = $presupuestoOtraCategoria - $suma_jugadoresOtraCategoria;
        $perdidaOtraCategoriaPorTraspaso = $added_points_compraOtraCategoria - $added_points_ventaOtraCategoria;
        $otraCategoriaFalla = $perdidaOtraCategoriaPorTraspaso > $restoOtraCategoria;
        
        $costoIntercambio = $valorJugadorAComprar - $valorVentaJugador; // cuánto me sale la compra

        $presupuestoViejo = $presupuesto;
        $presupuestoViejoOtraCategoria = $presupuestoOtraCategoria;
        $otraCategoria = ($categoria == "Top")?"Perrunflas":"Top";

        /**
        * Reviso si es súper perrun y le da para pagar los 2 tops, en dicho caso.
        */
        $alcanzaParaPagarSuperPerrun = true;
        // error_log("comprando super perrun? " . ($jugadorAComprar->esSuperPerrunfla?"Sí":"No"));
        // error_log("traspaso super perrun? " . ($esSuperPerrunfla?"Sí":"No"));
        if($esSuperPerrunfla && $jugadorAComprar->esSuperPerrunfla)
        {
            if($categoria == "Top") // Está vendiendo a un top, por lo tanto compra a un jugador que será top para el comprador.
            {
                $alcanzaParaPagarSuperPerrun = (($resto + $added_points_venta - $costoIntercambio - $added_points_compra - 2) >= 0);
            }
            else // Está vendiendo a un perrun, por lo tanto compra a un jugador que será perrun para el comprador.
            {
                $alcanzaParaPagarSuperPerrun = (($restoOtraCategoria + $added_points_ventaOtraCategoria - $added_points_compraOtraCategoria - 2) >= 0);
            }
        }

        /**
        * Reviso si falla el pelo a pelo, sólo puede pasar si hay una distancia mayor o igual a 4 entre los 
        * overall-ratings actuales de los jugadores cambiados.
        */
        $fallaPeloPelo = false;
        // error_log("Revisando pelo a pelo");
        // error_log("Overall actual de jugador a comprar ({$jugadorAComprar->name}): {$jugadorAComprar->overall_rating}.");
        // error_log("Overall actual de jugador a vender ({$jugadorAVender->name}): {$jugadorAVender->overall_rating}.");
        if($esPeloPelo && !(abs($jugadorAComprar->overall_rating-$jugadorAVender->overall_rating) <= 3))
        {
            // error_log("Falló el pelo a pelo");
            $fallaPeloPelo = true;
        }
        


        /**
        * Con todos los resultados generados, se arma el resultado y texto final.
        */
        $resultadoIntercambio = "";
        if(!$esPeloPelo)
        {
            if(!$alcanzaParaPagarSuperPerrun)
            {
                $falloIntercambio = true;
                $resultadoIntercambio .= "Error: No le alcanza para pagar el traspaso súper perrunfla ";
                if($categoria == "Top") 
                {
                    $resultadoIntercambio .= "porque tiene resto top ({$resto}) + puntos tops que le pagan por la venta ({$added_points_venta}) - costo del intercambio ({$costoIntercambio}) - puntos tops que paga arriba de la compra ({$added_points_compra}) - pago de puntos tops por la compra súper perrun (2) = " . ($resto + $added_points_venta - $costoIntercambio - $added_points_compra - 2) . " que no es mayor o igual que 0, por lo que quedaría en presupuesto top negativo.";
                }
                else
                {
                    $resultadoIntercambio .= "porque tiene resto top ({$restoOtraCategoria}) + puntos tops que le pagan por la venta ({$added_points_ventaOtraCategoria}) - puntos tops que paga arriba de la compra ({$added_points_compraOtraCategoria}) - pago de puntos tops por la compra súper perrun (2) = " . ($restoOtraCategoria + $added_points_ventaOtraCategoria - $added_points_compraOtraCategoria - 2) . " que no es mayor o igual que 0, por lo que quedaría en presupuesto top negativo.";
                }
            }
            if($otraCategoriaFalla)
            {
                $falloIntercambio = true;
                $resultadoIntercambio .= "Error: No se puede hacer el intercambio porque a $jugadorAVender->team_name le falló la categoría $otraCategoria.";
                $resultadoIntercambio .= " Ingresó con resto de $otraCategoria: $restoOtraCategoria y pagó $added_points_compraOtraCategoria para comprar y recibió $added_points_ventaOtraCategoria por la venta.";
                $resultadoIntercambio .= " Esto le generó una pérdida de $perdidaOtraCategoriaPorTraspaso y por ser mayor al resto, falló la operación. ";
            }
            if($resto + $added_points_venta - $costoIntercambio - $added_points_compra < 0)
            {
                $falloIntercambio = true;
                $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar;
                $resultadoIntercambio .= ($added_points_compra != 0)?", pagando arriba DY$$added_points_compra":"";
                $resultadoIntercambio .= " y le quiso vender a " . $jugadorAVender->name . " a DY$"  . $valorVentaJugador . "";
                $resultadoIntercambio .= ($added_points_venta != 0)?", recibiendo arriba DY$$added_points_venta,":"";
                $resultadoIntercambio .= " a ". $jugadorAComprar->team_name ." con un presupuesto $categoria de " . $presupuestoViejo . " y un resto de $resto. Por esto la cuenta daba: " . ($resto + $added_points_venta - $costoIntercambio - $added_points_compra) . " y no puede quedar en resto negativo. ";
            }
            if($violaFrontera)
            {
                $falloIntercambio = true;
                if($categoria == "Top")
                {
                    $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar . ", vendiendo a un top, pero su máximo perrunfla vale $frontera y no puede adquirir a un top más barato que el perrunfla más caro. ";
                }
                else
                {
                    $resultadoIntercambio .= "Error: " . $jugadorAVender->team_name . " quiso comprar a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar . ", vendiendo a un perrunfla, pero su mínimo top vale $frontera y no puede adquirir a un perrunfla más caro que su top más barato. ";
                }
            }
        }
        else
        {
            if($fallaPeloPelo)
            {
                $falloIntercambio = true;
                $resultadoIntercambio .= "porque no cumple las reglas de Pelo A Pelo (quieren intercambiar jugadores que tienen overall-ratings distanciados por 4 o más puntos: {$valorJugadorAComprar} y {$overallJugadorAVender}).";
            }
        }

        /**
        * Reviso los resultados que afectan a todas las operaciones (tiene que ser aceptada por todas las partes y estar
        * dentro del período para funcionar, cualquiera sea la operación).
        */
        if(!$vendedorAcepto)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el vendedor no acepto aún. ";
        }
        if(!$jugadorAcepto)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el jugador no acepto aún. ";
        }
        if(!$compradorAcepto)
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: el comprador no acepto aún. ";
        }
        if(!$periodo->getActualmenteEnPeriodo())
        {
            $falloIntercambio = true;
            $resultadoIntercambio .= "Error: No está en período de traspasos, el próximo período comenzará el: " . $periodo->getProximoInicio();
        }
        
        

        /**
        * Si no falló el intercambio, armo el mensaje que avisa que salió todo bien y doy 
        * los resultados de los presupuestos.
        */
        if(!$falloIntercambio) 
        {
            if(!$esPeloPelo)
            {
                // Es válida la cadena de traspasos.
                $presupuesto = $presupuestoViejo + $gananciaJT + $added_points_venta - $added_points_compra;
                $presupuestoOtraCategoria = $presupuestoViejoOtraCategoria + $added_points_ventaOtraCategoria - $added_points_compraOtraCategoria;
                
                if($esSuperPerrunfla && $jugadorAComprar->esSuperPerrunfla) // Está comprando a un súper perrunfla, paga 2 tops.
                {
                    $presupuesto -= (($categoria == "Top")?2:0);
                    $presupuestoOtraCategoria -= (($categoria != "Top")?2:0);
                } 


                $suma_jugadores = $suma_jugadores - $pagadoJugadorAVender + $valorJugadorAComprar;
                $resultadoIntercambio = $jugadorAVender->team_name . " le compra a " . $jugadorAComprar->name . " a DY$" . $valorJugadorAComprar;
                if($esSuperPerrunfla && $jugadorAComprar->esSuperPerrunfla) $resultadoIntercambio .= ", paga arriba los 2 tops por compra de súper perrunfla ";
                $resultadoIntercambio .= ($added_points_compra != 0)?" y pone arriba DY$$added_points_compra":"";
                $resultadoIntercambio .= " a ". $jugadorAComprar->team_name ." y vende a " . $jugadorAVender->name . " a DY$"  . $valorVentaJugador . " que le salió DY$" . $pagadoJugadorAVender;
                $resultadoIntercambio .= ($added_points_venta != 0)?" y recibe arriba DY$$added_points_venta":"";
                $resultadoIntercambio .= ", con un resto de " . $resto . " y un presupuesto $categoria de " . $presupuestoViejo . ".";
                $resultadoIntercambio .= ($presupuesto != $presupuestoViejo)?" Su nuevo presupuesto $categoria es: " . $presupuesto . ".":" No varió el presupuesto $categoria.";
                $resultadoIntercambio .= ($presupuestoOtraCategoria != $presupuestoViejoOtraCategoria)?" Varió el presupuesto $otraCategoria porque recibió DY$$added_points_ventaOtraCategoria por la venta y pagó DY$$added_points_compraOtraCategoria " . (($esSuperPerrunfla && $jugadorAComprar->esSuperPerrunfla && $categoria != "Top")?("+ 2 tops por comprar un súper perrunfla "):(" ")) . "por la compra. Tenía $presupuestoViejoOtraCategoria de presupuesto $otraCategoria y ahora tiene: $presupuestoOtraCategoria.":" No varió el presupuesto $otraCategoria.";
            }
            else // Es pelo a pelo y salió bien.
            {
                $presupuesto=$presupuestoViejo;
                $presupuestoOtraCategoria=$presupuestoViejoOtraCategoria;
                $resultadoIntercambio = $jugadorAVender->team_name . " le compra a " . $jugadorAComprar->name . " a DY$" . $pagadoJugadorAVender;
                $resultadoIntercambio .= " a ". $jugadorAComprar->team_name ." y vende a " . $jugadorAVender->name . " a DY$"  . $valorVentaJugador;

            }
        }
        return array(
            "result" => !$falloIntercambio, 
            "message" => $resultadoIntercambio,
            "presupuesto" => $presupuesto,
            "presupuestoViejo" => $presupuestoViejo,
            "presupuestoOtraCategoria" => $presupuestoOtraCategoria,
            "presupuestoViejoOtraCategoria" => $presupuestoViejoOtraCategoria,
            "suma_jugadores" => $suma_jugadores,
            "suma_jugadoresOtraCategoria" => $suma_jugadoresOtraCategoria
        );
    }

    function set_cadena_state($cadena_id, $team_id, $accept)
    {
        $this->conn->beginTransaction();
        $query = "UPDATE ". $this->table_name ."
            SET seller_accepted =:accept
            WHERE engaged_to =:engaged_to
            AND seller_id =:team_id;";

        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $cadena_id=htmlspecialchars(strip_tags($cadena_id));
        $team_id=htmlspecialchars(strip_tags($team_id));
        $accept=htmlspecialchars(strip_tags($accept));
        
        // bind values
        $stmt->bindParam(":engaged_to", $cadena_id);
        $stmt->bindParam(":team_id", $team_id);
        $stmt->bindParam(":accept", $accept);
        
        $sellUpdated = $stmt->execute();

        $query = "UPDATE ". $this->table_name ."
            SET buyer_accepted =:accept
            WHERE engaged_to =:engaged_to
            AND buyer_id =:team_id;";

        $stmt = $this->conn->prepare($query);
     
        
        // bind values
        $stmt->bindParam(":engaged_to", $cadena_id);
        $stmt->bindParam(":team_id", $team_id);
        $stmt->bindParam(":accept", $accept);
        
        
        if($sellUpdated && $stmt->execute()){
            $this->conn->commit();
            return true;
        }
        $this->conn->rollback();
        return false;
    }

    // read Traspasos
    function read(){
        // select all query
        $query = "
            SELECT * FROM " . $this->table_name . ";";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
    function get_amount($team_id)
    {
        // select all query
        $query = "
            SELECT count(*) as cantidad
            FROM `intercambios` 
            WHERE `is_pending` = 1 
            AND (`seller_id` = :team_id 
            OR `buyer_id` = :team_id); ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
        
        // execute query
        $stmt->execute();
        $amount = $stmt->fetch(PDO::FETCH_ASSOC);
        /*error_log($amount["cantidad"]);
        error_log("team_id: $team_id");
        */return $amount["cantidad"];
    }

    function read_no_chains_from_team($team_id)
    {
        // select all query
        $query = "SELECT i.*, j.name as player_name, je.is_top, e1.name as buyer_name, e2.name as seller_name
        FROM intercambios i, jugadores j left join jugadores_equipos je on je.player_id = j.id, equipos e1, equipos e2
        WHERE engaged_to NOT IN(
            SELECT DISTINCT(engaged_to) FROM intercambios
            WHERE buyer_id !=:mercado_asiatico
                AND seller_id !=:mercado_asiatico
            )
        AND j.id = i.player_id
        AND buyer_id = e1.id
        AND seller_id = e2.id
        AND is_pending = 1
        AND is_super_perrun = 0
        AND is_pelo_a_pelo = 0
        AND (
            seller_id =:team_id
            Or buyer_id =:team_id
            );";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
        $idMercadoAsiatico = Database::ID_MERCADO_ASIATICO;
        $stmt->bindParam(":mercado_asiatico", $idMercadoAsiatico);
        
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function read_updates_from_team($team_id)
    {
        $query = "SELECT i.*, j.name as player_name, je.is_top, e.name as buyer_name
        FROM intercambios i, jugadores j left join jugadores_equipos je on je.player_id = j.id, equipos e
            WHERE j.id = i.player_id
            AND buyer_id = e.id
            AND seller_id = buyer_id
            AND is_pending = 1
            AND is_super_perrun = 0
            AND is_pelo_a_pelo = 0
            AND seller_id =:team_id;";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    
    function read_chains_from_team($team_id)
    {
        // select all query
        $query = "SELECT i.*, j.name as player_name, je.is_top, e1.name as buyer_name, e2.name as seller_name
        FROM intercambios i, jugadores j left join jugadores_equipos je on je.player_id = j.id, equipos e1, equipos e2
        WHERE engaged_to IN(
            SELECT DISTINCT(engaged_to) FROM intercambios
            WHERE buyer_id !=:mercado_asiatico
                AND seller_id !=:mercado_asiatico
            )
        AND j.id = i.player_id
        AND buyer_id = e1.id
        AND seller_id = e2.id
        AND buyer_id != seller_id
        AND is_pending = 1
        AND is_super_perrun = 0
        AND is_pelo_a_pelo = 0
        AND (
            seller_id =:team_id
            Or buyer_id =:team_id
            );";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
        $idMercadoAsiatico = Database::ID_MERCADO_ASIATICO;
        $stmt->bindParam(":mercado_asiatico", $idMercadoAsiatico);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
    function read_super_perrun_from_team($team_id)
    {
        // select all query
        $query = "SELECT i.*, j.name as player_name, je.is_top, e1.name as buyer_name, e2.name as seller_name
        FROM intercambios i, jugadores j left join jugadores_equipos je on je.player_id = j.id, equipos e1, equipos e2
        WHERE engaged_to IN(
            SELECT DISTINCT(engaged_to) FROM intercambios
            WHERE buyer_id !=:mercado_asiatico
                AND seller_id !=:mercado_asiatico
            )
        AND j.id = i.player_id
        AND buyer_id = e1.id
        AND seller_id = e2.id
        AND buyer_id != seller_id
        AND is_pending = 1
        AND is_super_perrun = 1
        AND (
            seller_id =:team_id
            Or buyer_id =:team_id
            );";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
        $idMercadoAsiatico = Database::ID_MERCADO_ASIATICO;
        $stmt->bindParam(":mercado_asiatico", $idMercadoAsiatico);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
function read_pelo_a_pelo_from_team($team_id)
    {
        // select all query
        $query = "SELECT i.*, j.name as player_name, je.is_top, e1.name as buyer_name, e2.name as seller_name
        FROM intercambios i, jugadores j left join jugadores_equipos je on je.player_id = j.id, equipos e1, equipos e2
        WHERE engaged_to IN(
            SELECT DISTINCT(engaged_to) FROM intercambios
            WHERE buyer_id !=:mercado_asiatico
                AND seller_id !=:mercado_asiatico
            )
        AND j.id = i.player_id
        AND buyer_id = e1.id
        AND seller_id = e2.id
        AND buyer_id != seller_id
        AND is_pending = 1
        AND is_pelo_a_pelo = 1
        AND (
            seller_id =:team_id
            Or buyer_id =:team_id
            );";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $team_id=htmlspecialchars(strip_tags($team_id));

        $stmt->bindParam(":team_id", $team_id);
        $idMercadoAsiatico = Database::ID_MERCADO_ASIATICO;
        $stmt->bindParam(":mercado_asiatico", $idMercadoAsiatico);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function update_engaged_id()
    {
        $query = "UPDATE ". $this->table_name ."
            SET engaged_to =:engaged_to
            WHERE seller_id =:seller_id
            AND buyer_id =:buyer_id
            AND player_id =:player_id
            AND sell_date =:sell_date
            AND id=:id;";

        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->seller_id=htmlspecialchars(strip_tags($this->seller_id));
        $this->buyer_id=htmlspecialchars(strip_tags($this->buyer_id));
        $this->player_id=htmlspecialchars(strip_tags($this->player_id));
        $this->sell_date=htmlspecialchars(strip_tags($this->sell_date));
        $this->engaged_to=htmlspecialchars(strip_tags($this->engaged_to));
        $this->id=htmlspecialchars(strip_tags($this->id));
        
        // bind values
        $stmt->bindParam(":buyer_id", $this->buyer_id);
        $stmt->bindParam(":player_id", $this->player_id);
        $stmt->bindParam(":seller_id", $this->seller_id);
        $stmt->bindParam(":sell_date", $this->sell_date);
        $stmt->bindParam(":engaged_to", $this->engaged_to);
        $stmt->bindParam(":id", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    // create Traspaso
    function create(){

        $is_engaged_set = !empty($this->engaged_to);
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    seller_id=:seller_id, buyer_id=:buyer_id, player_id=:player_id, sell_date=:sell_date, sell_price=:sell_price, is_pending=:is_pending, is_super_perrun=:is_super_perrun, is_pelo_a_pelo=:is_pelo_a_pelo, seller_accepted=:seller_accepted, buyer_accepted=:buyer_accepted, added_top_points=:added_top_points, added_perrunflas_points=:added_perrunflas_points";

        if($is_engaged_set)
        {
            $query = $query . ", engaged_to=:engaged_to;";
        }
        else
        {
            $query = $query . ";";
        }
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->seller_id=htmlspecialchars(strip_tags($this->seller_id));
        $this->buyer_id=htmlspecialchars(strip_tags($this->buyer_id));
        $this->player_id=htmlspecialchars(strip_tags($this->player_id));
        $this->sell_date=htmlspecialchars(strip_tags($this->sell_date));
        $this->sell_price=htmlspecialchars(strip_tags($this->sell_price));
        $this->is_pending=htmlspecialchars(strip_tags($this->is_pending));
        $this->is_super_perrun=htmlspecialchars(strip_tags($this->is_super_perrun));
        $this->is_pelo_a_pelo=htmlspecialchars(strip_tags($this->is_pelo_a_pelo));
        $this->seller_accepted=htmlspecialchars(strip_tags($this->seller_accepted));
        $this->buyer_accepted=htmlspecialchars(strip_tags($this->buyer_accepted));
        $this->added_top_points=htmlspecialchars(strip_tags($this->added_top_points));
        $this->added_perrunflas_points=htmlspecialchars(strip_tags($this->added_perrunflas_points));
        
        // bind values
        $stmt->bindParam(":buyer_id", $this->buyer_id);
        $stmt->bindParam(":player_id", $this->player_id);
        $stmt->bindParam(":seller_id", $this->seller_id);
        $stmt->bindParam(":sell_date", $this->sell_date);
        $stmt->bindParam(":sell_price", $this->sell_price);
        $stmt->bindParam(":is_pending", $this->is_pending);
        $stmt->bindParam(":is_super_perrun", $this->is_super_perrun);
        $stmt->bindParam(":is_pelo_a_pelo", $this->is_pelo_a_pelo);
        $stmt->bindParam(":seller_accepted", $this->seller_accepted);
        $stmt->bindParam(":buyer_accepted", $this->buyer_accepted);
        $stmt->bindParam(":added_top_points", $this->added_top_points);
        $stmt->bindParam(":added_perrunflas_points", $this->added_perrunflas_points);
        
        if($is_engaged_set)
        {
            $this->engaged_to=htmlspecialchars(strip_tags($this->engaged_to));
            $stmt->bindParam(":engaged_to", $this->engaged_to);
        }


        // execute query
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }
        return -1;
         
    }



    // create Traspaso
    function delete()
    {
        $queryCargar = "SELECT * FROM intercambios WHERE engaged_to = :engaged_to;";
        $stmtCargar = $this->conn->prepare($queryCargar);
     
        $this->engaged_to=htmlspecialchars(strip_tags($this->engaged_to));
        
        // bind values
        $stmtCargar->bindParam(":engaged_to", $this->engaged_to);
        
        if($stmtCargar->execute()) // cargo todos los intercambios a borrar.
        {

            $queryDesenganchar = "
                UPDATE intercambios SET engaged_to = null WHERE engaged_to = :engaged_to";
            $stmtDesenganchar = $this->conn->prepare($queryDesenganchar);
         
            $this->engaged_to=htmlspecialchars(strip_tags($this->engaged_to));
            
            // bind values
            $stmtDesenganchar->bindParam(":engaged_to", $this->engaged_to);
            

            // execute query
            if($stmtDesenganchar->execute()) // desengancho a todos de los engage_to
            {
                $fallaron = false;
                while ($rowBorrar = $stmtCargar->fetch(PDO::FETCH_ASSOC))
                {
                    extract($rowBorrar);

                    $queryBorrar = "
                        DELETE FROM intercambios
                            WHERE id = :id;
                    ";
                    $stmtBorrar = $this->conn->prepare($queryBorrar);
                    
                    // error_log("borrando: " . $id);

                    $id=htmlspecialchars(strip_tags($id));
                    
                    // bind values
                    $stmtBorrar->bindParam(":id", $id);
                    

                    $fallaron = !$stmtBorrar->execute();
                }
                return !$fallaron; 
            }
            return false;
        }
        else
        {
            return false;
        }
         
    }
}
?>