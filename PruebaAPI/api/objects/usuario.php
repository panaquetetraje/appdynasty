<?php
class Usuario{
 
    // database connection and table name
    private $conn;
    private $table_name = "usuarios";
 
    // object properties
    public $name;
    public $pass;
    public $usuarioPsn;
    public $canalYoutube;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read_logged_user_data(){
     
        // select all query
        $query = "SELECT u.*, t.name as team_name, t.id as team_id, t.top_budget, t.perrunflas_budget FROM `usuarios` u, equipos t WHERE u.name =:name AND u.name = t.owner";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        
        // bind values
        $stmt->bindParam(":name", $this->name);
        
        // execute query
        $stmt->execute();
        
        return $stmt;
    }

    // read Usuarios
    function read(){
     
        // select all query
        $query = "SELECT * FROM
                    " . $this->table_name . " u
                    WHERE is_admin = 0";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
        
        return $stmt;
    }    

    // read Usuarios
    function read_one(){
     
        // select all query
        $query = "SELECT * FROM
                    " . $this->table_name . " u
                    WHERE name =:name";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        
        // bind values
        $stmt->bindParam(":name", $this->name);
        
        // execute query
        if($stmt->execute())
        {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->name = $row["name"];
            $this->pass = $row["pass"];
            $this->is_admin = $row["is_admin"];
        }
        
        return $stmt;
    }

    // create Usuario
    function create(){
     
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    name=:name, pass=:pass, is_admin=:is_admin";
                    
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->pass=htmlspecialchars(strip_tags($this->pass));
        $this->is_admin=htmlspecialchars(strip_tags($this->is_admin));
        
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":pass", $this->pass);
        $stmt->bindParam(":is_admin", $this->is_admin);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
    
    function login(){
        
        // select all query
        $query = "SELECT *
                FROM " . $this->table_name . " u
                WHERE
                    u.name = ?
                    AND u.pass = ?
                LIMIT
                    0,1";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        
        // bind name of Usuario to be updated
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->pass);
        

        $stmt->execute();
        
        return $stmt;
    }
        

    // update the Usuario
    function update($nuevoNombre, $nuevaClave, $nuevoUsuarioPSN, $nuevoCanalYoutube){
     
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :new_name,
                    pass = :pass,
                    usuario_psn = :usuario_psn,
                    canal_youtube = :canal_youtube
                WHERE
                    name = :name";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $nuevoNombre=htmlspecialchars(strip_tags($nuevoNombre));
        $nuevaClave=htmlspecialchars(strip_tags($nuevaClave));
        $nuevoUsuarioPSN=htmlspecialchars(strip_tags($nuevoUsuarioPSN));
        $nuevoCanalYoutube=htmlspecialchars(strip_tags($nuevoCanalYoutube));
        $this->name=htmlspecialchars(strip_tags($this->name));
        
        // bind new values
        $stmt->bindParam(':new_name', $nuevoNombre);
        $stmt->bindParam(':pass', $nuevaClave);
        $stmt->bindParam(':usuario_psn', $nuevoUsuarioPSN);
        $stmt->bindParam(':canal_youtube', $nuevoCanalYoutube);
        $stmt->bindParam(':name', $this->name);
     
        // execute the query
        if($stmt->execute()){
            $this->name = $nuevoNombre;
            $this->pass = $nuevaClave;
            $this->usuario_psn = $nuevoUsuarioPSN;
            $this->canal_youtube = $nuevoCanalYoutube;
            return true;
        }
     
        return false;
    }    
    /*
    // delete the Usuario
    function delete(){
     
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE name = ?";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
     
        // bind name of record to delete
        $stmt->bindParam(1, $this->name);
     
        // execute query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
    // search Usuarios
    function search($keywords){
     
        // select all query
        $query = "SELECT
                    c.name as category_name, p.name, p.name, p.valor_de_compra, p.pass, p.name, p.created
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categories c
                            ON p.name = c.name
                WHERE
                    p.name LIKE ? OR p.valor_de_compra LIKE ? OR c.name LIKE ?
                ORDER BY
                    p.created DESC";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
     
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    // read Usuarios with pagination
    public function readPaging($from_record_num, $records_per_page){
     
        // select query
        $query = "SELECT
                    c.name as category_name, p.name, p.name, p.valor_de_compra, p.pass, p.name, p.created
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categories c
                            ON p.name = c.name
                ORDER BY p.created DESC
                LIMIT ?, ?";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
     
        // execute query
        $stmt->execute();
     
        // return values from database
        return $stmt;
    }
    // used for paging Usuarios
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        return $row['total_rows'];
    }
    */
}
?>