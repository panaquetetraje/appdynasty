<?php
include_once '../config/database.php';
include_once '../datatypes/DtPartido.php';
include_once '../datatypes/DtEtapa.php';

class PartidosController
{
 
    // database connection and table name
    private $conn;
    private $nombresEtapas;
    private $etapas;
    static private $instance /*PartidosController*/;
    const API_KEY = 'sdlC6m7rb2HOcgmolcD_Aj_VTsX6eZC8dh9hK9ymJIw';
    const CLIENT_ID = '5301e38e89672894955551b125f5wwerlm74o4skkg404g800cowwgwcc4c08sswo84wsogo4c';
    const CLIENT_SECRET = '1knp5i5ja5twkgo88wg4go8k4swgc4kw4wg84sog4s84g8g8ks';

    // constructor with $db as database connection
    private function __construct()
    {
        // instantiate database and equipo object
        $database = new Database();
        $this->conn = $database->getConnection();
        $this->nombresEtapas = array();
        $this->etapas = array();
    }

    public static function getInstance()
    {
        if(!isset($instance))
        {
            $instance = new PartidosController();
        }
        return $instance;
    }

    public function getLinkDePartido($dtPartido)
    {
      return "https://www.toornament.com/es/tournaments/{$dtPartido->getIdCampeonato()}/matches/{$dtPartido->getId()}/";
    }

    /*
    * Asigna el resultado para un partido en el campeonato actual.
    */
    public function setMatchResult($idEquipoIngresandoResultado, $idPartido, $resLocal, $resVisitante) /*:boolean*/
    {
        $idCampeonato = $this->obtenerIdUltimoCampeonatoEnToornament();
        $tokenAutorizacion = $this->obtenerAutorizacionDeOrganizadorDeToornament();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/matches/{$idPartido}/games/1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PATCH",
          CURLOPT_POSTFIELDS =>"{\r\n    \"opponents\": [\r\n        {\r\n            \"number\": 1,\r\n            \"forfeit\": false,\r\n            \"score\": {$resLocal}\r\n        },\r\n        {\r\n            \"number\": 2,\r\n            \"forfeit\": false,\r\n            \"score\": {$resVisitante}\r\n        }\r\n    ]\r\n}",
          CURLOPT_HTTPHEADER => array(
            "X-Api-Key: " . self::API_KEY,
            "Authorization: {$tokenAutorizacion}",
            "Content-Type: text/plain"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $this->registrarIngresoDeResultado($idEquipoIngresandoResultado, $idPartido, $resLocal, $resVisitante);
    }

    private function registrarIngresoDeResultado($idEquipoIngresandoResultado, $idPartido, $resLocal, $resVisitante)
    {
        $query = "
            INSERT INTO log_ingresos_de_resultados (id_equipo, id_partido, res_local, res_visitante, fecha)
            VALUES({$idEquipoIngresandoResultado}, $idPartido, $resLocal, $resVisitante, CURRENT_TIMESTAMP)
            ;
        ";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // execute query
        return $stmt->execute();
    }

    /**
    * Devuelve un array de DtPartido's con la información de los próximos
    * partidos del equipo con la ID recibida.
    */
    public function readNextMatchesForTeam($teamId, $cantPartidosACargar = 3) /*:array<DtPartido>*/
    {
        $dtPartidos = array();

        $idToornamentEquipo = $this->obtenerIdEquipoEnToornamentEnUltimoCampeonato($teamId);
        $idCampeonato = $this->obtenerIdUltimoCampeonatoEnToornament();
        $tokenAutorizacion = $this->obtenerAutorizacionDeOrganizadorDeToornament();

        
        $i = 0;
        $matches = array();
        do
        {
          $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/matches?participant_ids={$idToornamentEquipo}&statuses=pending",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "X-Api-Key: " . self::API_KEY,
            "Range: matches=0-100",
            "Authorization: {$tokenAutorizacion}"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);


          // echo "response: \n";
          // var_dump($response);
          // echo "\n";
          // echo "\n";
          $matchesCargados = json_decode($response, true);
          if(is_array($matchesCargados))
            $matches = array_merge($matches, $matchesCargados);
          $i += 100;
        } while(is_array($matchesCargados) && count($matchesCargados) > 0);


        foreach ($matches as $i => $match) 
        {
            $etapa = $this->obtenerNombreEtapa($idCampeonato, $match["stage_id"], $tokenAutorizacion);
            $numFecha = $this->obtenerNumeroDeAsalto($idCampeonato, $match["round_id"], $match["stage_id"], $tokenAutorizacion);
            $fecha = "Fecha {$numFecha} de {$etapa}";

            $nombreLocal = $match['opponents'][0]['participant']['name'];
            $nombreVisitante = $match['opponents'][1]['participant']['name'];
            $resLocal = -1; // Está pendiente el partido
            $resVisitante = -1; // Está pendiente el partido
            array_push($dtPartidos, new DtPartido($match['id'], $fecha, $nombreLocal, $nombreVisitante, $resLocal, $resVisitante, $idCampeonato));
        }




        return $dtPartidos;
    }



    private function obtenerNumeroDeAsalto($idCampeonato, $idAsalto, $idEtapa, $tokenAutorizacion)
    {
        // echo "<br/>";
        // echo "obtenerNumeroDeAsalto idCampeonato: $idCampeonato, idAsalto: $idAsalto, idEtapa: $idEtapa, tokenAutorizacion: $tokenAutorizacion";
        // echo "<br/>";
        if(!isset($this->etapas[$idEtapa]))
        {
            // echo "<br/>";
            // echo "Entró al if, no tenía cargada la etapa.";
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/rounds?stage_ids={$idEtapa}",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "X-Api-Key: " . self::API_KEY,
                "Authorization: {$tokenAutorizacion}",
                "Range: rounds=0-49"
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);


            $resArray = json_decode($response, true);
            // echo "<br/>response: $response";
            // echo "<br/>";
            // echo "<br/>resArray: $resArray";
            $this->etapas[$idEtapa] = $resArray;
        }

        // echo "<br/>";
        // echo "this->etapas[idEtapa] quedó así";
        // var_dump($this->etapas[$idEtapa]);
        // echo "<br/>";

        $numAsalto = -1;
        $i = 0;
        while (isset($this->etapas[$idEtapa][$i]) 
            && $this->etapas[$idEtapa][$i] != null 
            && $idAsalto != $this->etapas[$idEtapa][$i]['id'] // Entro mientras no lo encuentre
        ) 
        {
            $i++;
        }
        return $this->etapas[$idEtapa][$i]['number'];
    }

    private function obtenerNombreEtapa($idCampeonato, $idEtapa, $tokenAutorizacion)
    {
        // echo "<br/>Cargando nombre etapa: " . $idEtapa;
        if(!isset($this->nombresEtapas[$idEtapa])) // Sólo traigo el nombre de la etapa de la API si no lo cargué antes.
        {
            // echo "<br/>Cargando desde la API<br/>";
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/stages/{$idEtapa}",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "X-Api-Key: " . self::API_KEY,
                "Authorization: {$tokenAutorizacion}"
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $resArray = json_decode($response, true);

            $this->nombresEtapas[$idEtapa] = $resArray["name"];
        }

        // echo "<br/>Nombre etapa: {$this->nombresEtapas[$idEtapa]}<br/>";
        return $this->nombresEtapas[$idEtapa];
    }

    private function obtenerAutorizacionDeOrganizadorDeToornament($scope = "result")
    {
        
        $curl = curl_init();


        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.toornament.com/oauth/v2/token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials&client_id=" . self::CLIENT_ID . "&client_secret=" . self::CLIENT_SECRET . "&scope=organizer%3A{$scope}",
          CURLOPT_HTTPHEADER => array(
            "X-Api-Key: " . self::API_KEY,
            "Content-Type: application/x-www-form-urlencoded"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $resArray = json_decode($response, true);
        return $resArray["access_token"];
    }

    private function obtenerIdUltimoCampeonatoEnToornament()
    {
        $query = "
            SELECT ct.id as idCampeonato 
            FROM campeonatos_toornament ct
            ORDER BY ct.fecha_fin DESC
            LIMIT 1
            ;
        ";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // execute query
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['idCampeonato'];
    }

    private function obtenerIdEquipoEnToornamentEnUltimoCampeonato($teamId)
    {
        // Busco el id del equipo en el último campeonato de toornament registrado.
        $query = "
            SELECT eet.id_toornament_equipo as idToornamentEquipo
            FROM equipos_en_campeonatos_toornament eet
            INNER JOIN campeonatos_toornament ct ON ct.id = eet.id_campeonato
            WHERE eet.id_dynasty_equipo = {$teamId}
            ORDER BY ct.fecha_fin DESC
            LIMIT 1
            ;
        ";
        
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // execute query
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['idToornamentEquipo'];
    }

    public function listarCampeonatosVigentes()
    {
      // Busco el id del equipo en el último campeonato de toornament registrado.
      $query = "
        SELECT *
        FROM campeonatos_toornament;
      ";
      
      // prepare query statement
      $stmt = $this->conn->prepare($query);
      
      // execute query
      $stmt->execute();
      
      $campeonatos = array();
      while($row = $stmt->fetch(PDO::FETCH_ASSOC))
      {
        $campeonato = array();
        $campeonato['id']           = $row["id"];
        $campeonato['nombre']       = $row["nombre"];
        $campeonato['fecha_inicio'] = $row["fecha_inicio"];
        $campeonato['fecha_fin'] = $row["fecha_fin"];
        if(strtotime("now") < strtotime($campeonato['fecha_fin'])
          && strtotime("now") > strtotime($campeonato['fecha_inicio']))
        {
          array_push($campeonatos, $campeonato);
        }
      }

      return $campeonatos;
    }



    /**
    * Devuelve un array de DtEtapa's con la información de las diferentes etapas
    * de un campeonato
    */
    public function listarEtapasDeCampeonato($idCampeonato) /*:array<DtEtapa>*/
    {
        $dtEtapas = array();
        
        $tokenAutorizacion = $this->obtenerAutorizacionDeOrganizadorDeToornament();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/stages",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "X-Api-Key: " . self::API_KEY,
            "Authorization: {$tokenAutorizacion}"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);


        // echo "response: \n";
        // var_dump($response);
        // echo "\n";
        // echo "\n";
        $stages = json_decode($response, true);
        // echo "\n";
        // var_dump($stages);
        // echo "\n";
        foreach ($stages as $i => $stage) 
        {
          // echo "\n";
          array_push($dtEtapas, new DtEtapa($stage['id'], $stage['name']));
        }




        return $dtEtapas;
    }

    public function ingresarNuevaHabilitacion($idCampeonato, $idEtapa, $fechaAHabilitar, $diaHabilitacion)
    {
      $query = "
        DELETE FROM habilitaciones_toornament
        WHERE id_campeonato = :id_campeonato
          AND id_etapa = :id_etapa
          AND fecha_habilitada = :fecha_a_habilitar
      ";
      $stmt = $this->conn->prepare($query);
      $idCampeonato=htmlspecialchars(strip_tags($idCampeonato));
      $stmt->bindParam(":id_campeonato", $idCampeonato);
      $idEtapa=htmlspecialchars(strip_tags($idEtapa));
      $stmt->bindParam(":id_etapa", $idEtapa);
      $fechaAHabilitar=htmlspecialchars(strip_tags($fechaAHabilitar));
      $stmt->bindParam(":fecha_a_habilitar", $fechaAHabilitar);
        
      // execute query
      $stmt->execute();

      // Busco el id del equipo en el último campeonato de toornament registrado.
      $query = "
        INSERT INTO habilitaciones_toornament (id_campeonato, id_etapa, fecha_habilitada, dia_habilitacion)
        VALUES(:id_campeonato, :id_etapa, :fecha_a_habilitar, :dia_habilitacion);
      ";
      
      // prepare query statement
      $stmt = $this->conn->prepare($query);
      $idCampeonato=htmlspecialchars(strip_tags($idCampeonato));
      $stmt->bindParam(":id_campeonato", $idCampeonato);
      $idEtapa=htmlspecialchars(strip_tags($idEtapa));
      $stmt->bindParam(":id_etapa", $idEtapa);
      $fechaAHabilitar=htmlspecialchars(strip_tags($fechaAHabilitar));
      $stmt->bindParam(":fecha_a_habilitar", $fechaAHabilitar);
      $diaHabilitacion=htmlspecialchars(strip_tags($diaHabilitacion));
      $stmt->bindParam(":dia_habilitacion", $diaHabilitacion);
        
      // execute query
      return $stmt->execute();
    }

    /**
    * Devuelve un array de DtPartido's con la información de los partidos
    * habilitados y pendientes del equipo con la ID recibida en la fecha recibida.
    */
    public function readMatchesForTeamAtDate($teamId, $fechaARevisar) /*:array<DtPartido>*/
    {
        $dtPartidos = array();

        $idToornamentEquipo = $this->obtenerIdEquipoEnToornamentEnUltimoCampeonato($teamId);
        $idCampeonato = $this->obtenerIdUltimoCampeonatoEnToornament();
        $tokenAutorizacion = $this->obtenerAutorizacionDeOrganizadorDeToornament();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/matches?participant_ids={$idToornamentEquipo}&statuses=pending",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "X-Api-Key: " . self::API_KEY,
            "Range: matches=0-100",
            "Authorization: {$tokenAutorizacion}"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);


        // echo "response: \n";
        // var_dump($response);
        // echo "\n";
        // echo "\n";
        $matches = json_decode($response, true);
        $etapaAnterior = 0;
        foreach ($matches as $i => $match) 
        {
          // echo "\n";
          // echo "\n";
          // echo "\n";
          // echo "\n";
          $numRonda = $this->obtenerNumeroDeAsalto($idCampeonato, $match["round_id"], $match["stage_id"], $tokenAutorizacion);
          // error_log("id_campeonato: {$idCampeonato}, id_etapa: {$match['stage_id']}, num_ronda: " . $numRonda);
          if($this->isRoundEnabledAtDate($idCampeonato, $match['stage_id'], $numRonda, $fechaARevisar))
          {
            $etapa = $this->obtenerNombreEtapa($idCampeonato, $match["stage_id"], $tokenAutorizacion);
            $fecha = "Fecha {$numRonda} de {$etapa}";

            $nombreLocal = $match['opponents'][0]['participant']['name'];
            $nombreVisitante = $match['opponents'][1]['participant']['name'];
            $resLocal = -1; // Está pendiente el partido
            $resVisitante = -1; // Está pendiente el partido
            array_push($dtPartidos, new DtPartido($match['id'], $fecha, $nombreLocal, $nombreVisitante, $resLocal, $resVisitante, $idCampeonato));
          }
          $etapaAnterior = $match['stage_id'];
        }

        return $dtPartidos;
    }

    /**
    * Devuelve un array de DtPartido's con la información de todos los partidos
    * habilitados y pendientes en la fecha recibida.
    */
    public function readPendingAndEnabledMatchesAtDate($fechaARevisar) /*:array<DtPartido>*/
    {
        $dtPartidos = array();

        $idCampeonato = $this->obtenerIdUltimoCampeonatoEnToornament();
        $tokenAutorizacion = $this->obtenerAutorizacionDeOrganizadorDeToornament();

        $i = 0;
        $matches = array();
        do
        {
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.toornament.com/organizer/v2/tournaments/{$idCampeonato}/matches?statuses=pending",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "X-Api-Key: " . self::API_KEY,
              "Range: matches=" . $i . "-" . ($i+99),
              "Authorization: {$tokenAutorizacion}"
            ),
          ));

          $response = curl_exec($curl);

          curl_close($curl);


          // echo "response: \n";
          // var_dump($response);
          // echo "\n";
          // echo "\n";
          $matchesCargados = json_decode($response, true);
          if(is_array($matchesCargados))
            $matches = array_merge($matches, $matchesCargados);
          $i += 100;
        } while(is_array($matchesCargados) && count($matchesCargados) > 0);
        
        $etapaAnterior = 0;
        foreach ($matches as $i => $match) 
        {
          // echo "\n";
          // echo "\n";
          // echo "\n";
          // echo "\n";
          $numRonda = $this->obtenerNumeroDeAsalto($idCampeonato, $match["round_id"], $match["stage_id"], $tokenAutorizacion);
          // error_log("id_campeonato: {$idCampeonato}, id_etapa: {$match['stage_id']}, num_ronda: " . $numRonda);
          if($this->isRoundEnabledAtDate($idCampeonato, $match['stage_id'], $numRonda, $fechaARevisar))
          {
            $etapa = $this->obtenerNombreEtapa($idCampeonato, $match["stage_id"], $tokenAutorizacion);
            $fecha = "Fecha {$numRonda} de {$etapa}";

            $nombreLocal = $match['opponents'][0]['participant']['name'];
            $nombreVisitante = $match['opponents'][1]['participant']['name'];
            $resLocal = -1; // Está pendiente el partido
            $resVisitante = -1; // Está pendiente el partido
            array_push($dtPartidos, new DtPartido($match['id'], $fecha, $nombreLocal, $nombreVisitante, $resLocal, $resVisitante, $idCampeonato));
          }
          $etapaAnterior = $match['stage_id'];
        }

        return $dtPartidos;
    }

    private function isRoundEnabledAtDate($idCampeonato, $idEtapa, $numRonda, $fecha)
    {
      $query = "
        SELECT dia_habilitacion 
        FROM habilitaciones_toornament
        WHERE id_campeonato = :id_campeonato
          AND id_etapa = :id_etapa
          AND fecha_habilitada = :numRonda
      ";
      $stmt = $this->conn->prepare($query);
      $idCampeonato = htmlspecialchars(strip_tags($idCampeonato));
      $stmt->bindParam(":id_campeonato", $idCampeonato);
      $idEtapa = htmlspecialchars(strip_tags($idEtapa));
      $stmt->bindParam(":id_etapa", $idEtapa);
      $numRonda = htmlspecialchars(strip_tags($numRonda));
      $stmt->bindParam(":numRonda", $numRonda);
        
      // execute query
      $stmt->execute();
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      // var_dump($row);
      if(!$row)
      {
        // No existe habilitación para la fecha seleccionada.
        return false;
      }
      else
      {
        // echo "strtotime(row['dia_habilitacion']) " . strtotime($row['dia_habilitacion']) . "dia_habilitacion: " . $row['dia_habilitacion'] . "\n";
        // echo "strtotime(fecha) " . strtotime($fecha) . "fecha: " . $fecha . "\n";
        return strtotime($row['dia_habilitacion']) <= strtotime($fecha);
      }
    }
}
?>