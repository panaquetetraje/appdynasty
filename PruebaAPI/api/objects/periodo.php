<?php
class Periodo
{
 
    // database connection and table name
    private $conn;
    
    // object properties
    public $id;
    public $fechaInicio;
    public $fechaFin;
            
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read Periodoes
    function read(){
        // select all query
        $query = "SELECT * FROM periodos_traspasos;";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }

    function getProximoInicio()
    {
        // select all query
        $query = "SELECT min(fecha_inicio) as proximo_inicio FROM periodos_traspasos
            WHERE fecha_inicio > NOW();";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // error_log("row[proximo_inicio]");
        // error_log($row["proximo_inicio"]);
        return $row["proximo_inicio"];
    }

    function getProximoCierre()
    {
        // select all query
        $query = "SELECT min(fecha_fin) as proximo_cierre FROM periodos_traspasos
            WHERE fecha_fin > NOW();";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // error_log("row[proximo_cierre]");
        // error_log($row["proximo_cierre"]);
        return $row["proximo_cierre"];
    }

    function getActualmenteEnPeriodo()
    {
        // select all query
        $query = "SELECT * FROM periodos_traspasos
            WHERE fecha_inicio < NOW()
            AND fecha_fin > NOW();";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
        
        return ($stmt->rowCount() > 0);
    }
    
}
?>