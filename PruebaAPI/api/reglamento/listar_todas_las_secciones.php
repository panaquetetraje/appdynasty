<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: GET");
    header("Access-Control-Allow-Credentials: true");
    header('Content-Type: application/json');
     
    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/ReglamentoController.php';
     
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
     
    $dtSecciones = ReglamentoController::getInstance()->listarTodasLasSecciones();
    if(is_array($dtSecciones)){
        // set response code - 200 OK
        $secciones = array();
        foreach ($dtSecciones as $seccion) 
        {
            array_push($secciones, [
                "indice" => $seccion->getIndice(),
                "titulo" => $seccion->getTitulo(),
                "contenido" => $seccion->getContenido()
            ]);
        }
        
        http_response_code(200);
        // make it json format
        echo json_encode($secciones);
    }
    else
    {
        // set response code 
        http_response_code(400);
     
        // tell the user jugador does not exist
        echo json_encode(array("message" => "No se pudieron listar las secciones."));
    }
?>