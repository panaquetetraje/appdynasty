<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
include_once '../objects/ReglamentoController.php';
 
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

    
// make sure data is not empty
if(
    !empty($data->indice) &&
    !empty($data->titulo)
){
    if(ReglamentoController::getInstance()->actualizarSeccion($data->indice, $data->titulo, $data->contenido)){
        http_response_code(200);
        echo json_encode(array("message" => "Sección actualizada"));
    }
    else
    {
        http_response_code(500);
        echo json_encode(array("message" => "Error: Sección no actualizada"));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Datos incorrectos"));
}
?>