<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/equipo.php';
 
// instantiate database and equipo object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$equipo = new Equipo($db);
 
// query equipos
$stmt = $equipo->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // equipos array
    $equipos_arr=array();
    $equipos_arr["records"]=array();
    //$equipos_arr["asignados"]=array();
    //$equipos_arr["libres"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $equipo_item=array(
            "id" => $id,
            "name" => $name,
            "top_budget" => $top_budget,
            "perrunflas_budget" => $perrunflas_budget,
            "logo" => $logo,
            "sofifa_team_uri" => $sofifa_team_uri
        );

       /* if (strpos($name, 'Fylyal') !== false) {
           $equipo_item_libre=array(
                "id" => $id,
                "name" => $name,
                "top_budget" => $top_budget,
                "perrunflas_budget" => $perrunflas_budget,
                "owner" => $owner
            ); 
        }*/

        array_push($equipos_arr["records"], $equipo_item);
        //array_push($equipos_arr["asignados"], $equipo_item_libre);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show equipos data in json format
    echo json_encode($equipos_arr);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no equipos found
    echo json_encode(
        array("message" => "No equipos found.")
    );
}
?>