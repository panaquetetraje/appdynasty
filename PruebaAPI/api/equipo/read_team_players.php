<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/equipo.php';
 
// instantiate database and equipo object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$equipo = new Equipo($db);

$data = json_decode(file_get_contents("php://input"));
$equipo->id = $data->id;

// query equipos
$stmt = $equipo->read_team_players();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // equipos array
    $equipos_arr=array();
    $equipos_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $equipo_item=array(
            "id" => $id,
            "name" => $name,
            "overall_rating" => max($overall_rating, Database::VALOR_MÍNIMO_TRASPASOS), 
            "is_top" => $is_top,
            "team_id" => ($team_id!=null)?$team_id:0,
            "team_name" => ($team_name!=null)?$team_name:"Sin equipo",
            "price_paid" => $price_paid
        );
 
        array_push($equipos_arr["records"], $equipo_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show equipos data in json format
    echo json_encode($equipos_arr);
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no equipos found
    echo json_encode(
        array("message" => "No jugadores found in equipo.")
    );
}
?>