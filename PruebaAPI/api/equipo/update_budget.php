<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/equipo.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare equipo object
$equipo = new Equipo($db);
 
// get id of equipo to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set ID property of equipo's budget to be edited
$equipo->id = $data->id;
$equipo->top_budget = $data->presupuesto_top;
$equipo->perrunflas_budget = $data->presupuesto_perrunfla;
 
/*
error_log("Updating: ");
error_log($equipo->id);
error_log($equipo->top_budget);
error_log($equipo->perrunflas_budget);
*/
// update the equipo's budget
if($equipo->update_budgets()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "Equipo's budgets were updated."));
}
 
// if unable to update the equipo's budget, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update equipo's budgets."));
}
?>