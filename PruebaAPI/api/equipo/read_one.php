<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/equipo.php';
 
// instantiate database and equipo object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$equipo = new Equipo($db);

$equipo->id = isset($_GET['id']) ? $_GET['id'] : die();

// query equipos
$res = $equipo->read_one();
 
// check if more than 0 record found
if($res){

    $suma_tops = $equipo->get_suma_jugadores(1);
    $resto_tops = $equipo->top_budget - $suma_tops;
    $suma_perrunflas = $equipo->get_suma_jugadores(0);
    $resto_perrunflas = $equipo->perrunflas_budget - $suma_perrunflas;

    $equipo_item=array(
        "id" => $equipo->id,
        "name" => $equipo->name,
        "top_budget" => $equipo->top_budget,
        "perrunflas_budget" => $equipo->perrunflas_budget,
        "suma_tops" => $suma_tops,
        "suma_real_tops" => $equipo->sumaRealTops,
        "resto_tops" => $resto_tops,
        "suma_perrunflas" => $suma_perrunflas,
        "suma_real_perrunflas" => $equipo->sumaRealPerrunflas,
        "resto_perrunflas" => $resto_perrunflas,
        "owner" => $equipo->owner,
        "fylyal_id" => $equipo->fylyal_id,
        "fylyal_name" => $equipo->fylyal_name,
        "logo" => $equipo->logo,
        "sofifa_team_uri" => $equipo->sofifa_team_uri,
        "canal_youtube" => $equipo->canal_youtube,
        "usuario_psn" => $equipo->usuario_psn
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show equipos data in json format
    echo json_encode(array("team" => $equipo_item));
}
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no equipos found
    echo json_encode(
        array("message" => "No se encontraron equipos.")
    );
}
?>