<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate equipo object
include_once '../objects/equipo.php';
 
$database = new Database();
$db = $database->getConnection();
 
$equipo = new Equipo($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

    
// make sure data is not empty
if(
    !empty($data->name) &&
    !empty($data->top_budget) &&
    !empty($data->perrunflas_budget) &&
    !empty($data->owner)
){
 
    // set equipo property values
    $equipo->name = $data->name;
    $equipo->top_budget = $data->top_budget;
    $equipo->perrunflas_budget = $data->perrunflas_budget;
    $equipo->owner = $data->owner;

    // create the equipo
    if($equipo->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "equipo was created."));
    }
 
    // if unable to create the equipo, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create equipo."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create equipo. Data is incomplete." . $data->name . " / " . $data->top_budget . " / " . $data->perrunflas_budget . " / " . $data->owner));
}
?>