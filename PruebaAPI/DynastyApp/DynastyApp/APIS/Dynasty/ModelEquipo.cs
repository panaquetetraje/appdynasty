﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynastyApp.APIS.Dynasty
{
    public class ModelEquipo
    {
        public int id { get; set; }
        public string name { get; set; }
        public float top_budget { get; set; }
        public float perrunflas_budget { get; set; }
        public string owner { get; set; }
        public override string ToString()
        {
            return name;
        }
        public string logo { get; set; }
        public string sofifa_team_uri { get; set; }
    }
}
