﻿using DynastyApp.APIS.Dynasty;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DynastyApp.APIS
{
    public class ApiDynasty
    {
        private const string API_DYNASTY_URL_LOGIN                                  = "https://dynastyfobal.000webhostapp.com/api/usuario/login.php";
        private const string API_DYNASTY_URL_READ_LOGGED_USER_DATA                  = "https://dynastyfobal.000webhostapp.com/api/usuario/read_logged_user_data.php";
        private const string API_DYNASTY_URL_READ_TEAM_PLAYERS                      = "https://dynastyfobal.000webhostapp.com/api/equipo/read_team_players.php";
        private const string API_DYNASTY_URL_SEARCH_PLAYERS                         = "https://dynastyfobal.000webhostapp.com/api/jugador/search.php";
        private const string API_DYNASTY_URL_READ_MATCHES_FOR_TEAM_AT_DATE          = "https://dynastyfobal.000webhostapp.com/api/partidos/read_matches_for_team_at_date.php";
        private const string API_DYNASTY_URL_READ_SECCIONES_REGLAMENTO              = "https://dynastyfobal.000webhostapp.com/api/reglamento/listar_todas_las_secciones.php";
        private const string API_DYNASTY_URL_READ_TEAMS                             = "https://dynastyfobal.000webhostapp.com/api/equipo/read.php";
        private const string API_DYNASTY_URL_UPDATE_USER_DATA                       = "https://dynastyfobal.000webhostapp.com/api/usuario/update_data.php";
        private const string API_DYNASTY_URL_SET_MATCH_RESULT                       = "https://dynastyfobal.000webhostapp.com/api/partidos/set_match_result.php";

        private static HttpClient client;
        private static bool inicializada = false;
        public static void Inicializar()
        {
            if(!inicializada)
            {
                inicializada = true;
                client = new HttpClient();
            }
        }

        public static async Task<ModelListaDeEquipos> cargarEquipos()
        {
            Console.WriteLine("cargarEquipos-------------: ");

            ModelListaDeEquipos post = null;
            try
            {
                HttpResponseMessage response = await client.GetAsync(API_DYNASTY_URL_READ_TEAMS);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                post = JsonConvert.DeserializeObject<ModelListaDeEquipos>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public static async Task<ObservableCollection<ModelSeccion>> cargarSeccionesReglamento()
        {
            ObservableCollection<ModelSeccion> post = null;
            string url = API_DYNASTY_URL_READ_SECCIONES_REGLAMENTO;
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                Console.WriteLine("result: " + result);
                post = JsonConvert.DeserializeObject<ObservableCollection<ModelSeccion>>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public static async Task<ObservableCollection<ModelPartido>> cargarPartidosParaEquipoEnFecha(int id, DateTime dateTime)
        {
            ObservableCollection<ModelPartido> post = null;
            Console.WriteLine("Fecha: " + dateTime.ToString());
            string url = API_DYNASTY_URL_READ_MATCHES_FOR_TEAM_AT_DATE + "?team_id=" + id + "&date=" + dateTime.ToString("yyyy-MM-dd");
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                /*
                 * Cambio los 1 y 0 por true y false cuando espero booleanos y por 0 cuando espero enteros.
                 
                result = result.Replace("\"is_top\":\"1\"", "\"is_top\":true");
                result = result.Replace("\"is_top\":\"0\"", "\"is_top\":false");
                result = result.Replace("\"is_top\":null", "\"is_top\":false");
                result = result.Replace("\"team_id\":null", "\"team_id\":0");
                result = result.Replace("\"team_name\":null", "\"team_name\":\"Mercado Asiático\"");
                result = result.Replace("\"price_paid\":null", "\"price_paid\":0");
                */
                Console.WriteLine("result: " + result);
                post = JsonConvert.DeserializeObject<ObservableCollection<ModelPartido>>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public static async Task<ModelOkOrError> cargarResultadoPartido(ModelPartido datosPartido, int idEquipoIngresandoResultado)
        {
            Dictionary<string, string> user_data = new Dictionary<string, string>
            {
                { "idEquipoIngresandoResultado", idEquipoIngresandoResultado.ToString() },
                { "idPartido", datosPartido.id.ToString() },
                { "resLocal", datosPartido.res_local.ToString() },
                { "resVisitante", datosPartido.res_visitante.ToString() }

            };

            string json = JsonConvert.SerializeObject(user_data, Formatting.Indented);
            ModelOkOrError post = null;
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(API_DYNASTY_URL_SET_MATCH_RESULT, content);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                post = JsonConvert.DeserializeObject<ModelOkOrError>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public static async Task<bool> Login(string Nombre, string Clave)
        {
            Console.WriteLine("LOGIN-------------");
            Dictionary<string, string> credenciales = new Dictionary<string, string>
            {
                { "name", Nombre },
                { "pass", Clave }
            };

            string json = JsonConvert.SerializeObject(credenciales, Formatting.Indented);
            Console.WriteLine("Login: " + json);
            ModelLogin post = null;
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(API_DYNASTY_URL_LOGIN, content);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                post = JsonConvert.DeserializeObject<ModelLogin>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post != null && post.result;
        }

        public static async Task<bool> ActualizarDatosUsuario(string nombreUsuarioAnterior, string nombreUsuario, string clave, string nombreEquipo, 
            string urlLogoEquipo, string uriSofifaTeam, string usuarioPSN, string canalYoutube)
        {
            Dictionary<string, string> user_data = new Dictionary<string, string>
            {
                { "previous_user_name", nombreUsuarioAnterior },
                { "user_name", nombreUsuario },
                { "pass", clave },
                { "team_name", nombreEquipo },
                { "url_logo_equipo", urlLogoEquipo },
                { "uri_sofifa_team", uriSofifaTeam },
                { "usuario_psn", usuarioPSN },
                { "canal_youtube", canalYoutube }

            };

            string json = JsonConvert.SerializeObject(user_data, Formatting.Indented);
            ModelOkOrError post = null;
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(API_DYNASTY_URL_UPDATE_USER_DATA, content);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                post = JsonConvert.DeserializeObject<ModelOkOrError>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post != null && !post.dioError;
        }

        public static async Task<ModelListaDeJugadores> buscarJugadores(string textoBusqueda)
        {
            Console.WriteLine("buscarJugadores-------------: " + textoBusqueda);

            ModelListaDeJugadores post = null;
            string url = API_DYNASTY_URL_SEARCH_PLAYERS + "?s=" + textoBusqueda;
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                /*
                 * Cambio los 1 y 0 por true y false cuando espero booleanos y por 0 cuando espero enteros.
                 */
                result = result.Replace("\"is_top\":\"1\"", "\"is_top\":true");
                result = result.Replace("\"is_top\":\"0\"", "\"is_top\":false");
                result = result.Replace("\"is_top\":null", "\"is_top\":false");
                result = result.Replace("\"team_id\":null", "\"team_id\":0");
                result = result.Replace("\"team_name\":null", "\"team_name\":\"Mercado Asiático\"");
                result = result.Replace("\"price_paid\":null", "\"price_paid\":0");
                Console.WriteLine("result: " + result);
                post = JsonConvert.DeserializeObject<ModelListaDeJugadores>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public async static Task<ModelDatosUsuario> cargarDatosDeUsuario(string NombreUsuario)
        {
            Console.WriteLine("cargarDatosDeUsuario-------------: " + NombreUsuario);
            Dictionary<string, string> credenciales = new Dictionary<string, string>
            {
                { "name", NombreUsuario }
            };

            string json = JsonConvert.SerializeObject(credenciales, Formatting.Indented);
            Console.WriteLine("cargarDatosDeUsuario: " + json);
            ModelDatosUsuario post = null;
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(API_DYNASTY_URL_READ_LOGGED_USER_DATA, content);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                post = JsonConvert.DeserializeObject<ModelDatosUsuario>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }

        public async static Task<ModelListaDeJugadores> cargarJugadoresDeEquipo(int idEquipoUsuarioLogueado)
        {
            Console.WriteLine("cargarJugadoresDeEquipo-------------: " + idEquipoUsuarioLogueado);
            Dictionary<string, string> credenciales = new Dictionary<string, string>
            {
                { "id", idEquipoUsuarioLogueado.ToString() }
            };

            string json = JsonConvert.SerializeObject(credenciales, Formatting.Indented);
            Console.WriteLine("cargarJugadoresDeEquipo: " + json);
            ModelListaDeJugadores post = null;
            try
            {
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(API_DYNASTY_URL_READ_TEAM_PLAYERS, content);
                Console.WriteLine("Response: " + response);
                var result = await response.Content.ReadAsStringAsync();
                /*
                 * Cambio los 1 y 0 por true y false cuando espero booleanos.
                 */
                result = result.Replace("\"is_top\":\"1\"", "\"is_top\":true");
                result = result.Replace("\"is_top\":\"0\"", "\"is_top\":false");
                post = JsonConvert.DeserializeObject<ModelListaDeJugadores>(result);
                Console.WriteLine("post: " + post.ToString());
            }
            catch (Exception er)
            {
                var lb = er.ToString();
                Console.WriteLine("-----> LB " + lb);
            }
            return post;
        }
    }
}
