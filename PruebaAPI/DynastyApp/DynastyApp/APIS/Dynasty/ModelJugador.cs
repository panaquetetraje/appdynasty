﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynastyApp.APIS.Dynasty
{
    public class ModelJugador
    {
        public int id { get; set; }
        public string name { get; set; }
        public int overall_rating { get; set; }
        public bool is_top { get; set; }
        public int team_id { get; set; }
        public string team_name { get; set; }
        public int price_paid { get; set; }

        public string uriImagen 
        {
            get { return "https://www.fifaindex.com/static/FIFA20/images/players/10/" + id.ToString() + ".webp"; }
        }
        public string uriSofifa
        {
            get { return "https://sofifa.com/player/" + id.ToString(); }
        }
        public override string ToString()
        {
            return name + " (" + overall_rating + ")";
        }
    }
}
