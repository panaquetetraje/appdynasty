﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynastyApp.APIS.Dynasty
{
    class ModelLogin
    {
        public bool result { get; set; }
        public string name { get; set; }
        public bool is_admin { get; set; }
    }
}
