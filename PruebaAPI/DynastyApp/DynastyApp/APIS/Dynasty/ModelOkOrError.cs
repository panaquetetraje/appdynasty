﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynastyApp.APIS.Dynasty
{
    public class ModelOkOrError
    {
        public string result { get; set; }
        public bool dioError => result != "ok";

        public string message { get; set; }

    }
}
