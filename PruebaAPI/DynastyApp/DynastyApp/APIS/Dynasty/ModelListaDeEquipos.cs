﻿using DynastyApp.APIS.Dynasty;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DynastyApp.APIS
{
    public class ModelListaDeEquipos
    {
        public ObservableCollection<ModelEquipo> records { get; set; }
    }
}
