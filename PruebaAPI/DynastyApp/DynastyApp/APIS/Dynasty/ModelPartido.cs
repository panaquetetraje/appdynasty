﻿using System;

public class ModelPartido
{
    public string id { get; set; }
    public string fecha { get; set; }
    public string nombre_local { get; set; }
    public string nombre_visitante { get; set; }
    public int res_local { get; set; }
    public int res_visitante { get; set; }
    public string id_campeonato { get; set; }
    public string link_partido { get; set; }
}