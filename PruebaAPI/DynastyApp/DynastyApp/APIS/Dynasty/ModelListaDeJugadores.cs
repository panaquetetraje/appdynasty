﻿using DynastyApp.APIS.Dynasty;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DynastyApp.APIS
{
    public class ModelListaDeJugadores
    {
        public ObservableCollection<ModelJugador> records { get; set; }
    }
}
