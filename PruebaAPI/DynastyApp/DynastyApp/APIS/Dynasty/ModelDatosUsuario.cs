﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynastyApp.APIS.Dynasty
{
    public class ModelDatosUsuario
    {
        public string name { get; set; }
        public string team_name { get; set; }
        public string fylyal_name { get; set; }
        public string fylyal_id { get; set; }
        public int team_id { get; set; }
        public float top_budget { get; set; }
        public float perrunflas_budget { get; set; }
        public int suma_tops { get; set; }
        public int suma_real_tops { get; set; }
        public int suma_perrunflas { get; set; }
        public int suma_real_perrunflas { get; set; }
        public string logo { get; set; }
        public string sofifa_team_uri { get; set; }
        public string canal_youtube { get; set; }
        public string usuario_psn { get; set; }
    }
}
