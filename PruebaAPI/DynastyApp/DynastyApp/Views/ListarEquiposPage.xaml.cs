﻿using DynastyApp.APIS.Dynasty;
using DynastyApp.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListarEquiposPage : ContentPage
    {
        public ListarEquiposPage()
        {
            this.BindingContext = new ListarEquiposViewModel();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            (BindingContext as ListarEquiposViewModel).cargarEquipos();
            base.OnAppearing();
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            ModelEquipo equipoSeleccionado = (ModelEquipo) e.Item;
            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            ((ListarEquiposViewModel)BindingContext).VerEquipo(equipoSeleccionado);
        }
    }
}
