﻿using DynastyApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerSeccionReglamentoPage : ContentPage
    {
        public VerSeccionReglamentoPage()
        {
            InitializeComponent();
            BindingContext = new VerSeccionReglamentoViewModel();
        }

        protected override void OnAppearing()
        {
            (BindingContext as VerSeccionReglamentoViewModel).actualizarSeccionMostrada();
            base.OnAppearing();
        }
    }
}