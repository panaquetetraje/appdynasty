﻿using DynastyApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerJugadorPage : ContentPage
    {
        public VerJugadorPage()
        {
            InitializeComponent();
            BindingContext = new VerJugadorViewModel();
        }

        protected override void OnAppearing()
        {
            (BindingContext as VerJugadorViewModel).actualizarJugadorMostrado();
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            Shell.Current.GoToAsync($"//{nameof(VerEquipoPage)}");
            return true;
        }
    }
}