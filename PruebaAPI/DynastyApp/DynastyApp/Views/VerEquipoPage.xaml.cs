﻿using DynastyApp.APIS.Dynasty;
using DynastyApp.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerEquipoPage : ContentPage
    {


        public VerEquipoPage()
        {
            InitializeComponent();
            //lblNombreEquipo.SetBinding(Label.TextProperty, new Binding(""));
            BindingContext = new VerEquipoViewModel();
        }
        
        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            ModelJugador jugadorSeleccionado = (ModelJugador) e.Item;
            
            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            ((VerEquipoViewModel)BindingContext).VerJugador(jugadorSeleccionado);
        }

        protected override void OnAppearing()
        {
            Console.WriteLine("OnAppearing--- " + this);
            ((VerEquipoViewModel)this.BindingContext).CargaInicial();
            base.OnAppearing();
        }
    }
}
