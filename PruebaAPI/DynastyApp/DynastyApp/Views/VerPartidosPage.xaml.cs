﻿using DynastyApp.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerPartidosPage : ContentPage
    {
        public VerPartidosPage()
        {
            InitializeComponent();
            this.BindingContext = new VerPartidosViewModel();
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            ModelPartido partidoSeleccionado = (ModelPartido)e.Item;

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            ((VerPartidosViewModel)BindingContext).VerPartido(partidoSeleccionado);
        }
        
        protected override void OnAppearing()
        {
            Console.WriteLine("OnAppearing--- " + this);
            ((VerPartidosViewModel)this.BindingContext).CargaInicial();
            base.OnAppearing();
        }
    }
}
