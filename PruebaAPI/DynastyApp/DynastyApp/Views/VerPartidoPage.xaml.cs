﻿using DynastyApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerPartidoPage : ContentPage
    {
        public VerPartidoPage()
        {
            InitializeComponent();
            BindingContext = new VerPartidoViewModel();
        }

        protected override void OnAppearing()
        {
            (BindingContext as VerPartidoViewModel).actualizarPartidoMostrado();
            base.OnAppearing();
        }
    }
}