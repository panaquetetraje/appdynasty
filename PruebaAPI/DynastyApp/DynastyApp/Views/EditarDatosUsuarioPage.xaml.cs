﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditarDatosUsuarioPage : ContentPage
    {
        public EditarDatosUsuarioPage()
        {
            InitializeComponent();
            this.BindingContext = new EditarDatosUsuarioViewModel();
        }
    }
}