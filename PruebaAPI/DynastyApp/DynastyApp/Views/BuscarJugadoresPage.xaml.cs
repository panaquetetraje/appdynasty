﻿using DynastyApp.APIS.Dynasty;
using DynastyApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BuscarJugadoresPage : ContentPage
    {
        public BuscarJugadoresPage()
        {
            InitializeComponent();
            this.BindingContext = new BuscarJugadoresViewModel();
        }
        public void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            ModelJugador jugadorSeleccionado = (ModelJugador) e.Item;

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            ((BuscarJugadoresViewModel)BindingContext).VerJugador(jugadorSeleccionado);
        }
    }
}