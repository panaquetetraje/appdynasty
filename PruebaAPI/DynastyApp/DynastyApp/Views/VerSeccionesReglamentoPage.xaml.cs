﻿using DynastyApp.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DynastyApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerSeccionesReglamentoPage : ContentPage
    {
        public VerSeccionesReglamentoPage()
        {
            InitializeComponent();
            this.BindingContext = new VerSeccionesReglamentoViewModel();
        }

        private void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            ModelSeccion seccionSeleccionado = (ModelSeccion)e.Item;

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            ((VerSeccionesReglamentoViewModel)BindingContext).VerSeccion(seccionSeleccionado);
        }
        
        protected override void OnAppearing()
        {
            Console.WriteLine("OnAppearing--- " + this);
            ((VerSeccionesReglamentoViewModel)this.BindingContext).CargaInicial();
            base.OnAppearing();
        }
    }
}
