﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class ListarEquiposViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private ObservableCollection<ModelEquipo> _equipos { get; set; }
        public ObservableCollection<ModelEquipo> Equipos
        {
            get { return _equipos; }
            set
            {
                _equipos = value;
                NotifyPropertyChanged();
            }
        }

        public async void cargarEquipos()
        {
            ApiDynasty.Inicializar();

            ObservableCollection<ModelEquipo> todosLosEquipos = ((ModelListaDeEquipos)await ApiDynasty.cargarEquipos()).records;
            for (int i = 0; i < todosLosEquipos.Count; i++)
            {
                ModelEquipo equipo = todosLosEquipos[i];
                if (equipo.id > 0)
                {
                    Equipos.Add(equipo);
                }
            }
        }

        public async void VerEquipo(ModelEquipo equipoSeleccionado)
        {
            Application.Current.Properties["EquipoMostrado"] = equipoSeleccionado;
            await Shell.Current.GoToAsync($"//{nameof(VerEquipoPage)}");
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ListarEquiposViewModel()
        {
            Equipos = new ObservableCollection<ModelEquipo>();
        }
    }
}