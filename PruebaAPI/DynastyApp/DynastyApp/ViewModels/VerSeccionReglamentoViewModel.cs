﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerSeccionReglamentoViewModel : BaseViewModel, INotifyPropertyChanged
    {
        public Command CargarResultadoCommand { get; }
        private ModelSeccion _datosSeccion { get; set; }
        public string GolesLocal { get; set; } = "";
        public string GolesVisitante { get; set; } = "";
        public ModelSeccion DatosSeccion
        {
            get { return _datosSeccion; }
            set
            {
                _datosSeccion = value;
                NotifyPropertyChanged();
            }
        }
        private string _textoError = "";
        public bool Error_IsVisible => _textoError != "";

        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Error_IsVisible");
            }
        }
        public void actualizarSeccionMostrada()
        {
            DatosSeccion = ((ModelSeccion)Application.Current.Properties["VerSeccion_SeccionMostrada"]);
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}