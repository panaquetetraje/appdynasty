﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerEquipoViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private string _nombreEquipo { get; set; } = "";
        private ObservableCollection<ModelJugador> _jugadores { get; set; } = new ObservableCollection<ModelJugador>();
        public string NombreEquipo
        {
            get { return _nombreEquipo; }
            set
            {
                _nombreEquipo = value;
                NotifyPropertyChanged();
            }
        }
        private string _logo = "";

        public bool TieneLogo => _logo != "";

        public string URLLogoEquipo
        {
            get { return _logo; }
            set
            {
                _logo = value;
                NotifyPropertyChanged();
            }
        }
        public ObservableCollection<ModelJugador> Jugadores
        {
            get { return _jugadores; }
            set
            {
                _jugadores = value;
                NotifyPropertyChanged();
            }
        }

        public async void VerJugador(ModelJugador jugadorSeleccionado)
        {
            Console.WriteLine("VerJugador" + jugadorSeleccionado.name);
            Application.Current.Properties["VerJugador_JugadorMostrado"] = jugadorSeleccionado;
            await Shell.Current.GoToAsync($"//{nameof(VerJugadorPage)}");
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public VerEquipoViewModel()
        {

        }

        public async void CargaInicial()
        {
            ApiDynasty.Inicializar();
            ModelEquipo equipoMostrado = (Application.Current.Properties["EquipoMostrado"] as ModelEquipo);
            NombreEquipo = equipoMostrado.name;
            URLLogoEquipo = equipoMostrado.logo;
            Jugadores = ((ModelListaDeJugadores) await ApiDynasty.cargarJugadoresDeEquipo(equipoMostrado.id)).records;
        }
    }
}