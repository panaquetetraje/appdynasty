﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerPartidoViewModel : BaseViewModel, INotifyPropertyChanged
    {
        public Command CargarResultadoCommand { get; }
        private ModelPartido _datosPartido { get; set; }
        public string GolesLocal { get; set; } = "";
        public string GolesVisitante { get; set; } = "";
        public ModelPartido DatosPartido
        {
            get { return _datosPartido; }
            set
            {
                _datosPartido = value;
                NotifyPropertyChanged();
            }
        }
        private string _textoError = "";
        public bool Error_IsVisible => _textoError != "";

        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Error_IsVisible");
            }
        }
        public void actualizarPartidoMostrado()
        {
            DatosPartido = ((ModelPartido)Application.Current.Properties["VerPartido_PartidoMostrado"]);
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public VerPartidoViewModel()
        {
            CargarResultadoCommand = new Command(OnCargarResultadoClicked); 
        }
        private async void OnCargarResultadoClicked(object obj)
        {
            if(GolesLocal == "" || GolesVisitante == "")
            {
                TextoError = "Datos incorrectos";
            }
            else
            {
                TextoError = "Cargando...";
            
                ApiDynasty.Inicializar();
                DatosPartido.res_local = Convert.ToInt32(GolesLocal);
                DatosPartido.res_visitante = Convert.ToInt32(GolesVisitante);
                Application.Current.Properties["VerPartido_PartidoMostrado"] = DatosPartido;

                ModelDatosUsuario DatosUsuarioLogueado = Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario;

                ModelOkOrError resultado = await ApiDynasty.cargarResultadoPartido(DatosPartido, DatosUsuarioLogueado.team_id);
                TextoError = resultado.message;
            }

        }
    }
}