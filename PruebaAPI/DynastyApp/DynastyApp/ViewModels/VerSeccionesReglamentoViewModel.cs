﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerSeccionesReglamentoViewModel : BaseViewModel, INotifyPropertyChanged
    {

        private ObservableCollection<ModelSeccion> _secciones { get; set; } = new ObservableCollection<ModelSeccion>();
        public ObservableCollection<ModelSeccion> Secciones
        {
            get { return _secciones; }
            set
            {
                _secciones = value;
                NotifyPropertyChanged();
            }
        }

        public async void VerSeccion(ModelSeccion seccionSeleccionada)
        {
            Application.Current.Properties["VerSeccion_SeccionMostrada"] = seccionSeleccionada;
            await Shell.Current.GoToAsync($"//{nameof(VerSeccionReglamentoPage)}");
        }

        private string _textoError = "";
        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Error_IsVisible");
            }
        }
        public bool Error_IsVisible => _textoError != "";

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void CargaInicial()
        {
            ApiDynasty.Inicializar();
            Secciones = await ApiDynasty.cargarSeccionesReglamento();
            if(Secciones.Count == 0)
            {
                TextoError = "No se encontraron secciones pendientes y habilitados";
            }
            else
            {
                TextoError = "Secciones pendientes y habilitados";
            }
        }
    }
}