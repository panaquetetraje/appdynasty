﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.ViewModels;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace DynastyApp.Views
{
    public class EditarDatosUsuarioViewModel : BaseViewModel, INotifyPropertyChanged
    {
        public Command ActualizarDatosCommand { get; }

        public string NombreUsuario { get; set; }
        public string Clave { get; set; }
        public string NombreEquipo { get; set; }

        private string _nombreFylyal = "";

        public bool TieneFylyal => _logo != "";
        public string NombreFylyal
        {
            get { return _nombreFylyal; }
            set
            {
                _nombreFylyal = value;
                NotifyPropertyChanged();
            }
        }

        private string _logo = "";

        public bool TieneLogo => _logo != "";
        
        public string URLLogoEquipo
        {
            get { return _logo; }
            set
            {
                _logo = value;
                NotifyPropertyChanged();
            }
        }

        private string _sofifaTeamUri = "";
        public string URISofifaTeam
        {
            get { return _sofifaTeamUri; }
            set
            {
                _sofifaTeamUri = value;
                NotifyPropertyChanged();
            }
        }
        private string _canalYoutube = "";
        public string CanalYoutube
        {
            get { return _canalYoutube; }
            set
            {
                _canalYoutube = value;
                NotifyPropertyChanged();
            }
        }

        private string _usuarioPsn = "";
        public string UsuarioPSN
        {
            get { return _usuarioPsn; }
            set
            {
                _usuarioPsn = value;
                NotifyPropertyChanged();
            }
        }


        


        private bool _error_IsVisible = false;
        private string _textoError = "";

        public bool Error_IsVisible
        {
            get { return _error_IsVisible; }
            set
            {
                _error_IsVisible = value;
                NotifyPropertyChanged();
            }
        }
        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
            }
        }
        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public EditarDatosUsuarioViewModel()
        {
            TextoError = "";
            Error_IsVisible = false;
            ActualizarDatosCommand = new Command(OnActualizarDatosClicked);
            if (Application.Current.Properties.TryGetValue("DatosUsuarioLogueado", out var value))
            {
                ModelDatosUsuario datosUsuario = Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario;
                NombreUsuario = datosUsuario.name;
                Clave = "";
                NombreEquipo = datosUsuario.team_name;
                NombreFylyal = datosUsuario.fylyal_name;
                URLLogoEquipo = datosUsuario.logo;
                URISofifaTeam = datosUsuario.sofifa_team_uri;
                UsuarioPSN = datosUsuario.usuario_psn;
                CanalYoutube = datosUsuario.canal_youtube;
            }
        }

        private async void OnActualizarDatosClicked(object obj)
        {
            TextoError = "Actualizando datos...";
            Error_IsVisible = true;
            ApiDynasty.Inicializar();
            bool datosFueronActualizados = await ApiDynasty.ActualizarDatosUsuario((Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).name, NombreUsuario, Clave
                , NombreEquipo, URLLogoEquipo, URISofifaTeam, UsuarioPSN, CanalYoutube);
            if(datosFueronActualizados)
            {
                /**
                 * Actualizo los datos del usuario logueado.
                 */
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).name = NombreUsuario;
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).team_name = NombreEquipo;
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).sofifa_team_uri = URISofifaTeam;
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).logo = URLLogoEquipo;
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).usuario_psn = UsuarioPSN;
                (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario).canal_youtube = CanalYoutube;
                TextoError = "Datos actualizados";
            }
            else
            {
                TextoError = "Error: No se actualizaron los datos";
            }
        }
    }
}