﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class BuscarJugadoresViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private ObservableCollection<ModelJugador> _jugadores { get; set; } = new ObservableCollection<ModelJugador>();
        public Command BuscarCommand { get; }
        public string TextoBusqueda { get; set; }
        public ObservableCollection<ModelJugador> Jugadores
        {
            get { return _jugadores; }
            set
            {
                _jugadores = value;
                NotifyPropertyChanged();
            }
        }

        public async void VerJugador(ModelJugador jugadorSeleccionado)
        {
            Application.Current.Properties["VerJugador_JugadorMostrado"] = jugadorSeleccionado;
            await Shell.Current.GoToAsync($"//{nameof(VerJugadorPage)}");
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public BuscarJugadoresViewModel()
        {
            BuscarCommand = new Command(OnBuscarClicked);
        }
        private async void OnBuscarClicked(object obj)
        {
            Console.WriteLine("OnBuscarClicked");
            ApiDynasty.Inicializar();
            Jugadores = ((ModelListaDeJugadores) await  ApiDynasty.buscarJugadores(TextoBusqueda)).records;
        }
    }
}