﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerPartidosViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private string _nombreEquipo { get; set; } = "";
        public string NombreEquipo
        {
            get { return _nombreEquipo; }
            set
            {
                _nombreEquipo = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<ModelPartido> _partidos { get; set; } = new ObservableCollection<ModelPartido>();
        public ObservableCollection<ModelPartido> Partidos
        {
            get { return _partidos; }
            set
            {
                _partidos = value;
                NotifyPropertyChanged();
            }
        }

        public async void VerPartido(ModelPartido partidoSeleccionado)
        {
            Application.Current.Properties["VerPartido_PartidoMostrado"] = partidoSeleccionado;
            await Shell.Current.GoToAsync($"//{nameof(VerPartidoPage)}");
        }

        private string _textoError = "";
        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Error_IsVisible");
            }
        }
        public bool Error_IsVisible => _textoError != "";

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void CargaInicial()
        {
            ApiDynasty.Inicializar();
            ModelDatosUsuario datosUsuarioLogueado = (Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario);
            NombreEquipo = datosUsuarioLogueado.team_name;
            Partidos = ((ObservableCollection<ModelPartido>) await ApiDynasty.cargarPartidosParaEquipoEnFecha(datosUsuarioLogueado.team_id, DateTime.Now));
            if(Partidos.Count == 0)
            {
                TextoError = "No se encontraron partidos pendientes y habilitados";
            }
            else
            {
                TextoError = "Partidos pendientes y habilitados";
            }
        }
    }
}