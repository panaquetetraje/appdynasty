﻿using DynastyApp.APIS.Dynasty;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class VerJugadorViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private ModelJugador _datosJugador { get; set; }
        public ModelJugador DatosJugador
        {
            get { return _datosJugador; }
            set
            {
                _datosJugador = value;
                NotifyPropertyChanged();
            }
        }

        public void actualizarJugadorMostrado()
        {
            DatosJugador = ((ModelJugador)Application.Current.Properties["VerJugador_JugadorMostrado"]);
        }

        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public VerJugadorViewModel()
        {
            
        }

        public ICommand ClickCommand => new Command<string>((url) =>
        {
            Launcher.OpenAsync(new System.Uri(url));
        });
    }
}