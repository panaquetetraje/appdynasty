﻿using DynastyApp.APIS;
using DynastyApp.APIS.Dynasty;
using DynastyApp.Views;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace DynastyApp.ViewModels
{
    public class LoginViewModel : BaseViewModel, INotifyPropertyChanged
    {
        public Command LoginCommand { get; }
        public string Nombre { get; set; } = "";
        public string Clave { get; set; } = "";
        private string _textoError = "";

        public bool Error_IsVisible => _textoError != "";
        
        public string TextoError
        {
            get { return _textoError; }
            set
            {
                _textoError = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Error_IsVisible");
            }
        }
        public new event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public LoginViewModel()
        {
            Console.WriteLine("LoginViewModel--------");
            ApiDynasty.Inicializar();
            if (Application.Current.Properties.TryGetValue("DatosUsuarioLogueado", out object value))
            {
                mostrarEquipoDeUsuarioLogueado();
            }
            else
            {
                LoginCommand = new Command(OnLoginClicked);
            }
        }


        private async void OnLoginClicked(object obj)
        {
            Console.WriteLine("OnLoginClicked: " + Nombre + ", " + Clave);
            TextoError = "Cargando...";
            ApiDynasty.Inicializar();
            bool resultado = await ApiDynasty.Login(Nombre, Clave);
            if (resultado)
            {
                ModelDatosUsuario DatosUsuarioLogueado = await ApiDynasty.cargarDatosDeUsuario(Nombre);

                Console.WriteLine("Cargó datos de usuario: " + DatosUsuarioLogueado.team_name);
                Application.Current.Properties["DatosUsuarioLogueado"] = DatosUsuarioLogueado;

                mostrarEquipoDeUsuarioLogueado();
            }
            else
            {
                TextoError = "Usuario o clave incorrecto";
            }
        }
        /*
         * @pre Application.Current.Properties["DatosUsuarioLogueado"] seteado.
         */
        private async void mostrarEquipoDeUsuarioLogueado()
        {
            ModelDatosUsuario DatosUsuarioLogueado = Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario;
            ModelEquipo equipoMostrado = new ModelEquipo
            {
                name = DatosUsuarioLogueado.team_name,
                logo = DatosUsuarioLogueado.logo,
                sofifa_team_uri = DatosUsuarioLogueado.sofifa_team_uri,
                id = DatosUsuarioLogueado.team_id
            };
            Application.Current.Properties["EquipoMostrado"] = equipoMostrado;
            TextoError = "";
            await Shell.Current.GoToAsync($"//{nameof(VerEquipoPage)}");
        }
    }
}