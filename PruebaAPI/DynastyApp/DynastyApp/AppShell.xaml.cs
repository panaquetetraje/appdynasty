﻿using DynastyApp.APIS.Dynasty;
using DynastyApp.ViewModels;
using DynastyApp.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace DynastyApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
        }

        private void OnMenuItemClicked(object sender, EventArgs e)
        {
            Application.Current.Properties.Clear();
            Shell.Current.FlyoutIsPresented = false;
            Application.Current.MainPage = new AppShell();
        }
        private async void OnMiEquipoClicked(object sender, EventArgs e)
        {
            ModelDatosUsuario datosUsuarioLogueado = Application.Current.Properties["DatosUsuarioLogueado"] as ModelDatosUsuario;
            if ((Application.Current.Properties["EquipoMostrado"] as ModelEquipo).name != datosUsuarioLogueado.team_name)
            {
                ModelEquipo equipoMostrado = new ModelEquipo
                {
                    name = datosUsuarioLogueado.team_name,
                    id = datosUsuarioLogueado.team_id
                };
                Application.Current.Properties["EquipoMostrado"] = equipoMostrado;
            }
            await Shell.Current.GoToAsync($"//{nameof(VerEquipoPage)}");
            Shell.Current.ForceLayout();

            Shell.Current.FlyoutIsPresented = false;
        }
    }
}
